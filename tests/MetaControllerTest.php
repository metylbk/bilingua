<?php


class MetaControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->seed('MetaTableSeeder');
    }

    public function testIndexMethod()
    {
        $response = $this->json('GET', 'api/metas?category=1', [], $this->getHeaderWithoutAuthentication());
        $response->seeJsonStructure([
            ['name', 'content', 'category'],

        ]);
    }

    public function testGetByIdMethod()
    {
        $response = $this->json('GET', 'api/metas/21', [], $this->getHeaderWithoutAuthentication());
        $response->seeJsonStructure([
            'category', 'name', 'content'
        ]);
    }
}
