<?php


class InterestControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->seed('UserTableSeeder');
    }

    public function testCreateInterestRequest()
    {
        $this->actingAs(App\User::find(1));

        $response = $this->json('POST', 'api/interests/', ['name' => 'aaa'], $this->getHeaderWithAuthentication());

        $response->seeJsonStructure([
            'name',
            'id',
            'interest_user_count',
        ]);
    }

    public function testIndex()
    {
        $this->actingAs(App\User::find(1));

        $response = $this->json('GET', 'api/interests/', [], $this->getHeaderWithAuthentication());

        $response->seeJsonStructure([
            [
                'name',
                'id',
                'interest_user_count',
            ],
        ]);
    }

}
