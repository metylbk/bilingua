<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CountryControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->seed('UserTableSeeder');
    }

    public function testIndexMethod()
    {
        $this->actingAs(App\User::find(1));

        $response = $this->json('GET', 'api/countries', [], $this->getHeaderWithAuthentication());

        $response->seeJsonStructure([
            ['id'],
        ]);
    }
}
