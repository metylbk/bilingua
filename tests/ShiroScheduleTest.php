<?php

use App\User;
use App\ShiroWord;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ShiroScheduleTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->seed('LanguageTableSeeder');

        factory(User::class, 2)->create(
            [
                'last_active_at' => Carbon::now(),
            ]
        );

        ShiroWord::create(
            [
                'word'               => 'pomme',
                'word_language_id'   => 26,
                'definition'         => 'apple',
                'definition_language_id' => 29,
                'sentence'           => 'Je mange une pomme',
                'correct_answers'    => 'pomme',
                'fun_fact'           => 'Did you know pomme de terre also means potato!',
                'level'              => 2,
            ]
        );

        // ShiroWord::create(
        //     [
        //         'word'               => '苹果',
        //         'word_language_id'   => 9,
        //         'pronunciation'         => 'píngguŏ',
        //         'definition'         => 'apple',
        //         'definition_language_id' => 29,
        //         'sentence'           => 'Wo xihuan chi pingguo',
        //         'correct_answers'    => '苹果,píngguŏ,pingguo',
        //         'level'              => 1,
        //     ]
        // );

        $user = User::findOrFail(2);
        $user->learningLanguages()->attach(26, ['proficiency' => 2]);
        $user->learningLanguages()->attach(9, ['proficiency' => 1]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserSendMessageToShiro()
    {
        sleep(8); // To wait for running schedule (php artisan schedule:run)

        $this->actingAs(User::find(2));

        $this->json('PUT', 'api/messages/1', ['message' => 'pomme2']);

        $incorrectMessage = User::find(env('SHIRO_ID'))->messages()->orderBy('id', 'desc')->first();

        $this->assertEquals('Oh so close! Can you try again?', $incorrectMessage->body);

        $this->json('PUT', 'api/messages/1', ['message' => 'pomme']);

        $correctMessage = User::find(env('SHIRO_ID'))->messages()->orderBy('id', 'desc')->first();

        $this->assertEquals('Did you know pomme de terre also means potato!', $correctMessage->body);
    }
}
