<?php

use App\User;

class UserControllerTest extends TestCase
{
    protected $token;

    public function setUp()
    {
        parent::setUp();

        $user = User::create([
            'email'    => 'test@example.com',
            'password' => '123456',
            'settings' => json_encode(['a' => 1, 'b' => 'ccc', 'd' => true]),
            'birthday' => \Carbon\Carbon::now(),
        ]);

        User::create([
            'email'    => 'test2@example.com',
            'password' => '123456',
            'settings' => json_encode(['visibility_min_age' => 59]),
        ]);

        $this->seed('UserTableSeeder');
        $this->seed('InterestTableSeeder');
    }

    public function testMeMethod()
    {
        $this->actingAs(App\User::find(1));
        $response = $this->json('GET', 'api/users/me', [], $this->getHeaderWithAuthentication());
        var_dump($response->response->getContent());
        $response->seeJsonStructure([
                'id',
                'username',
                'email',
                'first_name',
                'family_name',
                'avatar',
                'gender',
                'birthday',
                'bio',
                'phone',
                'email_verified',
                'settings',
                'google_id',
                'facebook_id',
                'created_at',
                'updated_at',
                'interests',
                'spoken_languages',
                'learning_languages',

        ]);
    }

    public function testGetProfileMethod()
    {
        $this->actingAs(App\User::find(1));

        $response = $this->json('GET', 'api/users/3', [], $this->getHeaderWithAuthentication());

        $response->seeJsonStructure([
                'id',
                'username',
                'first_name',
                'family_name',
                'avatar',
                'gender',
                'birthday',
                'bio',
                'created_at',
                'updated_at',
                'interests',
                'spoken_languages',
                'learning_languages',
        ]);
    }

    public function testUpdateMethod()
    {
        $this->actingAs(App\User::find(1));
        $response = $this->json('PATCH', 'api/users/me', ['bio' => 'updated', 'birthday' => '10'], $this->getHeaderWithAuthentication());

        //var_dump($response->response->getContent());

        $this->seeInDatabase('users', ['email' => 'test@example.com', 'bio' => 'updated', 'birthday' => \Carbon\Carbon::now()->subYears(10)->toDateString()]);
    }

    public function testSyncInterestMethod()
    {
        $this->actingAs(App\User::find(1));
        $response = $this->json('PUT', 'api/users/me/interests', ['interests' => [['id' => 1], ['id' => 2]]], $this->getHeaderWithAuthentication());

        $response->seeJsonStructure(['id']);
    }

    public function testGetSettings()
    {
        $this->actingAs(App\User::find(1));
        $response = $this->json('GET', 'api/users/me/settings', [], $this->getHeaderWithAuthentication());

        $response->seeJsonEquals(['a' => 1, 'b' => 'ccc', 'd' => true]);
    }

    public function testUpdateSettings()
    {
        $this->actingAs(App\User::find(1));
        $response = $this->json('PATCH', 'api/users/me/settings/', ['a' => 2, 'g' => 1], $this->getHeaderWithAuthentication());

        $response->seeJsonContains(['a' => 2, 'g' => 1]);
    }

    public function testGetUser()
    {
        $this->actingAs(App\User::find(1));
        $response = $this->json('GET', 'api/users/', [], $this->getHeaderWithAuthentication());
        var_dump($response->response->getContent());

        $response->seeJsonStructure([
            ['id'],
        ]);
    }

    public function testGetUserByEmailAndNoMatch()
    {
        $this->actingAs(App\User::find(1));
        $response = $this->json('GET', 'api/users/', ['email' => 'tes2t@example.com'], $this->getHeaderWithAuthentication());

        $response->see('[]');
    }

    public function testSyncLanguages()
    {
        $this->actingAs(App\User::find(1));
        $input = [
            'languages' => [
                ['id' => 1, 'proficiency' => 1],
                ['id' => 2, 'proficiency' => 2],
            ],
            'type' => 1,
        ];

        $response = $this->json('PUT', 'api/users/me/languages', $input, $this->getHeaderWithAuthentication());

        $this->seeInDatabase('learning_language_user', ['language_id' => 1, 'proficiency' => 1]);
        $response->seeStatusCode(204);
    }

    public function testBlockMethods()
    {
        $this->actingAs(App\User::find(1));
        $input = [
            'reason' => 'Fake profile.',
        ];

        $response = $this->json('POST', 'api/users/2/block', $input, $this->getHeaderWithAuthentication());
        $this->seeInDatabase('blocks', ['target_id' => 2, 'reason' => 'Fake profile.']);
        $response->seeStatusCode(204);

        $response = $this->json('GET', 'api/users/blocked', [], $this->getHeaderWithAuthentication());

        $response->seeJsonStructure([
            ['id'],
        ]);

        $response = $this->json('POST', 'api/users/2/unblock', [], $this->getHeaderWithAuthentication());

        $response->seeStatusCode(204);
    }
}
