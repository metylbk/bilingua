<?php

use App\Events\MessageHasEmailEvent;
use App\Events\MessageSentToManyEvent;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\Message;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;

class MessageControllerTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();

        User::create([
            'first_name' => 'first',
            'family_name' => 'family',
            'avatar' => 'https://app.bilingua.io/images/shiro/shiro-happy.png',
            'email'    => 'test-message@example.com',
            'password' => '123456',
            'settings' => json_encode(['a' => 1, 'b' => 'ccc', 'd' => true]),
            'birthday' => \Carbon\Carbon::now(),
        ]);

        User::create([
            'first_name' => 'first',
            'family_name' => 'family',
            'avatar' => 'https://app.bilingua.io/images/shiro/shiro-happy.png',
            'email'    => 'test-message-2@example.com',
            'password' => '123456',
            'settings' => json_encode(['visibility_min_age' => 59]),
        ]);

        User::create([
            'first_name' => 'first',
            'family_name' => 'family',
            'avatar' => 'https://app.bilingua.io/images/shiro/shiro-happy.png',
            'email'    => 'test-message-3@example.com',
            'password' => '123456',
            'settings' => json_encode(['visibility_min_age' => 59]),
        ]);

        $user = User::find(1);
        $thread = $user->threads()->create(['type' => '1-2']);
        $thread->users()->syncWithoutDetaching([2]);
        $user->threads()->updateExistingPivot($thread->id, ['last_read_at' => Carbon::now()]);
        $user->activeThreads()->attach($thread->id);

        $message = Message::create(
            [
                'thread_id' => 1,
                'user_id'   => 1,
                'body'      => 'test message',
            ]
        );
    }

    public function testGetThreadsMethod()
    {
        $this->actingAs(User::find(1));
        $response = $this->json('GET', 'api/messages');

        var_dump($response->response->content());

        $response->seeJsonStructure([
            ['id'],
        ]);
    }

    public function testGetMessagesMethod()
    {
        $this->actingAs(User::find(1));

        $response = $this->json('GET', 'api/messages/1');

        var_dump($response->response->content());

        $response->seeJsonStructure([
            'id',
        ]);
    }

    public function testCreateThreadMethod()
    {
        $this->actingAs(User::find(1));

        $response = $this->json('POST', 'api/messages', ['recipients' => ['3']]);

        var_dump($response->response->content());

        $response->seeJsonStructure([
            'id',
        ]);

        $response = $this->json('POST', 'api/messages', ['recipients' => ['3']]);

        var_dump($response->response->content());

        $response->seeJsonStructure([
            'id',
        ]);
    }

    public function testCreateMessageMethod()
    {
        Event::fake();
        $this->actingAs(User::find(1));

        $response = $this->json('PUT', 'api/messages/1', ['message' => 'this message contains an email: example@email.com']);

        var_dump($response->response->content());

        $this->assertResponseStatus(204);
        Event::assertFired(MessageHasEmailEvent::class);
    }

    public function testUpdateLastReadAt()
    {
        $this->actingAs(User::find(1));

        $response = $this->json('POST', 'api/messages/1/read');

        var_dump($response->response->content());

        $this->assertResponseStatus(204);
    }

    public function testDeleteThreadFromList()
    {
        $this->actingAs(User::find(1));

        $response = $this->json('DELETE', 'api/messages/1');

        var_dump($response->response->content());

        $this->assertResponseStatus(204);
    }

    public function testSpamAlertEvent()
    {
        Event::fake();
        $this->actingAs(User::find(1));
        for ($i = 0; $i < 5; $i++){
            $this->json('PUT', 'api/messages/1', ['message' => 'test message']);
            $this->assertResponseStatus(204);
        }
        if (User::find(1)->messages->where('body' , 'test message')->count() > 3){
            Event::assertFired(MessageSentToManyEvent::class);
        }
    }
}
