<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class AuthenticateControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->seed('UserTableSeeder');
    }

    public function testCheckEmail()
    {
        $response = $this->json('POST', 'api/auth/email.check', ['email' => 't@tt.tt'], $this->getHeaderWithoutAuthentication());
        $response->seeStatusCode(404);

        $response = $this->json('POST', 'api/auth/email.check', ['email' => 'email@example.com'], $this->getHeaderWithoutAuthentication());
        $response->seeStatusCode(204);
    }

    public function testRegisterUserResponse()
    {
        $response = $this->json('POST', 'api/auth/reg', ['email' => 'test@example.com', 'password' => '123456'], $this->getHeaderWithoutAuthentication());

        var_dump($response->response->getContent());

        $response->seeJsonStructure(['token']);
    }

    public function testLoginUser()
    {
        $response = $this->json('POST', 'api/auth/login', ['email' => 'email@example.com', 'password' => '123456'], $this->getHeaderWithoutAuthentication());

        $response->seeJsonStructure(['token']);

        $response->seeIsAuthenticated();
    }

    public function testTokenTest()
    {
        $response = $this->json('POST', 'api/auth/login', ['email' => 'email@example.com', 'password' => '123456'], $this->getHeaderWithoutAuthentication());

        $content = json_decode($response->response->getContent(), true);

        $response = $this->json('GET', 'api/auth/check', [], $this->getHeaderWithAuthentication());

        $response->assertResponseStatus(204);
    }
}
