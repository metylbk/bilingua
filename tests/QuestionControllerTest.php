<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class QuestionControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $user = User::create([
            'email'    => 'test@example.com',
            'password' => '123456',
            'settings' => json_encode(['a' => 1, 'b' => 'ccc', 'd' => true]),
            'birthday' => \Carbon\Carbon::now(),
        ]);

        User::create([
            'email'    => 'test2@example.com',
            'password' => '123456',
            'settings' => json_encode(['visibility_min_age' => 59]),
        ]);

        $user->questions()->syncWithoutDetaching(['1']);
        $user->questions()->updateExistingPivot(1, [
           'answer' => '1',
           'expect' => '12',
           'relevance' => '20'
        ]);

        User::find(2)->questions()->syncWithoutDetaching(['1']);
        User::find(2)->questions()->updateExistingPivot(1, [
           'answer' => '2',
           'expect' => '12',
           'relevance' => '20'
        ]);
    }

    public function testGetAllQuestions()
    {
        $this->actingAs(User::find(1));

        $response = $this->json('GET', 'api/questions');

        var_dump($response->response->getContent());

        $response->seeJsonStructure([
            ['id'],
        ]);
    }

    public function testGetUserQuestions()
    {
        $this->actingAs(User::find(1));

        $response = $this->json('GET', 'api/users/me/questions');

        var_dump($response->response->getContent());

        $response->seeJsonStructure([
            ['id'],
        ]);
    }

    public function testSaveUserQuestions()
    {
        $this->actingAs(User::find(1));

        $data = [
            'answer' => 2,
            'expect' => 12,
            'relevance' => 10,
        ];

        $response = $this->json('POST', 'api/users/me/questions/2', $data);

        var_dump($response->response->getContent());

        $response->assertResponseStatus(204);
    }

    public function testGetMatchScore()
    {
        $this->actingAs(User::find(1));

        $response = $this->json('GET', 'api/users/2/matchscore');

        var_dump($response->response->getContent());

        $response->seeText('0');

        $response->dontSeeText('wrong.');
    }
}
