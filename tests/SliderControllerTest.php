<?php

class SliderControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->seed('SliderTableSeeder');
    }

    public function testAllFunction()
    {
        $response = $this->get('api/sliders', $this->getHeaderWithoutAuthentication());

        $response->seeJsonStructure([
            ['content', 'description', 'image', 'title'],
        ]);
    }
}
