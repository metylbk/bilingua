<?php

use App\User;
use App\ShiroWord;

class ShiroWordControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        factory(User::class)->create();
    }

    public function testCreateShiroWordRequest()
    {
        $user = User::firstOrFail();

        $shiroWord = factory(ShiroWord::class)->create();

        $data = [
            'word'                   => $shiroWord->word,
            'word_language_id'       => $shiroWord->word_language_id,
            'pronunciation'          => $shiroWord->pronunciation,
            'definition'             => $shiroWord->definition,
            'definition_language_id' => $shiroWord->definition_language_id,
            'sentence'               => $shiroWord->sentence,
            'correct_answers'        => $shiroWord->correct_answers,
            'fun_fact'               => $shiroWord->fun_fact,
            'level'                  => $shiroWord->level,
        ];

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('POST', 'api/shiroWords', $data);
        $response->seeStatusCode(401);

        // Case 2: See response status code 204 when success
        $response = $this->actingAs($user)->json('POST', 'api/shiroWords', $data);
        $response->assertResponseStatus(204);

        // Case 3: Check whether the created one has the same data
        $newShiroWord = ShiroWord::findOrFail($shiroWord->id);
        $newData = [
            'word'                   => $newShiroWord->word,
            'word_language_id'       => $newShiroWord->word_language_id,
            'pronunciation'          => $newShiroWord->pronunciation,
            'definition'             => $newShiroWord->definition,
            'definition_language_id' => $newShiroWord->definition_language_id,
            'sentence'               => $newShiroWord->sentence,
            'correct_answers'        => $newShiroWord->correct_answers,
            'fun_fact'               => $newShiroWord->fun_fact,
            'level'                  => $newShiroWord->level,
        ];
        $this->assertEquals($data, $newData);
    }

    public function testIndex()
    {
        $validStructure = [
            [
                'id',
                'word',
                'word_language_id',
                'pronunciation',
                'definition',
                'definition_language_id',
                'sentence',
                'correct_answers',
                'fun_fact',
                'level',
                'created_at',
                'updated_at',
            ],
        ];

        $user = User::firstOrFail();

        factory(ShiroWord::class)->create();

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('GET', "api/shiroWords");
        $response->seeStatusCode(401);

        // Case 2: Get valid structure list of shiro words
        $response = $this->actingAs($user)->json('GET', "api/shiroWords");
        $response->seeJsonStructure($validStructure);
    }

    public function testUpdate()
    {
        $user = User::firstOrFail();

        $shiroWord = factory(ShiroWord::class)->create();

        $shiroWord2 = factory(ShiroWord::class)->create();

        $data = [
            'word'               => $shiroWord2->word,
            'word_language_id'   => $shiroWord2->word_language_id,
            'pronunciation'      => $shiroWord2->pronunciation,
            'definition'         => $shiroWord2->definition,
            'definition_language_id' => $shiroWord2->definition_language_id,
            'sentence'           => $shiroWord2->sentence,
            'correct_answers'    => $shiroWord2->correct_answers,
            'fun_fact'           => $shiroWord2->fun_fact,
            'level'              => $shiroWord2->level,
        ];

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('PUT', "api/shiroWords/{$shiroWord->id}", $data);
        $response->seeStatusCode(401);

        // Case 2: See response status code 204 when success
        $response = $this->actingAs($user)->json('PUT', "api/shiroWords/{$shiroWord->id}", $data);
        $response->assertResponseStatus(204);

        // Case 3: Make sure data is updated properly
        $updatedShiroWord = ShiroWord::findOrFail($shiroWord->id);
        $updatedData = [
            'word'               => $updatedShiroWord->word,
            'word_language_id'   => $updatedShiroWord->word_language_id,
            'pronunciation'      => $updatedShiroWord->pronunciation,
            'definition'         => $updatedShiroWord->definition,
            'definition_language_id' => $updatedShiroWord->definition_language_id,
            'sentence'           => $updatedShiroWord->sentence,
            'correct_answers'    => $updatedShiroWord->correct_answers,
            'fun_fact'           => $updatedShiroWord->fun_fact,
            'level'              => $updatedShiroWord->level,
        ];
        $this->assertEquals($data, $updatedData);
    }

    public function testDelete()
    {
        $user = User::firstOrFail();

        $shiroWord = factory(ShiroWord::class)->create();

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('DELETE', "api/shiroWords/{$shiroWord->id}");
        $response->seeStatusCode(401);

        // Case 2: See status code 404 when the Shiro word doesn't exist
        $response = $this->actingAs($user)->json('DELETE', "api/shiroWords/1000");
        $response->seeStatusCode(404);

        // Case 3: See response status code 204 when success
        $response = $this->actingAs($user)->json('DELETE', "api/shiroWords/{$shiroWord->id}");
        $response->assertResponseStatus(204);

        // Case 4: Make sure the Shiro word is deleted
        $this->assertEquals(ShiroWord::count(), 0);
    }
}
