<?php

use App\User;
use App\VocabularyList;
use App\Word;
use App\Translation;

class VocabularyListControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        factory(User::class, 2)->create();
    }

    public function testShow()
    {
        $user = User::firstOrFail();

        $vocabularyList = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
        ]);

        $validStructure = [
            'id',
            'name',
            'is_public',
            'user_id',
            'created_at',
            'updated_at',
        ];

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('GET', "api/vocabularyLists/{$vocabularyList->id}");
        $response->seeStatusCode(401);

        // Case 2: See status code 404 when the vocabulary list doesn't exist
        $response = $this->actingAs($user)->json('GET', "api/vocabularyLists/1000");
        $response->seeStatusCode(404);

        // Case 3: Get valid json data
        $response = $this->json('GET', "api/vocabularyLists/{$vocabularyList->id}");
        $response->seeJsonStructure($validStructure);
    }

    public function testCreateVocabularyListRequest()
    {
        $validStructure = [
            'id',
            'name',
            'is_public',
            'user_id',
            'created_at',
            'updated_at',
        ];

        $user = User::firstOrFail();

        $vocabularyList = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
        ]);

        $data = [
            'name'      => $vocabularyList->name,
            'is_public' => $vocabularyList->is_public,
        ];

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('POST', 'api/vocabularyLists', $data);
        $response->seeStatusCode(401);

        // Case 2: See the valid json structure when success
        $response = $this->actingAs($user)->json('POST', 'api/vocabularyLists', $data);
        $response->seeJsonStructure($validStructure);
    }

    public function testIndex()
    {
        $validStructure = [
            [
                'id',
                'name',
                'is_public',
                'user_id',
                'created_at',
                'updated_at',
            ],
        ];

        $user = User::firstOrFail();

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('GET', "api/users/{$user->id}/vocabularyLists");
        $response->seeStatusCode(401);

        // Case 2: See status code 404 when getting vocabulary lists of a nonexistent user
        $response = $this->actingAs($user)->json('GET', "api/users/1000/vocabularyLists");
        $response->seeStatusCode(404);

        // Case 3: User who doesn't have vocabulary lists gets vocabulary lists of himself/herself (the default vocabulary lists will be created)
        $response = $this->actingAs($user)->json('GET', "api/users/{$user->id}/vocabularyLists");
        $response->seeJsonStructure($validStructure);

        // Case 4: User who already has vocabulary lists gets vocabulary lists of himself/herself (see all lists)
        $vocabularyList = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
        ]);
        $response = $this->actingAs($user)->json('GET', "api/users/{$user->id}/vocabularyLists");
        $response->seeJsonStructure($validStructure);

        // Case 5: Get vocabulary lists of others (see only public lists)
        $user2 = factory(User::class)->create();
        $response = $this->actingAs($user2)->json('GET', "api/users/{$user->id}/vocabularyLists");
        $response->seeJsonStructure($validStructure);

        // Case 6: Get public lists of another user (the default vocabulary lists will be created)
        $response = $this->actingAs($user)->json('GET', "api/users/{$user2->id}/vocabularyLists");
        $response->seeJsonStructure($validStructure);
    }

    public function testUpdate()
    {
        $user = User::firstOrFail();

        $vocabularyList = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
        ]);

        $vocabularyList2 = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
        ]);

        $data = [
            'name'      => $vocabularyList2->name,
            'is_public' => $vocabularyList2->is_public,
        ];

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('PUT', "api/vocabularyLists/{$vocabularyList->id}", $data);
        $response->seeStatusCode(401);

        // Case 2: See status code 403 when update other's list
        $user2 = User::findOrFail(2);

        $response = $this->actingAs($user2)->json('PUT', "api/vocabularyLists/{$vocabularyList->id}", $data);
        $response->seeStatusCode(403);

        // Case 3: See response status code 204 when success
        $response = $this->actingAs($user)->json('PUT', "api/vocabularyLists/{$vocabularyList->id}", $data);
        $response->assertResponseStatus(204);

        // Case 4: Make sure data is updated properly
        $updatedVocabularyList = VocabularyList::findOrFail($vocabularyList->id);
        $updatedData = [
            'name' => $updatedVocabularyList->name,
            'is_public' => $updatedVocabularyList->is_public,
        ];
        $this->assertEquals($data, $updatedData);
    }

    public function testInvertPublic()
    {
        $user = User::firstOrFail();

        $vocabularyList = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
        ]);

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('PUT', "api/vocabularyLists/{$vocabularyList->id}/invertPublic");
        $response->seeStatusCode(401);

        // Case 2: See status code 403 when invert public other's list
        $user2 = User::findOrFail(2);

        $response = $this->actingAs($user2)->json('PUT', "api/vocabularyLists/{$vocabularyList->id}/invertPublic");
        $response->seeStatusCode(403);

        // Case 3: See response status code 204 when success
        $response = $this->actingAs($user)->json('PUT', "api/vocabularyLists/{$vocabularyList->id}/invertPublic");
        $response->assertResponseStatus(204);

        // Case 4: Check whether the is_public field is inverted or not
        $updatedVocabularyList = VocabularyList::findOrFail($vocabularyList->id);
        $this->assertEquals($vocabularyList->is_public, ! $updatedVocabularyList->is_public);
    }

    public function testDelete()
    {
        $user = User::firstOrFail();

        $vocabularyList = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
        ]);

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('DELETE', "api/vocabularyLists/{$vocabularyList->id}");
        $response->seeStatusCode(401);

        // Case 2: See status code 403 when delete other's list
        $user2 = User::findOrFail(2);

        $response = $this->actingAs($user2)->json('DELETE', "api/vocabularyLists/{$vocabularyList->id}");
        $response->seeStatusCode(403);

        // Case 3: See response status code 204 when success
        $word = factory(Word::class)->create();
        $vocabularyList->words()->attach($word->id);

        $response = $this->actingAs($user)->json('DELETE', "api/vocabularyLists/{$vocabularyList->id}");
        $response->assertResponseStatus(204);

        // Case 4: Make sure the vocabulary list is deleted
        $this->assertNull(VocabularyList::find($vocabularyList->id));

        // Case 5: Make sure the vocabulary word relationship is deleted
        $this->assertEquals($word->vocabularyLists()->count(), 0);
    }

    public function testAddWord()
    {
        $user = User::findOrFail(1);

        $vocabularyList = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
        ]);

        $word = factory(Word::class)->create();

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('POST', "api/vocabularyLists/{$vocabularyList->id}/words/{$word->id}");
        $response->seeStatusCode(401);

        // Case 2: See status code 403 when adding word of other's list.
        $response = $this->actingAs($user)->json('POST', "api/vocabularyLists/1000/words/{$word->id}");
        $response->seeStatusCode(403);

        // Case 3: See response status code 204 when success when adding without is_favorite
        $response = $this->actingAs($user)->json('POST', "api/vocabularyLists/{$vocabularyList->id}/words/{$word->id}");
        $response->assertResponseStatus(204);

        // Case 4: Make sure this vocabulary list word relationship is added
        $count = $vocabularyList->words()->count();
        $this->assertEquals($count, 1);

        // Case 5: See response status code 204 when success when adding with is_favorite
        $vocabularyList->words()->detach($word->id);

        $response = $this->actingAs($user)->json('POST', "api/vocabularyLists/{$vocabularyList->id}/words/{$word->id}", ['is_favorite' => true]);
        $response->assertResponseStatus(204);

        // Case 6: Make sure this vocabulary list word relationship is added
        $count = $vocabularyList->words()->count();
        $this->assertEquals($count, 1);
    }

    public function testRemoveWord()
    {
        $user = User::findOrFail(1);

        $vocabularyList = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
        ]);

        $word = factory(Word::class)->create();

        $vocabularyList->words()->attach($word->id);

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('DELETE', "api/vocabularyLists/{$vocabularyList->id}/words/{$word->id}");
        $response->seeStatusCode(401);

        // Case 2: See status code 403 when delete word in other's list
        $user2 = User::findOrFail(2);
        $response = $this->actingAs($user2)->json('DELETE', "api/vocabularyLists/{$vocabularyList->id}/words/{$word->id}");
        $response->seeStatusCode(403);

        // Case 3: See response status code 204 when success
        $response = $this->actingAs($user)->json('DELETE', "api/vocabularyLists/{$vocabularyList->id}/words/{$word->id}");
        $response->assertResponseStatus(204);

        // Case 4: Make sure this vocabulary list word relationship is deleted
        $count = $vocabularyList->words()->count();
        $this->assertEquals($count, 0);
    }

    public function testInvertFavoriteWord()
    {
        $user = User::firstOrFail();

        $vocabularyList = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
        ]);

        $word = factory(Word::class)->create();
        $vocabularyList->words()->attach($word->id, ['is_favorite' => random_int(0, 1)]);
        $IsFavorite = $vocabularyList->words[0]->pivot->is_favorite;

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('PUT', "api/vocabularyLists/{$vocabularyList->id}/words/{$word->id}/invertFavoriteWord");
        $response->seeStatusCode(401);

        // Case 2: See status code 403 when invert favorite word of other's list
        $user2 = User::findOrFail(2);

        $response = $this->actingAs($user2)->json('PUT', "api/vocabularyLists/{$vocabularyList->id}/words/{$word->id}/invertFavoriteWord");
        $response->seeStatusCode(403);

        // Case 3: See response status code 204 when success
        $response = $this->actingAs($user)->json('PUT', "api/vocabularyLists/{$vocabularyList->id}/words/{$word->id}/invertFavoriteWord");
        $response->assertResponseStatus(204);

        // Case 4: Check whether the is_favorite field is inverted or not
        $updatedIsFavorite = $vocabularyList->words()->whereWordId($word->id)->first()->pivot->is_favorite;
        $this->assertEquals($IsFavorite, ! $updatedIsFavorite);
    }
}
