<?php

use App\User;
use App\VocabularyList;
use App\Word;
use App\Translation;

class VocabularyControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        factory(User::class, 2)->create();
    }

    public function testShow()
    {
        $user = User::firstOrFail();

        $word = factory(Word::class)->create();

        $validStructureWithoutTranslation = [
            'id',
            'word',
            'word_language_id',
            'pronunciation',
            'example',
            'created_at',
            'updated_at',
            'translations' => [],
        ];

        $validStructure = [
            'id',
            'word',
            'word_language_id',
            'pronunciation',
            'example',
            'created_at',
            'updated_at',
            'translations' => [
                [
                    'id',
                    'translation',
                    'translation_language_id',
                    'word_id',
                    'created_at',
                    'updated_at',
                ],
            ],
        ];

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('GET', "api/vocabularies/{$word->id}");
        $response->seeStatusCode(401);

        // Case 2: See status code 404 when the vocabulary doesn't exist
        $response = $this->actingAs($user)->json('GET', "api/vocabularies/1000");
        $response->seeStatusCode(404);

        // Case 3: See valid json structure vocabulary without translation
        $response = $this->actingAs($user)->json('GET', "api/vocabularies/{$word->id}");
        $response->seeJsonStructure($validStructureWithoutTranslation);

        // Case 4: See valid json structure vocabulary
        $translation = factory(Translation::class)->create([
            'word_id' => $word->id,
        ]);
        $response = $this->actingAs($user)->json('GET', "api/vocabularies/{$word->id}");
        $response->seeJsonStructure($validStructure);
    }

    public function testCreateWordRequest()
    {
        $validStructure = [
            'id',
            'word',
            'word_language_id',
            'pronunciation',
            'example',
            'created_at',
            'updated_at',
        ];

        $user = User::firstOrFail();

        $word = factory(Word::class)->create();

        $data = [
            'word'               => $word->word,
            'word_language_id'   => $word->word_language_id,
            'pronunciation'      => $word->pronunciation,
            'example'            => $word->example,
        ];

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('POST', 'api/words', $data);
        $response->seeStatusCode(401);

        // Case 2: See valid json structure when success
        $response = $this->actingAs($user)->json('POST', 'api/words', $data);
        $response->seeJsonStructure($validStructure);
    }

    public function testCreateTranslationRequest()
    {
        $validStructure = [
            'id',
            'translation',
            'translation_language_id',
            'word_id',
            'created_at',
            'updated_at',
        ];

        $user = User::firstOrFail();

        $word = factory(Word::class)->create();

        $translation = factory(Translation::class)->create([
            'word_id' => $word->id,
        ]);

        $data = [
            'translation'             => $translation->translation,
            'translation_language_id' => $translation->translation_language_id,
        ];

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('POST', "api/words/{$word->id}/translations", $data);
        $response->seeStatusCode(401);

        // Case 2: See valid json structure when success
        $response = $this->actingAs($user)->json('POST', "api/words/{$word->id}/translations", $data);
        $response->seeJsonStructure($validStructure);
    }

    public function testIndex()
    {
        $validWithTranslationStructure = [
            [
                'id',
                'word',
                'word_language_id',
                'pronunciation',
                'example',
                'created_at',
                'updated_at',
                'pivot' => [
                    'vocabulary_list_id',
                    'word_id',
                    'is_favorite',
                    'created_at',
                    'updated_at',
                ],
                'translations' => [
                    [
                        'id',
                        'translation',
                        'translation_language_id',
                        'word_id',
                        'created_at',
                        'updated_at',
                    ],
                ],
            ]
        ];

        $validWithoutTranslationStructure = [
            [
                'id',
                'word',
                'word_language_id',
                'pronunciation',
                'example',
                'created_at',
                'updated_at',
                'pivot' => [
                    'vocabulary_list_id',
                    'word_id',
                    'is_favorite',
                    'created_at',
                    'updated_at',
                ],
                'translations' => [],
            ]
        ];

        $user = User::firstOrFail();

        $vocabularyList = factory(VocabularyList::class)->create([
            'user_id' => $user->id,
            'is_public' => false,
        ]);

        $word = factory(Word::class)->create();

        $vocabularyList->words()->attach($word->id, ['is_favorite' => true]);

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('GET', "api/vocabularyLists/{$vocabularyList->id}/vocabularies");
        $response->seeStatusCode(401);

        // Case 2: See status code 403 when the vocabulary list is private and the user of the vocabulary list is not the authorized user
        $user2 = User::findOrFail(2);
        $response = $this->actingAs($user2)->json('GET', "api/vocabularyLists/{$vocabularyList->id}/vocabularies");
        $response->seeStatusCode(403);

        // Case 3: See all vocabularies without translations
        $response = $this->actingAs($user)->json('GET', "api/vocabularyLists/{$vocabularyList->id}/vocabularies");
        $response->seeJsonStructure($validWithoutTranslationStructure);

        // Case 4: See all vocabularies with translation
        $translation = factory(Translation::class)->create([
            'word_id' => $word->id,
        ]);
        $response = $this->actingAs($user)->json('GET', "api/vocabularyLists/{$vocabularyList->id}/vocabularies");
        $response->seeJsonStructure($validWithTranslationStructure);
    }

    public function testUpdateWord()
    {
        $user = User::firstOrFail();

        $word = factory(Word::class)->create();

        $word2 = factory(Word::class)->create();

        $data = [
            'word'               => $word2->word,
            'word_language_id'   => $word2->word_language_id,
            'pronunciation'      => $word2->pronunciation,
            'example'            => $word2->example,
        ];

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('PUT', "api/words/{$word->id}", $data);
        $response->seeStatusCode(401);

        // Case 2: See response status code 404 when the word doesn't exist
        $response = $this->actingAs($user)->json('PUT', "api/words/1000", $data);
        $response->assertResponseStatus(404);

        // Case 3: See response status code 204 when success
        $response = $this->actingAs($user)->json('PUT', "api/words/{$word->id}", $data);
        $response->assertResponseStatus(204);

        // Case 4: Make sure data is updated properly
        $updatedWord = Word::findOrFail($word->id);
        $updatedData = [
            'word'               => $updatedWord->word,
            'word_language_id'   => $updatedWord->word_language_id,
            'pronunciation'      => $updatedWord->pronunciation,
            'example'            => $updatedWord->example,
        ];
        $this->assertEquals($data, $updatedData);
    }

    public function testUpdateTranslation()
    {
        $user = User::firstOrFail();

        $word = factory(Word::class)->create();

        $translation = factory(Translation::class)->create([
            'word_id' => $word->id,
        ]);

        $translation2 = factory(Translation::class)->create([
            'word_id' => $word->id,
        ]);

        $data = [
            'translation'             => $translation2->translation,
            'translation_language_id' => $translation2->translation_language_id,
        ];

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('PUT', "api/translations/{$translation->id}", $data);
        $response->seeStatusCode(401);

        // Case 2: See status code 404 when the translation doesn't exist
        $response = $this->actingAs($user)->json('PUT', "api/translations/1000", $data);
        $response->assertResponseStatus(404);

        // Case 3: See response status code 204 when success
        $response = $this->actingAs($user)->json('PUT', "api/translations/{$translation->id}", $data);
        $response->assertResponseStatus(204);

        // Case 4: Make sure data is updated properly
        $updatedTranslation = Translation::findOrFail($translation->id);
        $updatedData = [
            'translation'             => $translation2->translation,
            'translation_language_id' => $translation2->translation_language_id,
        ];
        $this->assertEquals($data, $updatedData);
    }

    public function testDeleteTranslation()
    {
        $user = User::firstOrFail();

        $word = factory(Word::class)->create();

        $translation = factory(Translation::class)->create([
            'word_id' => $word->id,
        ]);

        // Case 1: See status code 401 when unauthorized access
        $response = $this->json('DELETE', "api/translations/{$translation->id}");
        $response->seeStatusCode(401);

        // Case 2: See status code 404 when the translation doesn't exist
        $response = $this->actingAs($user)->json('DELETE', "api/translations/1000");
        $response->seeStatusCode(404);

        // Case 3: See response status code 204 when success
        $response = $this->actingAs($user)->json('DELETE', "api/translations/{$translation->id}");
        $response->assertResponseStatus(204);

        // Case 4: Make sure the translation is deleted
        $this->assertEquals(Translation::count(), 0);
    }
}
