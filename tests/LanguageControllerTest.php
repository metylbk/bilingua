<?php

class LanguageTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->seed('UserTableSeeder');
    }

    public function testIndexMethod()
    {
        $this->actingAs(App\User::find(1));

        $response = $this->json('GET', 'api/languages', [], $this->getHeaderWithAuthentication());
        $response->seeJsonStructure([
            ['id'],
        ]);
    }
}
