<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Notifications\Notifiable;



class User extends Authenticatable
{
    use SoftDeletes;
    use Messagable;
    use Notifiable;

    const SHIRO_MESSAGE_DAILY = 1;
    const SHIRO_MESSAGE_WEEKLY = 2;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'email_verified',
        'email_verify_code',
    ];

    protected $dates = ['deleted_at', 'last_active_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'deleted_at',
        //visible to user himself
        'phone', 'email', 'settings', 'google_id', 'facebook_id',
    ];

    public static function getEagerLoadRelationships()
    {
        return ['interests', 'spokenLanguages', 'learningLanguages', 'country'];
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function setBirthdayAttribute($birthday)
    {
        if (is_numeric($birthday)) {
            $this->attributes['birthday'] = Carbon::now()->subYears($birthday)->toDateString();
        } else {
            $this->attributes['birthday'] = $birthday;
        }

    }

    public function blockedUsers()
    {
        return $this->belongsToMany(self::class, 'blocks', 'sender_id', 'target_id')->withTimestamps();
    }

    public function blockedByUsers()
    {
        return $this->hasMany(Block::class, 'blocks', 'target_id', 'requester_id')->withTimestamps();
    }

    public function isBlockedBy(User $otherUser)
    {
        $idsOtherUserBlocks = $otherUser->blockedUsers()->lists('target_id');

        return in_array($this->id, $idsOtherUserBlocks, true);
    }

    public function logs()
    {
        return $this->belongsToMany(Log::class);
    }

    public function spokenLanguages()
    {
        return $this->belongsToMany(Language::class, 'spoken_language_user')
            ->withPivot('proficiency')
            ->withTimestamps();
    }

    public function learningLanguages()
    {
        return $this->belongsToMany(Language::class, 'learning_language_user')
            ->withPivot('proficiency')
            ->withTimestamps();
    }

    public function interests()
    {
        return $this->belongsToMany(Interest::class);
    }

    public function vocabularyLists()
    {
        return $this->hasMany(VocabularyList::class);
    }

    public function feedbacks()
    {
        return $this->hasMany(Feedback::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getAllInterestsNames()
    {
        return array_pluck($this->interests->toArray(), 'name');
    }

    public function getAllLearningLanguagesNames()
    {
        return array_pluck($this->learningLanguages->toArray(), 'name');
    }

    public function getAllSpokenLanguagesNames()
    {
        return array_pluck($this->spokenLanguages->toArray(), 'name');
    }

    public function getAllNativeLanguagesNames()
    {
        return array_pluck($this->spokenLanguages()->wherePivot('proficiency', 5)->get()->toArray(), 'name');
    }

    public function calcSortScore(User $anotherUser)
    {
        $score = 0;

        $matchInterests = array_intersect($this->getAllInterestsNames(), $anotherUser->getAllInterestsNames());
        $totalInterests = array_merge($this->getAllInterestsNames(), $anotherUser->getAllInterestsNames());
        if (!empty($totalInterests)) {
            $score += 20 * count($matchInterests) / count($totalInterests);
        }

        $matchLearningLanguages = array_intersect($this->getAllLearningLanguagesNames(), $anotherUser->getAllLearningLanguagesNames());
        $totalLearningLanguages = array_merge($this->getAllLearningLanguagesNames(), $anotherUser->getAllLearningLanguagesNames());
        if (!empty($totalLearningLanguages)) {
            $score += 50 * count($matchLearningLanguages) / count($totalLearningLanguages);
        }

        $matchSpokenLanguages = array_intersect($this->getAllSpokenLanguagesNames(), $anotherUser->getAllSpokenLanguagesNames());
        $totalSpokenLanguages = array_merge($this->getAllSpokenLanguagesNames(), $anotherUser->getAllSpokenLanguagesNames());
        if (!empty($totalSpokenLanguages)) {
            $score += 30 * count($matchSpokenLanguages) / count($totalSpokenLanguages);
        }
        //TODO: Fix amount for online status(50)

        $score -= 10 * log10(Carbon::now()->diffInSeconds($anotherUser->last_active_at));

        return $score;
    }

    /**
     * Message relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * Thread relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function threads()
    {
        return $this->belongsToMany(Thread::class)->withPivot(['last_read_at']);
    }

    public function activeThreads()
    {
        return $this->belongsToMany(Thread::class, 'thread_lists');
    }

    public function routeNotificationForSlack()
    {
        return 'https://hooks.slack.com/services/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';
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->family_name;
    }

    public function shiroWords()
    {
        return $this->belongsToMany(ShiroWord::class);
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class)->withPivot(['answer', 'expect', 'relevance']);
    }
}
