<?php

function getCountryListFromFile()
{
    $countries = json_decode(
        file_get_contents('app/countries.json'),
        true
    );

    return $countries;
}

function getQuestionListFromFile()
{
    $countries = json_decode(
        file_get_contents('app/questions.json'),
        true
    );

    return $countries;
}
