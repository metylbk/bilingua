<?php

namespace App\Http\Requests;

use Auth;
use App\VocabularyList;

class GetVocabulariesRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // If the user of the vocabulary list is not the authenticated user and the vocabulary list is private, we won't allow to get its vocabularies
        if (VocabularyList::findOrFail($this->route('id'))->is_public) {
            return true;
        }

        return Auth::user()->vocabularyLists->contains($this->route('id'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
