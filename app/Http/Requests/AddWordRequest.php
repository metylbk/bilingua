<?php

namespace App\Http\Requests;

use Auth;

class AddWordRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (! Auth::user()->vocabularyLists->contains($this->route('id'))) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_favorite' => 'boolean',
        ];
    }
}
