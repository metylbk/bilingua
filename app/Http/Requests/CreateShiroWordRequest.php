<?php

namespace App\Http\Requests;

class CreateShiroWordRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'word'                   => 'required',
            'word_language_id'       => 'required|integer',
            'definition'             => 'required',
            'definition_language_id' => 'required|integer',
            'level'                  => 'integer',
        ];
    }
}
