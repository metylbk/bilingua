<?php

namespace App\Http\Requests;

class GetUsersRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filter.interests'               => 'array',
            'filter.interests.*.id'          => 'distinct|exists:interests,id',
            'filter.min_age'                 => 'regex:1[3-9]|[2-5][0-9]',
            'filter.max_age'                 => 'regex:1[4-9]|[2-5][0-9]|-1|60',
            'filter.country_id'              => 'exists:countries,id',
            'filter.languages'               => 'array',
            'filter.languages.*.id'          => 'distinct|exists:languages,id',
            'filter.languages.*.proficiency' => 'between:1,5',
            'filter.profile_filled'          => 'boolean',
            'filter.introduction_filled'     => 'boolean',
        ];
    }
}
