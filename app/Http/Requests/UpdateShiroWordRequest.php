<?php

namespace App\Http\Requests;

use Auth;

class UpdateShiroWordRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'word_lang_id'           => 'integer',
            'definition_language_id' => 'integer',
            'level'                  => 'integer',
        ];
    }
}
