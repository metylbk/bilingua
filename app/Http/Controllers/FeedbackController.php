<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\CreateFeedbackRequest;
use App\Notifications\FeedbackReceived;

class FeedBackController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateFeedbackRequest $request)
    {
        $user = Auth::user();

        $feedbacks = $user->feedbacks()->create(['message' => $request->message]);

        $user->notify(new FeedbackReceived($user, $feedbacks));

        return response('', 204);
    }
}
