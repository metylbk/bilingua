<?php

namespace App\Http\Controllers;

use App\ShiroWord;
use Illuminate\Http\Request;
use App\Http\Requests\CreateShiroWordRequest;
use App\Http\Requests\UpdateShiroWordRequest;

class ShiroWordController extends Controller
{
    public function index()
    {
        $shiroWords = ShiroWord::get();

        return response()->json($shiroWords);
    }

    public function store(CreateShiroWordRequest $request)
    {
        ShiroWord::create($request->intersectKeys(ShiroWord::getFillableAttributes()));

        return response('', 204);
    }

    public function update($id, UpdateShiroWordRequest $request)
    {
        $shiroWord = ShiroWord::findOrFail($id);

        $shiroWord->update($request->intersectKeys(ShiroWord::getFillableAttributes()));

        return response('', 204);
    }

    public function delete($id)
    {
        $shiroWord = ShiroWord::findOrFail($id);

        $shiroWord->delete();

        return response('', 204);
    }
}
