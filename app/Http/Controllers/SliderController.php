<?php

namespace App\Http\Controllers;

use App\Slider;
use App\Http\Requests;

/**
 * @Resource("Sliders", uri="api/sliders")
 */
class SliderController extends Controller
{
    /**
     * Show all sliders.
     *
     * Get a JSON representation of all sliders.
     *
     * @Get("/")
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json"})
     * @Response(200, body="json:response/slider/index")
     */
    public function index()
    {
        return response()->json(Slider::latest()->get());
    }
}
