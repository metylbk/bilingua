<?php

namespace App\Http\Controllers;

use App\VocabularyList;
use App\Word;
use App\Translation;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\CreateWordRequest;
use App\Http\Requests\CreateTranslationRequest;
use App\Http\Requests\GetVocabulariesRequest;

class VocabularyController extends Controller
{
    public function index(GetVocabulariesRequest $request)
    {
        $vocabularies = VocabularyList::findOrFail($request->id)->words->load('translations');

        return response()->json($vocabularies);
    }

    public function storeWord(CreateWordRequest $request)
    {
        $word = Word::create($request->intersectKeys(Word::getFillableAttributes()));

        return response()->json(Word::find($word->id));
    }

    public function storeTranslation($id, CreateTranslationRequest $request)
    {
        $word = Word::findOrFail($id);

        $translation = $word->translations()->create($request->intersectKeys(Translation::getFillableAttributes()));

        return response()->json(Translation::find($translation->id));
    }

    public function show($id)
    {
        $vocabulary = Word::with('translations')->findOrFail($id);

        return response()->json($vocabulary);
    }

    public function updateWord($id, CreateWordRequest $request)
    {
        $word = Word::findOrFail($id);

        $word->update($request->intersectKeys(Word::getFillableAttributes()));

        return response('', 204);
    }

    public function updateTranslation($id, CreateTranslationRequest $request)
    {
        $translation = Translation::findOrFail($id);

        $translation->update($request->intersectKeys(Translation::getFillableAttributes()));

        return response('', 204);
    }

    public function deleteTranslation($id)
    {
        $translation = Translation::findOrFail($id);

        $translation->delete();

        return response('', 204);
    }
}
