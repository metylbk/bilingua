<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use App\User;
use Socialite;
use Carbon\Carbon;
use App\Http\Requests\JsonRequest;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\CheckEmailRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\SocialLoginRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Authenticate Handler (Token Generator).
 *
 * @Resource("Authenticate", uri="api/auth")
 */
class AuthenticateController extends Controller
{
    protected $socialLoginProviderIdentifiers = ['facebook_id', 'google_id'];

    public function checkEmail(CheckEmailRequest $request)
    {
        if (!count(User::whereEmail($request->email)->get())) {
            throw new NotFoundHttpException('email not found');
        }

        return response('', 204);
    }

    /**
     * Login.
     *
     * Get a Token for User.
     *
     * @Post("/login")
     *
     * @Request({"email": "email@example.com", "password": "123456"}, headers={"Accept": "application/vnd.bilingua.v1+json"})
     * @Response(200, body="json:response/authenticate/token")
     */
    public function login(LoginUserRequest $request)
    {
        if (! Auth::attempt($request->all())) {
            abort(422);
        }

        $user = Auth::user();
        activity()->causedBy($user)->withProperties(['method' => 'email'])->log('Login');

        // Update consecutive login days and Shiro message frequency for the user
        $consecutiveLoginDays = $user->consecutive_login_days;
        $shiroMessageFrequency = $user->shiro_message_frequency;

        if ($user->last_active_at < Carbon::yesterday()) {
            $consecutiveLoginDays = 1;
        } else {
            $consecutiveLoginDays += 1;
        }

        if ($shiroMessageFrequency == User::SHIRO_MESSAGE_WEEKLY && $consecutiveLoginDays == 5) {
            $user->update(['shiro_message_frequency' => User::SHIRO_MESSAGE_DAILY]);
        }

        if ($shiroMessageFrequency == User::SHIRO_MESSAGE_DAILY && $user->last_active_at < Carbon::today()->subWeek()) {
            $user->update(['shiro_message_frequency' => User::SHIRO_MESSAGE_WEEKLY]);
        }

        $user->update(['consecutive_login_days'  => $consecutiveLoginDays]);

        return response()->json(['token' => '']);
    }

    /**
     * Registration.
     *
     * Get a Token and create a new User.
     *
     * @Post("/reg")
     *
     * @Request({"email":"email@example.com","password":"123456"}, headers={"Accept": "application/vnd.bilingua.v1+json"}),
     * @Response(200, body="json:response/authenticate/token")
     */
    public function reg(CreateUserRequest $request)
    {
        $user = User::create($request->all());

        $user = User::find($user->id);

        $email_verify_code = str_random(32);
        $user->email_verify_code = $email_verify_code;
        $user->save();
        $data = [
            'confirm_link' => 'https://app.bilingua.io/email/verify/' . $email_verify_code,
            'username' => $user->username ?? '',
        ];

        Mail::queueOn('bilingua', 'emails.email-verification', $data, function ($m) use ($user) {
            $m->to($user->email);
            $m->subject('Please confirm your email address!');
        });

        Auth::login($user);

        return response()->json(['token' => '']);
    }

    /**
     * Check Token.
     *
     * Check Token validity.
     *
     * @Post("/check")
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json"})
     * @Response(204)
     */
    public function check()
    {
        return Auth::check() ? response('', 204) : abort(401);
    }

    /**
     * Oauth login.
     *
     * Get a Token for Oauth authenticating User.
     *
     * @Post("/social")
     * @Transaction({
     *     @Request({"email":"email@example.com","google_id":"1234567890"}, identifier="Google Sign-in", headers={"Accept": "application/vnd.bilingua.v1+json"}),
     *     @Request({"email":"email@example.com","facebook_id":"1234567890"}, identifier="Facebook Sign-in", headers={"Accept": "application/vnd.bilingua.v1+json"}),
     *     @Response(200, body="json:response/authenticate/token")
     * })
     */
    public function oauth(SocialLoginRequest $request)
    {
        $authUser = Socialite::driver($request->provider)->userFromToken($request->token);

        $user = User::firstOrCreate(['email' => $authUser->getEmail()]);

        activity()->causedBy($user)->withProperties(['method' => $request->provider])->log('Login');

        Auth::login($user);

        return response()->json(['token' => '']);
    }

    public function logout()
    {
        Auth::logout();

        return response('', 204);
    }
}
