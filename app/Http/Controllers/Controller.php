<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use App\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getUserFromToken()
    {
        if (! Auth::check()) {
            abort(401);
        }

        $user = Auth::user()->load(User::getEagerLoadRelationships());
        $user->update(['last_active_at' => Carbon::now()]);
        return $user;

    }
}
