<?php

namespace App\Http\Controllers;

use App\Meta;
use App\Http\Requests\GetMetasRequest;

/**
 * @Resource("Metas", uri="api/metas")
 */
class MetaController extends Controller
{
    /**
     * Metas.
     *
     * Get all metas by category id.
     *
     * @Get("/{?category}")
     * @Parameters({
     *     @Parameter("category", description="The id of category")
     * })
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json"})
     * @Response(200, body="json:response/metas/index")
     */
    public function index(GetMetasRequest $request)
    {
        $fields = ['category'];

        $query = Meta::query();

        foreach ($fields as $field) {
            if ($request->exists($field)) {
                $query = $query->where($field, $request->$field);
            }
        }

        $meta = $query->get();

        return response()->json($meta);
    }

    /**
     * Get meta.
     *
     * Get a meta by id.
     *
     * @Get("/{id}")
     * @Parameters({
     *     @Parameter("id", description="The id of meta", required=true)
     * })
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json"})
     * @Response(200, body="json:response/metas/index")
     */
    public function getById($id)
    {
        return response()->json(Meta::find($id));
    }
}
