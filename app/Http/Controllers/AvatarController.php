<?php

namespace App\Http\Controllers;

use Image;
use App\Http\Requests;

/**
 * No Public Api For Avatars Now.
 * 
 * @Resource("Avatar", uri="/avatars")
 */
class AvatarController extends Controller
{
    public function show($id)
    {
        $img = Image::make(storage_path() . '/app/avatars/' . $id . '.jpg');

        return $img->response('jpg');
    }
    
    public function showCoverImage($id)
    {
        $img = Image::make(storage_path() . '/app/covers/' . $id . '.jpg');

        return $img->response('jpg');
    }
}
