<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendInviteRequest;
use Illuminate\Http\Request;
use Mail;

class InviteController extends Controller
{
    public function invite(SendInviteRequest $request)
    {
        $user = $this->getUserFromToken();

        foreach ($request->emails as $email) {
            $name = $user->first_name ?? '';
            $name .= ' ';
            $name .= $user->family_name ?? '';

            Mail::queueOn('bilingua', 'emails.invitation', ['name' => $name], function ($m) use ($email) {
                $m->to($email);
                $m->subject('You\'re invited!');
            });
        }


        return response('', 204);
    }
}
