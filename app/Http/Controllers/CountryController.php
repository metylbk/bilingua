<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Requests;

class CountryController extends Controller
{

    public function index()
    {
        $countries = Country::all();

        return response()->json($countries);
    }
}
