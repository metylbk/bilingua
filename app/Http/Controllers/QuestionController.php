<?php

namespace App\Http\Controllers;

use App\Question;
use Auth;
use App\User;

use App\Http\Requests\SaveUserQuestionRequest;

class QuestionController extends Controller
{
    public function getAllQuestions()
    {
        return response()->json(Question::all());
    }

    public function getUserQuestions()
    {
        return response()->json(Auth::user()->questions);
    }

    public function saveUserQuestionAnswer($id, SaveUserQuestionRequest $request)
    {
        $user = Auth::user();

        $user->questions()->syncWithoutDetaching([$id]);

        $user->questions()->updateExistingPivot($id, [
           'answer' => $request->answer,
           'expect' => $request->expect,
           'relevance' => $request->relevance,
        ]);

        return response('', 204);
    }

    public function getMatchScore($id)
    {
        $anotherUser = User::findOrFail($id);

        $authUserQuestionsIds = Auth::user()->questions()->getRelatedIds();

        $anotherUserQuestionsIds = $anotherUser->questions()->getRelatedIds();

        $mutualIds = $authUserQuestionsIds->intersect($anotherUserQuestionsIds);

        $matchscore = $mutualIds->reduce(function ($carry, $questionId) use ($anotherUser) {
            $questionData = Auth::user()->questions()->find($questionId);

            $expect = $questionData->pivot->expect;

            $answer = $anotherUser->questions()->find($questionId)->pivot->answer;

            $relevance = $questionData->pivot->relevance;

            $isMatch = str_contains($expect, $answer);

            return $carry + $isMatch ? $relevance : 0;
        }, 0);

        if (! $mutualIds->isEmpty()) {
            $matchscore = $matchscore * ( 1 - 1 / $mutualIds->count());
        }

        return response($matchscore);
    }
}
