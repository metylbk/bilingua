<?php
namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Thread;
use App\Message;
use Carbon\Carbon;
use App\Events\HaveNewMessageEvent;
use App\Events\MessageHasEmailEvent;
use App\Events\NewThreadMessageEvent;
use App\Events\MessageSentToManyEvent;
use App\Events\NewMessageToShiroEvent;
use App\Http\Requests\CreateThreadRequest;
use App\Http\Requests\CreateMessageRequest;
use App\Jobs\SendChatMessageReceivedEmailAlert;
use App\Notifications\NewThreadMessageReceived;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MessageController extends Controller
{
    public function index()
    {
        return response()->json(Auth::user()->activeThreads->load('users', 'users.country', 'users.interests', 'users.spokenLanguages', 'users.learningLanguages'));
    }

    public function show($id)
    {
        if( !Auth::user()->threads->contains($id)) {
            abort(403);
        }

        $thread = Thread::with('messages', 'users')->find($id);

        return response()->json($thread);
    }

    public function store(CreateThreadRequest $request)
    {
        $recipients = $request->recipients;
        $user = Auth::user();

        if(count($recipients) == 1) {
            $recipient = $recipients[0];

            $anotherUser = User::findOrFail($recipient);

            $type = $recipient > $user->id ? $user->id.'-'.$recipient : $recipient.'-'.$user->id;

            $thread = $user->threads()->firstOrCreate(['type' => $type]);

            $user->activeThreads()->syncWithoutDetaching([$thread->id]);

            $thread->users()->syncWithoutDetaching([$anotherUser->id]);

            return response()->json(Thread::find($thread->id));
        } else {
            //$thread = $user->threads()->create();
            //TODO: group chat
            /*
                // Recipients
                if ($request->has('recipients')) {
                    $thread->addParticipant($input['recipients']);

                    foreach($request->recipients as $recipient) {
                        User::findOrFail($recipient)->notify(new NewThreadMessageReceived(Message::find($message->id)));
                    }
                }

                return Thread::find($thread->id);
            */

            return response('', 204);
        }
    }

    public function update($id, CreateMessageRequest $request)
    {
        if( !Auth::user()->threads->contains($id)) {
            abort(403);
        }

        $thread = Thread::findOrFail($id);

        // Message
        $message = Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::id(),
                'body'      => $request->message,
            ]
        );

        // If this message is sent to Shiro bot, we will handle it
        $shiroId = env('SHIRO_ID');
        $participantIds = explode('-', $thread->type);

        if (in_array($shiroId, $participantIds)) {
            broadcast(new NewMessageToShiroEvent($thread, Message::find($message->id)));
        } else {
            foreach($thread->users as $user) {
                $user->activeThreads()->syncWithoutDetaching([$thread->id]);

                $user->notify(new NewThreadMessageReceived($thread, Message::find($message->id)));

                if ($user->id != Auth::id()) {
                    $job = (new SendChatMessageReceivedEmailAlert(Auth::user(), $user, $thread, $message))->delay(Carbon::now()->addMinutes(15));

                    dispatch($job);
                }
            }

            if ($this->messageBodyHasEmail($message->body)){
                event(new MessageHasEmailEvent(Message::find($message->id)));
            }

            if (User::find(Auth::id())->messages->where('body' , $request->message)->count() > 3){
                event(new MessageSentToManyEvent(Auth::user()));
            }

            broadcast(new NewThreadMessageEvent($thread, Message::find($message->id)));
        }

        return response('', 204);
    }

    public function updateLastReadAt($id)
    {
        if( !Auth::user()->threads->contains($id)) {
            abort(403);
        }

        Auth::user()->threads()->updateExistingPivot($id, ['last_read_at' => Carbon::now()]);

        return response('', 204);
    }

    public function deleteThreadFromList($id)
    {
        if( !Auth::user()->activeThreads->contains($id)) {
            abort(403);
        }

        Auth::user()->activeThreads()->detach($id);

        return response('', 204);
    }

    public function messageBodyHasEmail($message){
        if (strpos($message, '@') == false){
            return false;
        }
        $messageArray =  explode(" ", $message);
        for ($i = 0; $i < count($messageArray); $i++){
            if (filter_var($messageArray[$i], FILTER_VALIDATE_EMAIL) !== false){
                return true;
            }
        }
        return false;
    }
}
