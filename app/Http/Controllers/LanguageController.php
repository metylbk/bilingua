<?php

namespace App\Http\Controllers;

use App\Language;
use App\Http\Requests\CreateLanguagesRequest;
use App\Http\Requests\UpdateProficiencyRequest;

/**
 * @Resource("Languages", uri="api/languages")
 */
class LanguageController extends Controller
{
    /**
     * Get all languages.
     *
     * Get all languages
     *
     * @GET("/")
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/languages/index")
     */
    public function index()
    {
        $languages = Language::all();

        return response()->json($languages);
    }
}
