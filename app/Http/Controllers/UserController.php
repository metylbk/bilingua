<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Image;
use App\Http\Requests\UploadAvatarRequest;
use Storage;
use App\Http\Requests\CreateLanguagesRequest;
use App\Http\Requests\GetUsersRequest;
use Illuminate\Http\Request;
use App\Language;
use App\User;
use Auth;
use App\Block;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\AddInterestToUserRequest;
use App\Http\Requests\UpdateUserSettingsRequest;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * @Resource("Users", uri="api/users")
 */
class UserController extends Controller
{
    /**
     * Get User.
     *
     * Get a User.
     *
     * @Get("/{?email}")
     * @Parameters({
     *     @Parameter("email", description="Email of User")
     * })
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json"})
     * @Response(200, body="json:response/user/show")
     */
    public function index(GetUsersRequest $request)
    {
        $fields = ['email', 'name'];

        $query = User::query()
            ->where('first_name', '!=', null)
            ->where('family_name', '!=', null)
            ->where('avatar', '!=', null)
            ->where('birthday', '!=', null);

        foreach ($fields as $field) {
            if ($request->exists($field)) {
                if ($field == 'name') {
                    $query = $query->where('first_name', $request->name)
                        ->orWhere('family_name', $request->name);
                } else {
                    $query = $query->where($field, $request->$field);
                }
            }
        }
        $query = $query->orderBy('last_active_at','desc');
        $users = $query->get();
        $users = $users->take(50);
        //$users = $this->sortUserList($users);
        return response()->json($users);
    }

    protected function sortUserList($users)
    {
        $authUser = $this->getUserFromToken();

        if ($users instanceof User) {
            return [$users];
        }

        $users = $users->filter(function ($user) use ($authUser) {
            $settings = json_decode($user->settings, true);

            if ($settings['visibility_same_gender'] ?? false) {
                if ($authUser->gender != $user->gender) {
                    return false;
                }
            }

            if ($settings['visibility_min_age'] ?? false) {
                if (Carbon::now()->diffInYears(Carbon::parse($authUser->birthday)) < $settings['visibility_min_age']) {
                    return false;
                }
            }

            if ($settings['visibility_max_age'] ?? false) {
                if (Carbon::now()->diffInYears(Carbon::parse($authUser->birthday)) > $settings['visibility_max_age']) {
                    return false;
                }
            }

            if ($settings['visibility_speaking_native_language_match'] ?? false) {
                $userLearningLanguagesNames = $user->getAllLearningLanguagesNames();
                $authUserNativeLanguagesNames = $authUser->getAllNativeLanguagesNames();

                $matchLanguagesNames = array_intersect($userLearningLanguagesNames, $authUserNativeLanguagesNames);

                if (empty($matchLanguagesNames)) {
                    return false;
                }
            }

            if ($settings['visibility_learning_native_language_match'] ?? false) {
                $authUserLearningLanguagesNames = $authUser->getAllLearningLanguagesNames();
                $userNativeLanguagesNames = $user->getAllNativeLanguagesNames();

                $matchLanguagesNames = array_intersect($authUserLearningLanguagesNames, $userNativeLanguagesNames);

                if (empty($matchLanguagesNames)) {
                    return false;
                }
            }

            if ($settings['visibility_same_country'] ?? false) {
                if ($authUser->country_id != $user->country_id) {
                    return false;
                }
            }

            //TODO:visibility_location_match,visibility_max_distance

            return true;
        });

        $users = $users->sortByDesc(function ($user) use ($authUser) {
            return $authUser->calcSortScore($user);
        });

        return $users->values()->all();
    }

    /**
     * Me.
     *
     * Get authenticating User profile.
     *
     * @Get("/me")
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/user/me")
     */
    public function me(Request $request)
    {
        $user = Auth::user()->load(User::getEagerLoadRelationships())->makeVisible(['phone', 'email', 'settings', 'google_id', 'facebook_id']);

        return response()->json($user);
    }

    /**
     * Show User.
     *
     * Get User profile.
     *
     * @Get("/{id}")
     * @Parameters({
     *     @Parameter("id", description="The id of User", required=true)
     * })
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/user/show")
     */
    public function show($id)
    {
        $user = User::with(User::getEagerLoadRelationships())->findOrFail($id);

        return response()->json($user);
    }

    /**
     * Show blocked users.
     *
     * Get blocked users.
     *
     * @Get("/blocked")
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/user/blocked")
     */
    public function blocked()
    {
        $users = $this->getUserFromToken()->blockedUsers()->get();

        return response()->json($users);
    }

    /**
     * Block user.
     *
     * Block a user.
     *
     * @Post("/{id}/block")
     * @Parameters({
     *     @Parameter("id", description="The id of User", required=true)
     * })
     *
     * @Request({"reason":"Fake profile."}, headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(204)
     */
    public function block(Request $request, $id)
    {
        $user = $this->getUserFromToken();
        $user->blockedUsers()->attach($id, ['reason' => $request->reason]);

        return response('', 204);
    }

    /**
     * Unblock user.
     *
     * Unblock a user.
     *
     * @Post("/{id}/unblock")
     * @Parameters({
     *     @Parameter("id", description="The id of User", required=true)
     * })
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(204)
     */
    public function unblock($id)
    {
        $user = $this->getUserFromToken();
        $user->blockedUsers()->detach($id);

        return response('', 204);
    }

    /**
     * Update User.
     *
     * Update authenticating User profile.
     *
     * @Patch("/me")
     *
     * @Request({"key1":"value1","key2":"value2"}, headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/user/update")
     */
    public function update(UpdateUserRequest $request)
    {
        $user = $this->getUserFromToken();
        $user->update($request->all());

        return response()->json($user);
    }

    /**
     * Sync User Interest.
     *
     * Update authenticating User profile.
     *
     * @Put("/me/interests")
     *
     * @Request({"interests":{{"id":1},{"id":2},{"id":3}}}, headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/user/syncInterest")
     */
    public function syncInterest(AddInterestToUserRequest $request)
    {
        $interestsIds = collect($request->interests)->pluck('id')->toArray();

        $this->getUserFromToken()->interests()->sync($interestsIds);

        return response()->json(User::find(Auth::id()));
    }

    /**
     * Sync user languages.
     *
     * Sync user languages.
     *
     * @PUT("/me/languages")
     *
     *
     * @Request({"languages": {{"id": 1, "proficiency": 1}, {"id": 2, "proficiency": 5}}, "type":1}, headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(204)
     */
    public function syncLanguages(CreateLanguagesRequest $request)
    {
        $relationFunctionName = [
            Language::TYPE_LEARNING => 'learningLanguages',
            Language::TYPE_SPOKEN   => 'spokenLanguages',
        ];

        $data = [];

        //$request->languages is protected property in the class
        $languages = $request->input('languages');

        if (count($languages) > 5) {
            throw new UnprocessableEntityHttpException('too many languages');
        }

        foreach ($languages as $language) {
            $data[$language['id']] = ['proficiency' => $language['proficiency']];
        }

        $functionName = $relationFunctionName[$request->type];

        $this->getUserFromToken()->$functionName()->sync($data);

        return response('', 204);
    }

    /**
     * Get settings.
     *
     * Get user settings.
     *
     * @Get("/me/settings")
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/user/getSettings")
     */
    public function getSettings()
    {
        return response($this->getUserFromToken()->settings);
    }

    /**
     * Update settings.
     *
     * Update user settings.
     *
     * @Patch("/me/settings")
     *
     * @Request({"key1": "value1", "key2": "value2"}, headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/user/updateSettings")
     */
    public function updateSettings(UpdateUserSettingsRequest $request)
    {
        $user     = $this->getUserFromToken();
        $settings = json_decode($user->settings, true);

        foreach ($request->all() as $key => $value) {
            $settings[$key] = $value;
        }

        $user->settings = json_encode($settings);
        $user->save();

        return response($user->settings);
    }

    /**
     * Upload avatar.
     *
     * Upload avatar
     *
     * @Post("/me/avatar")
     *
     * @Request({"_comment": "this is not a json request, please use default file request with id 'image'"}, headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(204)
     */
    public function uploadAvatar(UploadAvatarRequest $request)
    {
        $user = $this->getUserFromToken();

        $img = Image::make($request->file('image'));
        if ($img->height() >= $img->width())
        {
            $img->resize(400, null , function ($constraint) {
                $constraint->aspectRatio();
            });
        }else {
            $img->resize(null, 400,function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $img->save(storage_path() . '/app/avatars/' . $user->id . '.jpg');

        $user->avatar = 'https://' . $request->getHttpHost() . '/avatars/' . $user->id;

        $user->save();

        return response('', 204);
    }

    /**
     * Upload cover image.
     *
     * Upload cover image
     *
     * @Post("/me/images/cover")
     *
     * @Request({"_comment": "this is not a json request, please use default file request with id 'image'"}, headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(204)
     */
    public function uploadCoverImage(UploadAvatarRequest $request)
    {
        $user = $this->getUserFromToken();

        $img = Image::make($request->file('image'));
        if ($img->height() >= $img->width())
        {
            $img->resize(400, null , function ($constraint) {
                $constraint->aspectRatio();
            });
        }else {
            $img->resize(null, 400,function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $img->save(storage_path() . '/app/covers/' . $user->id . '.jpg');

        $user->cover_image = 'https://' . $request->getHttpHost() . '/images/covers/' . $user->id;

        $user->save();

        return response('', 204);
    }

    /**
     * Delete User.
     *
     * Delete User
     *
     * @Delete("/me")
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(204)
     */
    public function destroy()
    {
        $this->getUserFromToken()->delete();

        return response('', 204);
    }

    public function verifyEmail($code)
    {
        $user = User::where('email_verify_code', $code)->first();

        if (! is_null($user)) {
            $user->email_verified = 1;
            $user->save();
        }

        return redirect()->to('https://bilingua.io');
    }
}
