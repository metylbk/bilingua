<?php

namespace App\Http\Controllers;

use App\Interest;
use Illuminate\Http\Request;
use App\Http\Requests\CreateInterestRequest;

/**
 * @Resource("Interests", uri="api/interests")
 */
class InterestController extends Controller
{
    /**
     * Get interests.
     *
     * Get all interests.
     *
     * @Get("/")
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/interests/index")
     */
    public function index()
    {
        $interests = Interest::all()
            ->sortByDesc('interest_user_count')
            ->values()
            ->all();

        return response()->json($interests);
    }

    /**
     * Store an interest.
     *
     * Add authenticating user interest.
     *
     * @Post("/")
     * @Request({"name": "interest1"}, headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/interests/store")
     */
    public function store(CreateInterestRequest $request)
    {
        $interest = Interest::create(['name' => $request->input('name')]);

        return response()->json($interest);
    }

    /**
     * Hottest interests.
     *
     * Get a list of interests ordered by the number of users interested in.
     *
     * @Get("/hottest")
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/interests/hottest")
     */
    public function hottest()
    {
        $sortFunction = function ($interest) {
            return $interest->users->count();
        };

        $interests = Interest::with('users')->get()->sortByDesc($sortFunction);

        return response()->json($interests);
    }

    /**
     * Search interests.
     *
     * Search interests given a query ordered by the number of users interested in.
     *
     * @Get("/search/{name}")
     * @Parameters({
     *     @Parameter("name", description="A query", required=true)
     * })
     *
     * @Request(headers={"Accept": "application/vnd.bilingua.v1+json", "Authorization": "Bearer <token_here>"})
     * @Response(200, body="json:response/interests/search")
     */
    public function search($name)
    {
        $sortFunction = function ($interest) {
            return $interest->users->count();
        };

        $interests = Interest::where('name', 'like', '%' . $name . '%')->with('users')->get()->sortByDesc($sortFunction);

        return response()->json($interests);
    }
}
