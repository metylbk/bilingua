<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVocabularyListRequest;
use App\Http\Requests\UpdateVocabularyListRequest;
use App\Http\Requests\DeleteVocabularyListRequest;
use App\Http\Requests\AddWordRequest;
use App\User;
use App\VocabularyList;
use Auth;

class VocabularyListController extends Controller
{
    public function index($id)
    {
        $user = User::findOrFail($id);

        // Add default lists for user who does not have any list
        if (! $user->vocabularyLists->count()) {
            $user->vocabularyLists()->createMany(VocabularyList::$defaultLists);
        }

        // Response all public and private vocabulary lists if user views his profile
        // Otherwise response only public vocabulary lists
        if (Auth::id() == $id) {
            $vocabularyLists = $user->vocabularyLists()->get();
        } else {
            $vocabularyLists = $user->vocabularyLists()->whereIsPublic(true)->get();
        }

        return response()->json($vocabularyLists);
    }

    public function store(CreateVocabularyListRequest $request)
    {
        $vocabularyList = Auth::user()->vocabularyLists()->create($request->intersectKeys(VocabularyList::getFillableAttributes()));

        return response()->json(VocabularyList::find($vocabularyList->id));
    }

    public function show($id)
    {
        $vocabularyList = VocabularyList::findOrFail($id);

        return response()->json($vocabularyList);
    }

    public function update($id, UpdateVocabularyListRequest $request)
    {
        $vocabularyList = VocabularyList::findOrFail($id);

        $vocabularyList->update($request->intersectKeys(VocabularyList::getFillableAttributes()));

        return response('', 204);
    }

    public function invertPublic($id, UpdateVocabularyListRequest $request)
    {
        $vocabularyList = VocabularyList::findOrFail($id);

        $vocabularyList->is_public = ! $vocabularyList->is_public;

        $vocabularyList->save();

        return response('', 204);
    }

    public function delete($id, DeleteVocabularyListRequest $request)
    {
        $vocabularyList = VocabularyList::findOrFail($id);

        $vocabularyList->words()->detach();

        $vocabularyList->delete();

        return response('', 204);
    }

    public function addWord($id, $wordId, AddWordRequest $request)
    {
        $vocabularyList = VocabularyList::findOrFail($id);

        $vocabularyList->words()->attach($wordId, $request->intersectKeys(VocabularyList::getFillablePivotAttributes()));

        return response('', 204);
    }

    public function removeWord($id, $wordId, AddWordRequest $request)
    {
        $vocabularyList = VocabularyList::findOrFail($id);

        $vocabularyList->words()->detach($wordId);

        return response('', 204);
    }

    public function invertFavoriteWord($id, $wordId, AddWordRequest $request)
    {
        $vocabularyList = VocabularyList::findOrFail($id);

        $currentIsFavorite = $vocabularyList->words()->whereWordId($wordId)->first()->pivot->is_favorite;

        $vocabularyList->words()->updateExistingPivot($wordId, ['is_favorite' => ! $currentIsFavorite]);

        return response('', 204);
    }
}
