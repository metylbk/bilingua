<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use Mail;
use Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Request::macro('intersectKeys', function ($keys) {
            return array_intersect_key($this->all(), array_flip($keys));
        });

        User::created(function ($user) {
            $user = User::find($user->id);
            Mail::queueOn('bilingua', 'emails.registration-welcome', ['name' => $user->name], function ($m) use ($user) {
                $m->to($user->email);
                $m->subject('You are very special!');
            });
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
