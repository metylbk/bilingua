<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;
use App\Thread;


class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Broadcast::routes();

        /*
         * Authenticate the user's personal channel...
         */
        Broadcast::channel('App.User.*', function ($user, $userId) {
            return (int) $user->id === (int) $userId;
        });

        Broadcast::channel('chat.thread.*', function ($user, $threadId) {
            if (Thread::findOrFail($threadId)->users->contains($user->id)) {
                return ['thread' => Thread::find($threadId), 'user' => $user];
            } else {
                return false;
            }
        });
    }
}
