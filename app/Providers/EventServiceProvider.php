<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\NewThreadMessageEvent' => [
            'App\Listeners\NewThreadMessageEventListener',
        ],
        'App\Events\HaveNewMessageEvent' => [
            'App\Listeners\HaveNewMessageEventListener',
        ],
        'App\Events\MessageHasEmailEvent' => [
            'App\Listeners\MessageHasEmailEventListener'
        ],
        'App\Events\MessageSentToManyEvent' => [
            'App\Listeners\MessageSentToManyEventListener'
        ],
        'App\Events\NewMessageToShiroEvent' => [
            'App\Listeners\NewMessageToShiroEventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
