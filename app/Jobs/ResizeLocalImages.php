<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Image;

class ResizeLocalImages implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $url;

    /**
     * Create a new job instance.
     * @param $dir: location of the folder in which images are.
     * @return void
     */
    public function __construct($dir)
    {
        $this->url = $dir;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($dir)
    {
        $response_array = array();
        if (is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false)
                {
                    if ($file == '.' or $file == '..' or $file == '.gitignore') continue;
                    $image_url = $dir . $file;
                    $img = Image::make($image_url);
                    if ($img->height() >= $img->width())
                    {
                        $img->resize(400, null , function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else {
                        $img->resize(null, 400,function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $img->save($dir . $file);
                    $response_array[] = 'done resizing: ' . $file;
                }
                closedir($dh);
                Log::info(print_r($response_array, true));
            }
        }else{
            Log::info('folder not found');
        }
    }
}
