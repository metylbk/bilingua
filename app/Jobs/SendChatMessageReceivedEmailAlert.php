<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\ChatNewMessageAlertEmail;
use App\EmailLog;
use Carbon\Carbon;
use App\Thread;

class SendChatMessageReceivedEmailAlert implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $sender;

    public $receiver;

    public $thread;

    public $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sender, $receiver, $thread, $message)
    {
        $this->sender = $sender;

        $this->receiver = $receiver;

        $this->thread = $thread;

        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $count = EmailLog::whereUserId($this->receiver->id)->whereType('chat-new-message-alert')->where('created_at', '>=', Carbon::now()->subDays(1))->count();

        if ($count == 0) {

            $user = Thread::find($this->thread->id)->users()->withPivot('last_read_at')->whereUserId($this->receiver->id)->first();

            if ($user->pivot->last_read_at ?? Carbon::now()->subMinutes(16) <= Carbon::now()->subMinutes(15)) {

                Mail::to($this->receiver->email)->send(new ChatNewMessageAlertEmail($this->sender, $this->receiver, $this->message->body));
                EmailLog::forceCreate([
                    'user_id' => $this->receiver->id,
                    'type' => 'chat-new-message-alert',
                ]);

            }

        }

        \Log::info(EmailLog::all());

    }
}
