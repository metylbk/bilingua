<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $fillable = [
        'translation',
        'translation_language_id',
        'word_id',
    ];

    public static function getFillableAttributes()
    {
        return [
            'translation',
            'translation_language_id',
            'word_id',
        ];
    }

    public function word()
    {
        return $this->belongsTo(Word::class);
    }
}
