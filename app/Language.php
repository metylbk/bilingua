<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    const TYPE_LEARNING = 1;
    const TYPE_SPOKEN   = 2;

    protected $hidden = ['users', 'created_at', 'updated_at'];

    protected $guarded = [];

    public function usersSpeaking()
    {
        return $this->belongsToMany(User::class, 'spoken_language_user');
    }

    public function usersLearning()
    {
        return $this->belongsToMany(User::class, 'learning_language_user');
    }
}
