<?php

namespace App\Listeners;

use App\Events\NewThreadMessageEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewThreadMessageEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewThreadMessageEvent  $event
     * @return void
     */
    public function handle(NewThreadMessageEvent $event)
    {
        //
    }
}
