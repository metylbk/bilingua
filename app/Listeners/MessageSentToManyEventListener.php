<?php

namespace App\Listeners;

use App\Events\MessageSentToManyEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageSentToManyEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageSentToManyEvent  $event
     * @return void
     */
    public function handle(MessageSentToManyEvent $event)
    {
        //
    }
}
