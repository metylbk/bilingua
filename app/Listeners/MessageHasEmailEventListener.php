<?php

namespace App\Listeners;

use App\Events\MessageHasEmailEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageHasEmailEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageHasEmailEvent  $event
     * @return void
     */
    public function handle(MessageHasEmailEvent $event)
    {
        //
    }
}
