<?php

namespace App\Listeners;

use App\Message;
use App\Events\NewMessageToShiroEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\NewThreadMessageReceived;

class NewMessageToShiroEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewThreadMessageEvent  $event
     * @return void
     */
    public function handle(NewMessageToShiroEvent $event)
    {
        $shiroId = env('SHIRO_ID');
        $thread  = $event->thread;
        $message = $event->message;
        $user    = $message->user;

        $lastShiroWord = $user->shiroWords()->orderBy('updated_at', 'desc')->first();

        if (! $lastShiroWord->pivot->is_answered) {
            $correctAnswers = explode(',', $lastShiroWord->correct_answers);

            if (in_array($message->body, $correctAnswers)) {
                $correctMessage = $thread->messages()->create(
                    [
                        'user_id' => $shiroId,
                        'body'    => 'Wow, great job!',
                    ]
                );

                $user->shiroWords()->updateExistingPivot($lastShiroWord->id, ['is_answered' => true]);

                $user->notify(new NewThreadMessageReceived($thread, Message::find($correctMessage->id)));

                if ($lastShiroWord->fun_fact) {
                    $funFactMessage = $thread->messages()->create(
                        [
                            'user_id' => $shiroId,
                            'body'    => $lastShiroWord->fun_fact,
                        ]
                    );

                    $user->notify(new NewThreadMessageReceived($thread, Message::find($funFactMessage->id)));
                }

                $user->activeThreads()->syncWithoutDetaching([$thread->id]);
            } else {
                $incorrectMessage = $thread->messages()->create(
                    [
                        'user_id' => $shiroId,
                        'body'    => 'Oh so close! Can you try again?',
                    ]
                );

                $user->activeThreads()->syncWithoutDetaching([$thread->id]);

                $user->notify(new NewThreadMessageReceived($thread, Message::find($incorrectMessage->id)));
            }
        }
    }
}
