<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VocabularyList extends Model
{
    protected $fillable = [
        'name',
        'is_public',
    ];

    public static function getFillableAttributes()
    {
        return [
            'name',
            'is_public',
        ];
    }

    public static function getFillablePivotAttributes()
    {
        return [
            'is_favorite',
        ];
    }

    public static $defaultLists = [
        ['name' => 'History', 'is_public' => false],
        ['name' => 'I don\'t know it', 'is_public' => true],
        ['name' => 'I know it a bit', 'is_public' => true],
        ['name' => 'I\'ve mastered it', 'is_public' => true],
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function words()
    {
        return $this->belongsToMany(Word::class)->withPivot('is_favorite')->withTimestamps();
    }
}
