<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $hidden = ['users', 'created_at', 'updated_at'];

    protected $guarded = [];

    protected $appends = ['interest_user_count'];

    public function getInterestUserCountAttribute()
    {
        return $this->users->count();
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
