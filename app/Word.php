<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $fillable = [
        'word',
        'word_language_id',
        'pronunciation',
        'example',
    ];

    public static function getFillableAttributes()
    {
        return [
            'word',
            'word_language_id',
            'pronunciation',
            'example',
        ];
    }

    public function vocabularyLists()
    {
        return $this->belongsToMany(VocabularyList::class);
    }

    public function translations()
    {
        return $this->hasMany(Translation::class);
    }
}
