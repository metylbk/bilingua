<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $hidden = ['id'];
}
