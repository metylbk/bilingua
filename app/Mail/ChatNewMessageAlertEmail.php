<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChatNewMessageAlertEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $name;

    public $actionUrl = 'https://app.bilingua.io';

    public $senderAvatarUrl;

    public $chatMessage;

    public $sender;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sender, $receiver, $message)
    {
        $this->name = $receiver->name;

        $this->senderAvatarUrl = $sender->avatar;

        $this->chatMessage = $message;

        $this->sender = $sender->name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->sender . ' sent you a new message!')
                    ->view('emails.chat-new-message-alert');
    }
}
