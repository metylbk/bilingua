<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiroWord extends Model
{
    protected $fillable = [
        'word',
        'word_language_id',
        'pronunciation',
        'definition',
        'definition_language_id',
        'sentence',
        'correct_answers',
        'fun_fact',
        'level',
    ];

    public static function getFillableAttributes()
    {
        return [
            'word',
            'word_language_id',
            'pronunciation',
            'definition',
            'definition_language_id',
            'sentence',
            'correct_answers',
            'fun_fact',
            'level',
        ];
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
