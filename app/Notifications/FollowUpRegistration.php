<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FollowUpRegistration extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hey, ' . $this->user->family_name . ' ' . $this->user->first_name)
            ->line('Hope you\'ve had the chance to connect with some new friends through Bilingua, and to kick-start your learning!')
            ->line('We love using our app and want to make sure you are getting the most out of yours! 
            Did you know you can search for users based on common interests? 
            And that you can invite friends from Facebook, Twitter, or even by email? 
            For more awesome features')
            ->actionUrl('click here', "https://app.bilingua.io")
            ->line('Please let us know if you have any questions or suggestions.
             I love to hear from language lovers like me!')
            ->line('Happy conversations');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
