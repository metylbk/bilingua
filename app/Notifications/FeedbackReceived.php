<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Messages\MailMessage;

class FeedbackReceived extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $feedback;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $feedback)
    {
        $this->user = $user;
        $this->feedback = $feedback;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack($notifiable)
    {
        return (new SlackMessage)
            ->success()
            ->content('We received a new feedback')
            ->attachment(function ($attachment) {
                $attachment->title('Feedback #' . $this->feedback->id)
                           ->fields([
                                'Email' => $this->user->email,
                                'Name' => $this->user->first_name . ' ' . $this->user->family_name,
                                'Feedback' => $this->feedback->message,
                            ]);
            });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }


}
