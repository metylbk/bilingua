<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class IntroduceShiro extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hey '.$this->user->family_name . ' ' . $this->user->first_name)
            ->line('My name is Shiro, and I\'m your language learning personal assistant.
             My job is to help you manage your progress on learning a new language,
             but I will also do my best to keep you motivated and make sure you have fun.')
            ->line('If you need any help on how to enhance your time on Bilingua, feel free to ask :)')
            ->line('Before I go, here are 2 tips to help you initiate your learning journey:')
            ->line('Set a regular training goal. For instance, once a day (say, before you go to sleep),
             use Bilingua for at least 10 minutes to keep your target language fresh in your mind.')
            ->line('Don\'t be shy. You\'re here to progress, and the only way to progress is to fail. You wouldn\'t be able to walk today if you hadn\'t fallen over a few times as a baby.
             The process is the same for learning a language - remember that everybody else here is also learning :)')
            ->line('I hope to see you online soon! (Yes, I am a bot.)')
            ->line('Shiro :)');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
