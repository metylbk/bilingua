<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationWelcome extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hello '.$this->user->family_name . ' ' . $this->user->first_name)
            ->line('As fellow language lovers who know the struggles involved in learning a new language,
             we created Bilingua as the smart way forward for language exchange!')
            ->line('We are excited you have decided to join us in using Bilingua Beta.
             As one of our first users, your experience is extremely important to us.
              There are so many more exciting features we are working hard on bringing to you,
               so please reach out to ask what they are or to make some suggestions.')
            ->line('We have put a lot of time and effort into Bilingua, but we know we are still a work in progress.
             If you have any questions or feedback, then I\'d love to hear from you!')
            ->line('Guillaume Catella')
            ->line('Founder');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
