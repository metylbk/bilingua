<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Invitation extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $name = $this->user->family_name . ' ' . $this->user->first_name;
        return (new MailMessage)
            ->greeting('Hello !')
            ->line($name . ' has discovered a smarter way to pick up new languages,
             and would like to invite you to join in the fun.')
            ->line('Join ' . $name .' and many other users who are waiting to share their language with you.')
            ->line('It\'s easy to sign up! Just download our app from the app store {insert link}
             and you\'re on your way to speaking like a native.')
            ->line('If you want to learn more first, check out this article: {insert link}')
            ->line('Can\'t wait to get to know you!')
            ->line('Shiro');
    }

    /**
     * Get the array representation of the notification.
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
