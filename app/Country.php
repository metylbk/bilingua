<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;

    protected $visible = [
        'id',
        'country_code',
        'full_name',
        'name',
        'calling_code',
        'flag',
        'iso_3166_2'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
