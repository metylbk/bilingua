<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(EmailSchedule::introduceShiro())->daily()->at('20:00');

        $schedule->call(EmailSchedule::followUpRegistration())->daily()->at('20:00');

        $schedule->call(ShiroWordSchedule::sendDailyMessage())->daily()->at('17:00');

        $schedule->call(ShiroWordSchedule::sendWeeklyMessage())->tuesdays()->at('19:00');

        // $schedule->call(ShiroWordSchedule::sendDailyMessage())->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
