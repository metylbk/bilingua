<?php

namespace App\Console;

use Carbon\Carbon;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Mail;

class EmailSchedule
{
    use DispatchesJobs;

    static public function introduceShiro()
    {
        return function() {
            $users = User::where('created_at', 'like', Carbon::yesterday()->toDateString(). '%')->get();

            foreach ($users as $user) {
                Mail::queueOn('bilingua', 'emails.introduce-shiro', ['name' => $user->name], function ($m) use ($user) {
                    $m->to($user->email);
                    $m->subject('The more the merrier!');
                });
            }
        };
    }

    static public function followUpRegistration()
    {
        return function() {
            $users = User::where('created_at', 'like', Carbon::now()->subDays(3)->toDateString(). '%')->get();

            foreach ($users as $user) {
                Mail::queueOn('bilingua', 'emails.follow-up-registration', ['name' => $user->name], function ($m) use ($user) {
                    $m->to($user->email);
                    $m->subject('Practice makes perfect');
                });
            }
        };
    }
}