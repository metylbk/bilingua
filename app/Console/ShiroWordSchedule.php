<?php

namespace App\Console;

use App\User;
use App\Thread;
use App\Message;
use App\ShiroWord;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Notifications\NewThreadMessageReceived;

class ShiroWordSchedule
{
    use DispatchesJobs;

    public static function sendDailyMessage()
    {
        return function() {
            $shiroId = env('SHIRO_ID');
            $shiro = User::findOrFail($shiroId);

            $users = User::where('id', '<>', $shiroId)
                         ->where('last_active_at', '>', Carbon::now()->subWeek())
                         ->whereShiroMessageFrequency(User::SHIRO_MESSAGE_DAILY)
                         ->get();

            self::sendMessage($shiro, $users);
        };
    }

    public static function sendWeeklyMessage()
    {
        return function() {
            $shiroId = env('SHIRO_ID');
            $shiro = User::findOrFail($shiroId);

            $users = User::where('id', '<>', $shiroId)
                         ->where('last_active_at', '<=', Carbon::now()->subWeek())
                         ->orWhere(function ($query) {
                            $query->where('last_active_at', '>', Carbon::now()->subWeek())
                                  ->whereShiroMessageFrequency(User::SHIRO_MESSAGE_WEEKLY);
                         })
                         ->get();

            self::sendMessage($shiro, $users);
        };
    }

    private static function sendMessage($shiro, $users)
    {
        foreach ($users as $user) {
            $type = ($shiro->id < $user->id) ? $shiro->id.'-'.$user->id : $user->id.'-'.$shiro->id;

            $thread = $user->threads()->firstOrCreate(['type' => $type]);

            $user->activeThreads()->syncWithoutDetaching([$thread->id]);

            $thread->users()->syncWithoutDetaching([$shiro->id]);

            $sentWords = $user->shiroWords()->pluck('shiro_word_id');

            foreach ($user->learningLanguages as $learningLanguage) {
                $proficiency = $learningLanguage->pivot->proficiency;

                $shiroWord = ShiroWord::whereNotIn('id', $sentWords)->whereLevel($proficiency)->first();

                if ($shiroWord) {
                    $message1 = $thread->messages()->create(
                        [
                            'user_id' => $shiro->id,
                            'body'    => "Hey there! Our word of the day today is {$shiroWord->word}. It means {$shiroWord->definition}",
                        ]
                    );

                    $user->shiroWords()->syncWithoutDetaching([$shiroWord->id => ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]]);

                    $user->notify(new NewThreadMessageReceived($thread, Message::find($message1->id)));

                    $message2 = $thread->messages()->create(
                        [
                            'user_id' => $shiro->id,
                            'body'    => "For instance, {$shiroWord->sentence}",
                        ]
                    );

                    $user->notify(new NewThreadMessageReceived($thread, Message::find($message2->id)));

                    $message3 = $thread->messages()->create(
                        [
                            'user_id' => $shiro->id,
                            'body'    => "Let's try typing {$shiroWord->word}!",
                        ]
                    );

                    $user->notify(new NewThreadMessageReceived($thread, Message::find($message3->id)));
                }
            }
        }
    }
}
