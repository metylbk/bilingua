## Git
Please create a branch for your work and code in that branch, after push to the repo, create a merge request to develop branch.

#### Git Flow
Bilingua is not currently using the git-flow plugin, but it will in the near future. It will be useful to become familiar with it: http://danielkummer.github.io/git-flow-cheatsheet/

## Front End
All code is located in the "public" folder. This will contain the "src" sub-folder that will be used to hold all the source html, scss/css, js and image files. After running the build task through Gulp all production ready files will be placed inside the "dist" sub-folder. All intermediate level built files for development will be placed inside the "build" sub-folder.

### Creating A New Feature
Where \<feature-name\> refers to the name of the feature you are working on
1. Checkout develop branch: `git checkout develop`
2. Create new branch from develop: `git checkout -b feature-<feature-name>`
3. Delete your "node_modules" folder if exists, then run `npm install` (Also make sure Gulp-CLI is installed globally ```npm install --global gulp-cli```)
4. Rename feature.html to feature-\<feature-name\>.html
5. Add _\<feature-name\>.scss to /public/src/styles/pages
6. Import scss file from #4 into /public/src/styles/style.scss: `@import "pages/_<feature-name>.scss"`
7. Inside your new feature-\<feature-name\>.html, change `data-page="feature"` to `data-page="feature-<feature-name>"`
8. Create a file called feature-\<feature-name\>.js inside the /public/src/js/pages folder
9. Copy all of the JS code from /public/src/js/pages/feature.js into the file create in #8
10. Inside /public/src/js/feature-\<feature-name\>.js, change `myApp.onPageInit('feature', function (page) {` to `myApp.onPageInit('feature-<feature-name>', function (page) {`
11. In the index.html file, find the navbar and this menu item `<a href="feature.html" class="link">Feature</a>` - change this to `<a href="feature-<feature-name>.html" class="link"><feature-name></a>` (this will properly setup the Ajax page includes for Framework7)
12. Do the same thing as #11 in your /public/src/feature-\<feature-name\>.html file

Now to navigate to your feature, you run gulp (it will open up a browsersync proxy), click the appropriate link in the navbar. Do not go to /feature-\<feature-name\>.html

### Branching
Please follow these guidelines for branching and making new features: https://gitlab.com/creatella/knowledge-base/wikis/how-to-branch

### CSS Syntax
We will follow the BEM methodology, and especially its variant BEVM, for writing/creating CSS class names. A knowledge of the SMACSS methodology will also be helpful.
Some useful links for this:
1. [http://getbem.com/introduction/](http://getbem.com/introduction/)
2. [http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)
3. [https://www.viget.com/articles/bem-sass-modifiers](https://www.viget.com/articles/bem-sass-modifiers)

### Style Guide
For the JavaScript coding we will follow this Style Guide - https://github.com/eschafer/javascript-style-guide .
Any variations to this style guide for our own purposes will be noted here.

### EditorConfig
Please install the relevant [EditorConfig](http://editorconfig.org) plugin for your code editor so that there is a  consistent coding style across all team members based on the rules defined in `.editorconfig`.

### Framework7
We will be using [Framework7](http://framework7.io/docs/) for the CSS styles. Framework7 has a javascript component which we are using as well. Please use as much Framework7 as possible. Try to minimize the amount of custom classes and styles you write.

### React.js framework
We will be using [React.js](https://facebook.github.io/react/) as the primary javascript library to create a modern web app (this is coming in the future, not now). React will be used as the view layer. To illustrate the process of writing jsx files (the recommended way to write React code), create several React components, pass data long them a demo html file has been created called 'react-test.html'. This file uses Bootstrap to quickly create a prototype. Bootstrap is **NOT** intended to be used in the actual project and is only used for this demo file. The task for compiling React's jsx files has been defined in `gulpfile.js`.

### Build Process
For compiling scss files, minifying js files, and all other relevant build tasks we will be using [Gulp](http://gulpjs.com/). The various Gulp tasks that will be employed will be noted here. All terminal commands need to be executed from the root of the project directory. The gulpfile will be updated accordingly as the project progresses.

* Use `npm install` from your terminal to install all necessary node modules required for the build process. This process needs to be repeated whenever any changes to package.json are made.

* To start the process of watching all frontend files and doing the necessary compiling and linting type `gulp` in your terminal. This will also execute Browsersync which will automatically open up in the browser with one of the html files in the "build" folder.

* To only perform the build process without watching for changes execute `gulp build`. This task will place all the relevant files in /public/build.

* To only compile scss files execute `gulp sass` in the terminal.

* To only lint the js/jsx files execute `gulp eslint` in the terminal.

* To compile the jsx files, concatenate the source js files and lint them, execute `gulp js`.


## Staging server
You don't have to deploy the backend code on your local environment in this project. You can just send ajax request to staging server for development purpose.
Currently, staging server is [https://staging.bilingua.io](https://staging.bilingua.io).
* [https://k567d6c3nqbrdzb5.app.bilingua.io/](https://k567d6c3nqbrdzb5.app.bilingua.io/) or [https://r.createl.la/3](https://r.createl.la/3])

## Deployment Triggers
* [Staging](https://forge.laravel.com/servers/68024/sites/183408/deploy/http?token=VyLH81TAihJHccUUPTvbruFvCQP6E6EoQVa0SQSc)
* [Production](https://forge.laravel.com/servers/68024/sites/210602/deploy/http?token=z2FzZ5NIKPOkwQM8enUxaXuXXP7U2dXNFQKUddyG)


## API document
### Auth
#### logout
POST /auth/logout
### Chat
#### get thread list
GET /messages
#### get one thread
GET /messages/{id}
#### create thread
POST /messages
```recipients[]=1 // another user id ```
#### create message
PUT /messages/{id}
```message=xxx ```
#### report read status (the only way to update last_read_at)
POST /messages/{id}/read
#### delete thread from list
DELETE /messages/{id}

### Vocabulary List
#### Get vocabulary lists of an user
GET /users/{id}/vocabularyLists
#### Get a specific vocabulary list
GET /vocabularyLists/{id}
#### Create a new vocabulary list
POST /vocabularyLists

```
name=xx
&is_public=0,1
```
#### Update an existed vocabulary list
PUT /vocabularyLists/{id}

```
name=xx
&is_public=0,1
```
#### Invert is_public field of an existed vocabulary list
PUT /vocabularyLists/{id}/invertPublic
#### Delete an existed vocabulary list
DELETE /vocabularyLists/{id}
#### Add a word to a list
POST /vocabularyLists/{id}/words/{wordId}

```
is_favorite=0,1
```
#### Remove a word from a list
DELETE /vocabularyLists/{id}/words/{wordId}
#### Invert is_favorite field of a word in a list
PUT /vocabularyLists/{id}/words/{wordId}/invertFavoriteWord
#### Get vocabularies of a list
GET /vocabularyLists/{id}/vocabularies
#### Get a specific vocabulary
GET /vocabularies/{id}
#### Create a new word
POST /words

```
word=xx
&word_language_id=1,2,3
&pronunciation=xx
&example=xxx
```
#### Create a new translation
POST /words/{id}/translations

```
translation=xx
&translation_language_id=1,2,3
```
#### Update an existed word
PUT /words/{id}

```
word=xx
&word_language_id=1,2,3
&pronunciation=xx
&example=xxx
```
#### Update an existed translation
PUT /translations/{id}

```
translation=xx
&translation_language_id=1,2,3
```
#### Remove an existed word from a vocabulary list
DELETE /vocabularyLists/{vocabularyListId}/words/{id}
#### Delete an existed translation
DELETE /translations/{id}

#### Get all Shiro words
GET /shiroWords
#### Create a new Shiro word
POST /shiroWords

```
word=xx
&word_language_id=1,2,3
&pronunciation=xx
&definition=xx
&definition_language_id=1,2,3
&sentence=xxx
&fun_fact=xxx
&level=1,2,3
```
#### Update an existed Shiro word
POST /shiroWords/{id}

```
word=xx
&word_language_id=1,2,3
&pronunciation=xx
&definition=xx
&definition_language_id=1,2,3
&sentence=xxx
&fun_fact=xxx
&level=1,2,3
```
#### Delete an existed Shiro word
DELETE /shiroWords/{id}

### Personality Match
#### get all questions
GET /questions

#### get user's questions information
GET /users/me/questions

#### save user's answer
POST /users/me/questions/{id}
```
answer=1|2|3|4|5
expect=123 // if user choose option 1 and 2, then value is "12", if choose option 2-4, value is "234"
relevance=20 // refer to slide multiple time
```

#### get match score
GET /users/{id}/matchscore


### Other
#### leave feedback
POST /feedback
```message=xxxx ```


# Creatella Pusher Chat
## Basic Concepts
### Understand Models(in MVC) in Chat
#### User
**user** is an entity that can send and receive message, in this case, website users or bots.
#### Thread
**thread** is like a chat room, a **thread** can have one or many users in it, also contains one or many **messages**.

**thread** have following important attributes.
##### title
**title** is something that user can change, usually indicates the subject of the chat, it's set to `null` by default.

When client get `null` for this, please render the title with the names of all other users that in this **thread** by alphabetical order.

Example: if **user** *Akali*, *Jax*, *Diana*, *Kalista* is in the **thread** , and the current authenticated **user** is *Jax*, the title should be `Akali, Diana, Kalista`.
#### Message
**message** is the what **user** send to other **user** in a **thread**.

**message** have following important attributes.
##### body
**body** is the content of a message.
##### created_at
**created_at** is the timestamp that **user** send the message.
##### updated_at
**updated_at** is the timestamp that **user** last time edit the message.

if the message is sent over 5 minutes, **user** can't edit the message.
### Understand important Relationships between Models
#### thread_user
one **user** can have many **thread**, and one **thread** can have many **user**, **thread** and **user** is Many-to-Many relationship.

**thread_user** pivot table have following important attributes.
##### last_read_at
**last_read_at** is the timestamp that user last check the **thread**, **messages** created after that time should consider not read.

### Understand Pages on Front-end
#### Page - **thread** List
This is the page to show a list of **thread**, sort by **created_at** of last **message**.
##### Q&A
Q: Which **thread** should be on this list?

A: those **thread** that user didn't delete on his side(which should saved in *localstorage*) since last time received a **message** and those **thread** have a new **message**.
#### Page - Chat (one **thread**)
This is the page to show one **thread**, and many **message** that in this **thread**.

### Understand Work Flow
#### 0. Listen Notification Channel, notification will be sent once there is a new **message**.
#### 1. **User** enters **thread** list page or equivalent action (for a desktop app, if we have a chat in sidebar or footer, then whatever user is in, he should consider in this step)
##### 1.1 Make an API call to retrieve all new **thread** for authenticated **user** and render the page.
##### 1.2 subscribe all pusher channel for each **thread**
In a one-to-one **thread**, check another **user** online status and render, if receive event that another **user** is leaving the channel, change the status, if receive event that another **user** joining the channel, change the status.

if receive a new **message**, move up the **thread** to top of the page, and render the change the **thread** preview **message**(usually the last message) to the lastest **message** on this page.

#### 2. User enters one thread page or equivalent action
##### 2.0 if **thread** list page is not rendered in current page(html is not hidden or removed, two page are separate), only subscribe channel for this **thread**, if **thread** list page is hidden, continue render it without make it visible, if **thread** list page is removed, unsubscribe other channel.
##### 2.1 make API call to get all new **message** for this thread, and render the page, mark those **message** that not been read, and when user take some action(touch, click, type) on the page, mark them read, and ping the server to update **last_read_at**.
##### 2.2 if receive new **message** event, insert into page, and wait for user taking action, then mark read and ping the server.
##### 2.3 if authenticated **user** is typing, ping the server and server will broadcast typing event to other **user**(after Echo support that, do client broadcasting). After user click send, make API call.
##### 2.4 if receive **message** changed/deleted event (another **user** make an edit/delete action), do the change. if receive typing event, do the change.

## Echo guide
### Basics of Laravel Pusher Integration
install package
```
npm install --save laravel-echo pusher-js
```

example basic import code with staging key.
```
import Echo from "laravel-echo"

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '8940857d7fcad22b2d0b',
});
```

listen for public channel event
```
Echo.channel('channel-name-here')
    .listen('event-name-here', (e) => {
        console.log(e);
    });
```

listen for private channel event
```
Echo.private('channel-name-here')
    .listen('event-name-here');
```

listen for presence channel event
```
Echo.join('channel-name-here')
    .here((users) => {
        //
    })
    .joining((user) => {
        console.log(user.name);
    })
    .leaving((user) => {
        console.log(user.name);
    })
    .listen('event-name-here', (e) => {
        //
    });
```
The `here` callback will be executed immediately once the channel is joined successfully,

and will receive an array containing the user information for all of the other users currently subscribed to the channel.

The `joining` method will be executed when a new user joins a channel,

while the `leaving` method will be executed when a user leaves the channel.

**ALL USER SHOULD** listen for notifications.
```
Echo.private('App.User.' + userId)
    .notification((notification) => {
        console.log(notification.type);
    });
```

## Detail API and Events List and Notification Type
### API
**Working in Progress**
### Notification
**Working in Progress**

When a user receive a new message, a notification will be sent.
### Events
**Working in Progress**

When a user enter a chat, subscribe the presence channel `chat.thread.<id>`
#### `NewThreadMessageEvent`
once a new message is updated, this event will be triggered.


### Using Pusher.js to work with Echo
Echo.js uses ES6 that bilingua's code doesn't support so there is a need to work around the need of using echo.js library. Here are some ways that can be done.


Subscribing to a channel and setting an event listener:

With Echo:
```
Echo.channel('channel-name-here')
    .listen('event-name-here', (e) => {
        console.log(e);
    });
```

With Pusher:
```
var channel = pusher.subscribe('channel-name-here');

channel.bind('event-name-here', function(e) {
  console.log(e);
});
```

Subscribing to private or presence channels:

With Echo:
```
Echo.private('channel-name-here')
```

With Pusher:
```
channel = pusher.subscribe('private-' + 'channel-name-here');

channel = pusher.subscribe('presence-' + 'channel-name-here');
```

When a user enter a chat, subscribe the presence channel `chat.thread.<id>`, once a new message is updated,
an event named `NewThreadMessageEvent` will be trigged.
