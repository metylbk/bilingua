<?php


//Route::options('{all}', 'OptionsMethodHandlerController@handle')->where('all', '.*');

Route::get('avatars/{id}', 'AvatarController@show');
Route::get('images/covers/{id}', 'AvatarController@showCoverImage');

Route::get('email/verify/{code}', 'UserController@verifyEmail');

Route::get('auth/cookie/check', function(){
    return \Auth::check() ? 'true' : 'false';
});

Route::group(['prefix' => 'api'], function(){
    Route::post('auth/reg', 'AuthenticateController@reg');
    Route::post('auth/login', 'AuthenticateController@login');
    Route::post('auth/oauth', 'AuthenticateController@oauth');
    //TODO: UPDATE EMAIL CHECK API
    Route::post('auth/email.check', 'AuthenticateController@checkEmail');

    Route::get('metas', 'MetaController@index');
    Route::get('metas/{id}', 'MetaController@getById');
    Route::get('sliders', 'SliderController@index');

});


Route::group(['prefix' => 'api', 'middleware' => 'auth'], function(){

    Route::post('auth/logout', 'AuthenticateController@logout');

    Route::get('messages', 'MessageController@index');
    Route::post('messages', 'MessageController@store');
    Route::get('messages/{id}','MessageController@show');
    Route::post('messages/{id}/read', 'MessageController@updateLastReadAt');
    Route::put('messages/{id}','MessageController@update');
    Route::delete('messages/{id}', 'MessageController@deleteThreadFromList');
    Route::post('feedback', 'FeedbackController@store');

    Route::get('users/{id}/vocabularyLists', 'VocabularyListController@index');
    Route::get('vocabularyLists/{id}', 'VocabularyListController@show');
    Route::post('vocabularyLists', 'VocabularyListController@store');
    Route::put('vocabularyLists/{id}', 'VocabularyListController@update');
    Route::put('vocabularyLists/{id}/invertPublic', 'VocabularyListController@invertPublic');
    Route::delete('vocabularyLists/{id}', 'VocabularyListController@delete');
    Route::post('vocabularyLists/{id}/words/{wordId}', 'VocabularyListController@addWord');
    Route::delete('vocabularyLists/{id}/words/{wordId}', 'VocabularyListController@removeWord');
    Route::put('vocabularyLists/{id}/words/{wordId}/invertFavoriteWord', 'VocabularyListController@invertFavoriteWord');

    Route::get('vocabularyLists/{id}/vocabularies', 'VocabularyController@index');
    Route::get('vocabularies/{id}', 'VocabularyController@show');
    Route::post('words', 'VocabularyController@storeWord');
    Route::post('words/{id}/translations', 'VocabularyController@storeTranslation');
    Route::put('words/{id}', 'VocabularyController@updateWord');
    Route::put('translations/{id}', 'VocabularyController@updateTranslation');
    Route::delete('translations/{id}', 'VocabularyController@deleteTranslation');

    Route::get('shiroWords', 'ShiroWordController@index');
    Route::post('shiroWords', 'ShiroWordController@store');
    Route::put('shiroWords/{id}', 'ShiroWordController@update');
    Route::delete('shiroWords/{id}', 'ShiroWordController@delete');

    Route::get('auth/check', 'AuthenticateController@check');

    Route::get('interests', 'InterestController@index');
    Route::post('interests', 'InterestController@store');
    //Route::get('interests/hottest', 'InterestController@hottest');
    //Route::get('interests/search/{name}', 'InterestController@search');

    Route::get('users', 'UserController@index');
    Route::get('users/me', 'UserController@me');
    Route::delete('users/me', 'UserController@destroy');
    Route::patch('users/me', 'UserController@update');
    Route::put('users/me/interests', 'UserController@syncInterest');
    Route::put('users/me/languages', 'UserController@syncLanguages');
    Route::get('users/me/settings', 'UserController@getSettings');
    Route::put('users/me/settings', 'UserController@syncSettings');
    Route::patch('users/me/settings', 'UserController@updateSettings');

    Route::post('users/me/avatar', 'UserController@uploadAvatar');
    Route::post('users/me/images/cover', 'UserController@uploadCoverImage');
    Route::get('users/me/questions', 'QuestionController@getUserQuestions');
    Route::post('users/me/questions/{id}', 'QuestionController@saveUserQuestionAnswer');
    Route::get('users/blocked',  'UserController@blocked');

    Route::get('users/{id}', 'UserController@show');
    Route::get('users/{id}/matchscore/', 'QuestionController@getMatchScore');
    Route::post('users/{id}/block', 'UserController@block');
    Route::post('users/{id}/unblock', 'UserController@unblock');

    Route::get('avatars/{id}', 'AvatarController@show');
    Route::get('images/cover/{id}', 'AvatarController@showCoverImage');

    Route::get('languages', 'LanguageController@index');

    Route::get('countries', 'CountryController@index');

    Route::post('invite', 'InviteController@invite');

    Route::get('questions', 'QuestionController@getAllQuestions');

});
