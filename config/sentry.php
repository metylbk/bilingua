<?php

return array(
    'dsn' => 'https://5fe55ae1a1464455b0524976589ff748:a50979146de5449e8fbd8b7faf103fd6@sentry.createl.la/3',

    // capture release as git sha
    // 'release' => trim(exec('git log --pretty="%h" -n1 HEAD')),

    // Capture bindings on SQL queries
    'breadcrumbs.sql_bindings' => true,
);
