require('cache-require-paths');

var gulp = require('gulp');

// incude the plugins
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var pxtorem = require('postcss-pxtorem');
var mqpacker = require('css-mqpacker');
var cssnano = require('cssnano');
var eslint = require('gulp-eslint');
var stylelint = require('stylelint');
var sourceMaps = require('gulp-sourcemaps');
var reporter = require('postcss-reporter');
var scss = require('postcss-scss');
var browserSync = require('browser-sync').create();
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var htmlReplace = require('gulp-html-replace');
var replace = require('gulp-replace');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');

// declare file and folder paths
var baseDir = 'public/src';
var jsFolder = baseDir + '/js';
var cssFolder = baseDir + '/styles';

var jsFiles = {
    libs: [jsFolder + '/libs/*.js'],
    vendor: [jsFolder + '/vendors/*.js'],
    utils: jsFolder + '/utils/*.js',
    main: jsFolder + '/main.js',
    components: jsFolder + '/components/*.js',
    pages: jsFolder + '/pages/*.js'

};

var sassFiles = cssFolder + '/**/*.scss';

var buildFolder = 'public/build';
var distFolder = 'public/dist';


// Lint JS files
gulp.task('eslint', function() {
    return gulp.src([jsFiles.utils, jsFiles.main, jsFiles.components, jsFiles.pages, '!' + jsFolder + '/utils/CordovaApp.js'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

// Lint sass files
gulp.task('stylelint', function() {
    return gulp.src(sassFiles)
        .pipe(postcss(
            [stylelint(), reporter({clearMessages: true, throwError: false})],
            {syntax: scss}
        ));
});

/*
 * Concatenate jsFiles.vendor and jsFiles.source into one JS file,
 * run eslint before concatenating.
 */
gulp.task('js', ['eslint'], function() {
    return gulp.src(jsFiles.vendor.concat(jsFiles.utils, jsFiles.main, jsFiles.components, jsFiles.pages))
        .pipe(sourceMaps.init())
        .pipe(concat('main.js'))
        .pipe(sourceMaps.write('maps'))
        .pipe(gulp.dest(buildFolder + '/js'));
});


// compile sass and use postcss
gulp.task('sass', ['stylelint'], function() {
    var processors = [
        autoprefixer({browsers: 'Safari >= 8, ie >= 11, iOS >= 8, Android >= 4.4'}),
        pxtorem({replace: false, rootValue: 14})
    ];

    return gulp.src(sassFiles)
        .pipe(sourceMaps.init())
        .pipe(sass()
            .on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(sourceMaps.write('maps'))
        .pipe(gulp.dest(buildFolder + '/css'))
        .pipe(browserSync.stream({match: '**/*.css'}));
});


// enable browser-sync
gulp.task('browserSync', function() {
    browserSync.init({
        injectChanges: true,
        port: 8080,
        server: {
            baseDir: buildFolder
        }
    });
});


// gulp copy files to build folder
gulp.task('build:copy', ['build:copyHtml', 'build:copyImages', 'build:copyJSLibs', 'build:copyFonts']);

gulp.task('robots', function() {
    return gulp.src(baseDir + '/robots.txt')
        .pipe(gulp.dest(buildFolder));
});

gulp.task('build:copyHtml', function() {
    return gulp.src(baseDir + '/*.html')
        .pipe(gulp.dest(buildFolder));
});

gulp.task('build:copyImages', function() {
    return gulp.src(baseDir + '/images/**/*')
        .pipe(gulp.dest(buildFolder + '/images'));
});

gulp.task('build:copyJSLibs', function() {
    return gulp.src(jsFiles.libs)
        .pipe(gulp.dest(buildFolder + '/js/libs'));
});

gulp.task('build:copyFonts', function() {
    return gulp.src(baseDir + '/fonts/**/*')
        .pipe(gulp.dest(buildFolder + '/fonts'));
});


// perform all build tasks
gulp.task('build', ['sass', 'js', 'robots', 'build:copy']);


// watch for changes
gulp.task('watch', ['browserSync', 'build'], function() {
    gulp.watch(sassFiles, ['sass']);
    gulp.watch(jsFolder + '/**/*.js', ['js-watch']);
    gulp.watch(baseDir + '/**/*.html', ['html-watch']);
});

gulp.task('js-watch', ['js'], function() {
    browserSync.reload();
});

gulp.task('html-watch', ['build:copyHtml'], function() {
    browserSync.reload();
});


/*
 * Production build tasks
 */

// optimize images
gulp.task('images', function() {
    return gulp.src(baseDir + '/images/**/*.{png,jpg,gif,svg,ico}')
        .pipe(cache(imagemin({
            svgoPlugins: [{removeViewBox: false}]
        })))
        .pipe(gulp.dest(distFolder + '/images'));
});

// clear the images cache
gulp.task('cache:clear', function(done) {
    return cache.clearAll(done);
});

// copy fonts to dist folder
gulp.task('fonts', function() {
    return gulp.src(baseDir + '/fonts/**/*')
        .pipe(gulp.dest(distFolder + '/fonts'));
});

// clean the dist folder
gulp.task('clean:dist', function() {
    return del.sync(distFolder);
});

// prepare css files for production environment
gulp.task('dist:css', ['sass'], function() {
    var processors = [
        mqpacker,
        cssnano({
            discardComments: {
                removeAll: true
            }
        })
    ];

    return gulp.src(buildFolder + '/css/*.css')
        .pipe(postcss(processors))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(distFolder + '/css'));
});

// prepare js files for production environment
gulp.task('dist:js', ['js'], function() {
    return gulp.src(buildFolder + '/js/main.js')
        .pipe(replace('8940857d7fcad22b2d0b', 'f00b384ef0cd29d53a09'))
        .pipe(replace('https://k567d6c3nqbrdzb5.app.bilingua.io', 'https://app.bilingua.io'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify({preserveComments: 'license'}))
        .pipe(gulp.dest(distFolder + '/js'));
});

gulp.task('dist:libjs', function() {
    return gulp.src(jsFiles.libs)
        .pipe(gulp.dest(distFolder + '/js/libs'));
});

// prepare html files for production environment
gulp.task('dist:html', function() {
    return gulp.src(buildFolder + '/*.html')
        .pipe(htmlReplace({
            'cssMain': 'css/style.min.css',
            'jsMain': 'js/main.min.js'
        }))
        .pipe(gulp.dest(distFolder));
});

// main production build task
gulp.task('dist', function(done) {
    runSequence(
        'clean:dist',
        ['dist:css', 'dist:js', 'dist:libjs', 'dist:html', 'images', 'fonts'],
        done
    );
});


// default task
gulp.task('default', ['watch']);
