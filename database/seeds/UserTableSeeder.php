<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            'visibility_same_gender' => true,
            'visibility_min_age'     => 13,
            'visibility_max_age'     => 14,
        ];
        $user = User::create([
            'username'                  => 'iamauser',
            'email'                     => 'email@example.com',
            'password'                  => '123456',
            'first_name'                => 'Hanlin',
            'family_name'               => 'Wang',
            'avatar'                    => 'https://www.gravatar.com/avatar/' . md5(str_random(10)),
            'gender'                    => '1',
            'birthday'                  => Carbon::now(),
            'bio'                       => 'I am from China!',
            'phone'                     => '0081237124',
            'settings'                  => json_encode($settings),
            'google_id'                 => '111',
            'facebook_id'               => '123',
            'last_active_at'            => Carbon::now(),
        ]);

        factory(App\User::class, 10)->create();
    }
}
