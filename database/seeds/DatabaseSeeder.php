<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        //$this->call(LanguageTableSeeder::class);
        $this->call(MetaTableSeeder::class);
        //$this->call(InterestTableSeeder::class);
        $this->call(SliderTableSeeder::class);
        // $this->call(MessageTableSeeder::class);
    }
}
