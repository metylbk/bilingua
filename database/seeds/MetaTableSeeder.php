<?php

use Illuminate\Database\Seeder;

class MetaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Meta::class, 20)->create();

        DB::table('metas')->insert([
            ['category' => '1', 'name' => 'slider-1', 'content' => 'https://www.gravatar.com/avatar/3fb609d909e2ebf56213c1b894703453?s=328&d=identicon&r=PG'],
            ['category' => '1', 'name' => 'slider-2', 'content' => 'https://www.gravatar.com/avatar/3fb609d909e2ebf56213c1b894703453?s=328&d=identicon&r=PG'],
            ['category' => '1', 'name' => 'slider-3', 'content' => 'https://www.gravatar.com/avatar/3fb609d909e2ebf56213c1b894703453?s=328&d=identicon&r=PG'],
            ['category' => '1', 'name' => 'slider-4', 'content' => 'https://www.gravatar.com/avatar/3fb609d909e2ebf56213c1b894703453?s=328&d=identicon&r=PG'],
            ['category' => '1', 'name' => 'slider-5', 'content' => 'https://www.gravatar.com/avatar/3fb609d909e2ebf56213c1b894703453?s=328&d=identicon&r=PG'],
        ]);
    }
}
