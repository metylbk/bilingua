<?php

use Illuminate\Database\Seeder;

class InterestTableSeeder extends Seeder
{
    protected $hidden = ['id'];
    /**
     * Run the database seeds.
     */
    public function run()
    {
        factory(App\Interest::class, 20)->create();
    }
}
