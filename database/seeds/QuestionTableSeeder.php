<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = getQuestionListFromFile();


        foreach ($questions as $question) {
            App\Question::forceCreate([
                'question' => $question['question'],
                'option_1' => $question['option_1'],
                'option_2' => $question['option_2'],
                'option_3' => $question['option_3'],
                'option_4' => $question['option_4'],
                'option_5' => $question['option_5'],
            ]);
        }
    }
}
