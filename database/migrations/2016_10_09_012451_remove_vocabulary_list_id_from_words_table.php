<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveVocabularyListIdFromWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('words', function ($table) {
            $table->dropColumn('is_favorite');
            $table->dropColumn('level');

            $table->dropForeign('words_vocabulary_list_id_foreign');
            $table->dropColumn('vocabulary_list_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
