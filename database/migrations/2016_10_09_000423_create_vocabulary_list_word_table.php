<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVocabularyListWordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vocabulary_list_word', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vocabulary_list_id')->unsigned();
            $table->integer('word_id')->unsigned();
            $table->boolean('is_favorite')->default(false);
            $table->timestamps();

            $table->foreign('vocabulary_list_id')->references('id')->on('vocabulary_lists');
            $table->foreign('word_id')->references('id')->on('words');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vocabulary_list_word');
    }
}
