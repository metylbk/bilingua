<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique()->nullable();
            $table->string('first_name')->nullable();
            $table->string('family_name')->nullable();
            $table->string('password', 60)->nullable();
            $table->string('avatar')->nullable();
            $table->string('cover_image')->nullable();
            $table->integer('gender')->nullable();
            $table->date('birthday')->nullable();
            $table->text('bio')->nullable();
            $table->text('description')->nullable();
            $table->integer('country_id')->unsigned()->default(702);
            $table->string('phone', 25)->nullable();
            $table->boolean('email_verified')->default(0);
            $table->string('email_verify_code')->nullable();
            $table->string('email')->unique();
            $table->json('settings')->nullable();
            $table->string('google_id')->unique()->nullable();
            $table->string('facebook_id')->unique()->nullable();
            $table->timestamp('last_active_at')->default(\Carbon\Carbon::now());
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('users');
    }
}
