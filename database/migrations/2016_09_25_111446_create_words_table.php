<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('words', function (Blueprint $table) {
            $table->increments('id');
            $table->string('word');
            $table->integer('word_language_id')->unsigned();
            $table->string('pronunciation')->nullable();
            $table->string('example')->nullable();
            $table->boolean('is_favorite')->default(false);
            $table->smallInteger('level')->unsigned();
            $table->integer('vocabulary_list_id')->unsigned();
            $table->timestamps();

            $table->foreign('word_language_id')->references('id')->on('languages');
            $table->foreign('vocabulary_list_id')->references('id')->on('vocabulary_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('words');
    }
}
