<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiroWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shiro_words', function (Blueprint $table) {
            $table->increments('id');
            $table->string('word');
            $table->integer('word_language_id')->unsigned();
            $table->string('pronunciation')->nullable();
            $table->string('definition');
            $table->integer('definition_language_id')->unsigned();
            $table->string('sentence')->nullable();
            $table->string('correct_answers')->nullable();
            $table->string('fun_fact')->nullable();
            $table->smallInteger('level')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('word_language_id')->references('id')->on('languages');
            $table->foreign('definition_language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shiro_words');
    }
}
