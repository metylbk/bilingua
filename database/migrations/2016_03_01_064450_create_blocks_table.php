<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sender_id')->unsigned();
            $table->integer('target_id')->unsigned();
            $table->string('reason')->nullable();
            $table->timestamps();

            $table->foreign('sender_id')->references('id')->on('users');
            $table->foreign('target_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('blocks');
    }
}
