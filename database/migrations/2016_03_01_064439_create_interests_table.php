<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('interests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });

        $interests = [
            'Animals',
            'Design',
            'Business',
            'Crafts',
            'Health',
            'Alternative Culture',
            'Travel',
            'Classical Music',
            'Activism',
            'Family',
            'Energy',
            'Cars',
            'Animation',
            'Pets',
            'Fine Arts',
            'Education',
            'Fashion',
            'Exercise',
            'History',
            'Hotels',
            'Electronic Music',
            'News',
            'Relationships',
            'Engineering',
            'Trains',
            'TV',
            'Cooking',
            'Performing Arts',
            'Finance',
            'Beauty',
            'Medicine',
            'Philosophy',
            'Luxury',
            'Jazz and Blues',
            'Humanitarianism',
            'Humor',
            'Mathematics',
            'Extreme Sports',
            'Movies',
            'Food',
            'Photography',
            'Learning',
            'World Culture',
            'Nightlife',
            'Pop Music',
            'Politics',
            'Friends',
            'Science',
            'Games',
            'Dramas',
            'Religion',
            'Spas',
            'Rock Music',
            'AI and Robotics',
            'Video Games',
            'Spirituality',
            'Tourism',
            'Programming',
            'Sports',
            'Books',
            'Outdoors',
            'Gadgets',
            'Drinks',
            'Technology',
            'Internet',
            'Startups',
        ];

        foreach ($interests as $interest) {
            App\Interest::create(['name' => $interest]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('interests');
    }
}
