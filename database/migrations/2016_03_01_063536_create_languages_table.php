<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 2);
            $table->string('english_name');
            $table->string('native_name')->nullable();
            $table->string('flag');
            $table->boolean('is_major')->default(false);
            $table->timestamps();
        });

        (new LanguageTableSeeder())->run();
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('languages');
    }
}
