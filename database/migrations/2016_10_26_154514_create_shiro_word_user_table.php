<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiroWordUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shiro_word_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shiro_word_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('is_answered')->default(false);
            $table->timestamps();

            $table->foreign('shiro_word_id')->references('id')->on('shiro_words');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shiro_word_user');
    }
}
