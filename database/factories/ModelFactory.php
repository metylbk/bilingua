<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
            'username'       => $faker->userName . $faker->userName,
            'email'          => $faker->userName . $faker->email,
            'password'       => '123456',
            'first_name'     => $faker->firstName,
            'family_name'    => $faker->lastName,
            'avatar'         => 'https://www.gravatar.com/avatar/' . md5(str_random(10)),
            'gender'         => $faker->numberBetween(1, 3),
            'birthday'       => $faker->dateTimeThisCentury,
            'bio'            => $faker->text,
            'phone'          => $faker->phoneNumber,
            'settings'       => json_encode(['a' => 10, 'b' => 'ccccc', 'd' => true]),
            'google_id'      => $faker->numberBetween(1000000000000, 9999999999999),
            'facebook_id'    => $faker->numberBetween(1000000000000, 9999999999999),
            'remember_token' => str_random(10),
    ];
});

$factory->define(App\Meta::class, function (Faker\Generator $faker) {
    return [
        'name'    => $faker->word,
        'content' => $faker->word,
        'category' => 1,
    ];
});

$factory->define(App\Slider::class, function (Faker\Generator $faker) {
    return [
        'title'       => $faker->word,
        'description' => $faker->sentence(6),
        'content'     => $faker->word,
        'image'       => 'https://placekitten.com/700/907',
    ];
});

$factory->define(App\Interest::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->bothify('Interest-####'),
    ];
});

$factory->define(App\VocabularyList::class, function (Faker\Generator $faker) {
    return [
        'name'      => $faker->unique()->bothify('List-####'),
        'is_public' => (bool)random_int(0, 1),
    ];
});

$factory->define(App\Word::class, function (Faker\Generator $faker) {
    return [
        'word'             => $faker->unique()->word,
        'word_language_id' => random_int(1, 30),
        'pronunciation'    => $faker->word,
        'example'          => $faker->sentence(10),
    ];
});

$factory->define(App\Translation::class, function (Faker\Generator $faker) {
    return [
        'translation'             => $faker->sentence(2),
        'translation_language_id' => random_int(1, 30),
    ];
});

$factory->define(App\ShiroWord::class, function (Faker\Generator $faker) {
    return [
        'word'               => $faker->unique()->word,
        'word_language_id'   => random_int(1, 30),
        'pronunciation'      => $faker->word,
        'definition'         => $faker->word,
        'definition_language_id' => random_int(1, 30),
        'sentence'           => $faker->sentence(10),
        'correct_answers'    => $faker->word,
        'fun_fact'           => $faker->sentence(10),
        'level'              => random_int(1, 3),
    ];
});
