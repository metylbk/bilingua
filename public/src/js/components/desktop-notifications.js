function desktopNotification(trigger, messageBody, threadURL, messageStatus, user, redirectObject) {
    var receivedMessage,
        targetURL,
        isGranted,
        notification,
        status = messageStatus;

    if (redirectObject['chatId'] == user.id) {
        return;
    }
    // Function display notification with message body and link to chatroom page with that message
    function newMessage(messageBody, threadURL, status) {

        //Contains message text and target URL
        receivedMessage = messageBody,
        targetURL = threadURL;

        if (receivedMessage) {
            notifyMe('Bilingua', receivedMessage, '../../images/favicons/android-chrome-192x192.png ', targetURL);
        }

        function notifyMe(title, body, icon, url) {

            //default to false
            isGranted = false;

            //check if the browser supports notifications
            if (!(window.hasOwnProperty('Notification'))) {
                //console.log("This browser does not support desktop notification");
            }
            else if (Notification.permission === 'granted') {
                //set to true
                isGranted = true;
            }
            else if (Notification.permission !== 'denied') {
                //ask user for permission
                Notification.requestPermission(function(permission) {
                    if (permission === 'granted') {
                        //set to true
                        isGranted = true;
                    }
                });
            }

            if (isGranted === true) {

                //create a new notification
                notification = new Notification(title, {
                    icon: icon,
                    body: body
                });

                //change document title
                document.title = 'New Message from ' + redirectObject.targetName;

                //when clicked close the notification and go to the url
                notification.onclick = function(e) {
                    e.preventDefault();
                    mainView.router.load({
                        url: 'chatroom.html',
                        query: {
                            id: redirectObject.currentId,
                            userid: redirectObject.chatId,
                            name: redirectObject.targetName,
                            image: redirectObject.targetImage,
                            fromPartner: false
                        }
                    });
                    try {
                        window.focus();
                        notification.close();
                        window.scrollUpdate('read');
                    }
                    catch (ex) {
                      //console.log(ex);
                    }
                };

                //Notification display time
                setTimeout(function() {
                    notification.close();
                }, 10000);

                //when closing the notification change the document title
                notification.onclose = function() {
                    //change document title back to original
                    document.title = 'Bilingua';
                };
            }


            //request permission
            Notification.requestPermission();
        }
    }

    status = messageStatus;

    if (document.hasFocus() && trigger == 'newMessage' && status == 'unread') {
        status = 'read';
      //console.log('Don\'t show notification');
    }
    else {
        newMessage(messageBody, threadURL, messageStatus);
        //console.log('Show notifications');
    }
}

window.DisplayNotification = function(notification) {
    var message = notification.message;
    var thread = notification.thread;
    var chatId = message.user_id;
    var messageBody = message.body;
    var messageStatus = 'unread';
    makeApiRequest('users/' + chatId, null, 'GET', true).success(function(data) {
        var user = window.authUser;
        var targetImage = data.avatar;
        var targetName = data.first_name + ' ' + data.family_name;
        var currentId = 'conv-' + thread.id;
        var redirectObject = {
            'targetImage': targetImage,
            'targetName': targetName,
            'chatId': chatId,
            'currentId': currentId
        };
        desktopNotification('newMessage', messageBody, 'http://localhost:8080/', messageStatus, user, redirectObject);
    });
};
