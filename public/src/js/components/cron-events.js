(function($, document, window) {

    'use strict';

    /**
     * Inivite invite friends popup
     */

    if (Auth.isLoggedIn() && !localStorage.inviteFriendsPopup || localStorage.inviteFriendsPopup < 4) {
        myApp.onPageAfterAnimation('chats partners my-profile', function(page) {
            makeApiRequest('users/me', null, 'GET', true)
                .done(function(res) {
                    var registerDate = new Date(res.created_at);
                    var today = new Date();

                    // Initialize
                    if (!localStorage.inviteFriendsPopup) {
                        localStorage.inviteFriendsPopup = 1;
                    }
                    else {
                        // Popup after 3 days
                        if (localStorage.inviteFriendsPopup == 1 && today > registerDate.addDays(3)) {
                            window.BilinguaPopup.inviteFriends();
                            localStorage.inviteFriendsPopup = 2;
                        }

                        // Popup after 14 days
                        if (localStorage.inviteFriendsPopup == 2 && today > registerDate.addDays(14)) {
                            window.BilinguaPopup.inviteFriends();
                            localStorage.inviteFriendsPopup = 3;
                        }

                        // Popup after 30 days
                        if (localStorage.inviteFriendsPopup == 3 && today > registerDate.addDays(30)) {
                            window.BilinguaPopup.inviteFriends();
                            localStorage.inviteFriendsPopup = 4;
                        }
                    }
                });
        });
    }

}(jQuery, document, window));
