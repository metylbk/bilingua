window.blockDeleteUser = function() {
    var pressEvent = myApp.support.touch ? 'press' : 'click';
    var $mediaList = $('body');

    // disable the context menu for touchscreens
    if (myApp.support.touch) {
        $mediaList.on('contextmenu', '.conversation-item', function(e) {
            e.preventDefault();
        });

        $mediaList.on(pressEvent, '.conversation-item', function(e) {
            var $this = $(this);
            e.stopPropagation();
            e.preventDefault();

            if ($this.hasClass('conversation-item') && $this.find('.options-dots')
                    .is(':visible')) {
                return;
            }

            if ($(this)
                    .closest('.conversation-item')
                    .hasClass('isActive')) {
                $(this)
                    .closest('.conversation-item')
                    .removeClass('isActive');
                $('.conversation-list')
                    .removeClass('disable-redirect');
            }
            else {
                $(this)
                    .closest('.conversation-item')
                    .addClass('isActive');
                $('.conversation-list')
                    .addClass('disable-redirect');
            }
        })

        .on('click', 'a.js-delete, a.js-block', function(e) {
            e.stopPropagation();
            e.preventDefault();
            $(this)
                .closest('.conversation-item')
                .removeClass('isActive');
            $('.conversation-list')
                .removeClass('disable-redirect');
        });
    }
    else {
        $mediaList
            .on('click', '.options-dots', function(e) {
                e.stopPropagation();
                e.preventDefault();
                $(this)
                    .closest('.conversation-item')
                    .addClass('isActive');
                $('.conversation-list')
                    .addClass('disable-redirect');
            })
            .on('click', 'a.js-delete, a.js-block', function(e) {
                e.stopPropagation();
                e.preventDefault();
                $(this)
                    .closest('.conversation-item')
                    .removeClass('isActive');
                $('.conversation-list')
                    .removeClass('disable-redirect');
            });

    }
    $mediaList.on('click', function(e) {
        $(this)
            .find('.conversation-item')
            .removeClass('isActive');
        $('.conversation-list')
            .removeClass('disable-redirect');
    });
};
