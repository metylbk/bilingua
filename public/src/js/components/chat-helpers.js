window.fromChat = false;

window.renderAgain = undefined;

window.notificationThread;

window.initialLoad = true;

window.getUnreadMessages = function(threadId) {
    if (threadId) {
        return JSON.parse(localStorage.unreadMessages)[threadId];
    }
    return JSON.parse(localStorage.unreadMessages);
};

window.setUnreadMessages = function(newState) {
    localStorage.unreadMessages = JSON.stringify(newState);
};

window.incrementUnreadMessage = function(threadId) {
    var temp = window.getUnreadMessages();
    temp[threadId]++;
    window.setUnreadMessages(temp);
};

window.nullUnreadMessages = function(threadId) {
    var temp = window.getUnreadMessages();
    temp[threadId] = 0;
    window.setUnreadMessages(temp);
};

if (!localStorage.unreadMessages) {
    window.setUnreadMessages({});
}

// Get scroll to bottom on each render
// Cheralathans's function
window.scrollUpdate = function(status) {
    var element = document.querySelector('.messages-wrapper');
    if (status == 'read') {
        $(element)
            .scrollTop($(element)[0].scrollHeight);
    }
    else if (status == 'unread') {
        $(element)
            .scrollTop(
                $('#message-unread-status')
                    .offset().top - $(element)
                    .offset().top + $(element)
                    .scrollTop()
            );
    }
};

// Parminder's function
window.scrollUpdate = function() {
    var element1 = document.querySelector('.messages-wrapper');
    var element2 = document.querySelector('.messages-content');
    element1.scrollTop = element1.scrollHeight;
    element2.scrollTop = element2.scrollHeight;
};

// window.LoginLogger = function() {
//     $.ajax({
//         url: 'https://staging.bilingua.io/auth/cookie/check',
//         xhrFields: {
//             withCredentials: true
//         }
//     }).done(function(data) {
//         console.log(data);
//     });
// }

window.totalUnreadMessageCount = function(obj) {
    var totalUnreadMessageCount = 0,
        el;
    for (el in obj) {
        if (obj.hasOwnProperty(el)) {
            totalUnreadMessageCount += parseFloat(obj[el]);
        }
    }
    return totalUnreadMessageCount;
};

window.renderUnreadMessageCount = function() {
    var totalUnreadMessages = window.totalUnreadMessageCount(JSON.parse(localStorage.unreadMessages)),
        favCanvas,
        favContext,
        favImage,
        favLink;

    if (totalUnreadMessages > 0) {
        $('.link .total-unread-messages').html('<span class="badge bg-red unread-messages">' + totalUnreadMessages + '</span>');
    }
    else {
        $('.link .total-unread-messages').html('');
    }

    //unread count favicon generator
    if (totalUnreadMessages > 0) {
        favCanvas = document.createElement('canvas'),
        favCanvas.width = 32;
        favCanvas.height = 32;
        favContext = favCanvas.getContext('2d');
        favImage = new Image();
        favImage.src = '../images/favicons/favicon-plain.png';
        favImage.onload = function() {
            favContext.drawImage(favImage, 0, 0);
            favContext.fillStyle = '#269ad5';
            favContext.font = 'bold 18px sans-serif';
            favContext.lineWidth = 1;
            favContext.strokeStyle = '#269ad5';
            if (totalUnreadMessages <= 9) {
                favContext.fillText(totalUnreadMessages, 11, 22);
                favContext.strokeText(totalUnreadMessages, 11, 22);
            }
            else {
                favContext.fillText('9+', 8, 22);
            }

            favLink = document.querySelector('link[rel*="icon"]') || document.createElement('link');
            favLink.type = 'image/png';
            favLink.rel = 'shortcut icon';
            favLink.href = favCanvas.toDataURL('image/png');
            document.getElementsByTagName('head')[0].appendChild(favLink);
        };
    }
    else {
        favLink = document.querySelector('link[rel*="icon"]') || document.createElement('link');
        favLink.type = 'image/x-icon';
        favLink.rel = 'shortcut icon';
        favLink.href = '../images/favicons/favicon.ico';
        document.getElementsByTagName('head')[0].appendChild(favLink);
    }
};
