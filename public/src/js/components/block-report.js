(function($, document, window) {

    'use strict';

    // Block/Report Popup Function
    /**
     * Popup for block/report user
     */

    function blockReport() {
        $('.js-block').on('click', function() {
            var popupHTML = '<div class="popup options-popup">' +
                '<div class="content-block">' +
                '<h1 class="options-popup__user">Block user</h1>' +
                '<p class="options-popup__text">Blocking user will remove any conversations you had with this user, remove that user from your search results and remove you from his search results.</p>' +
                '<div class="list-block">' +
                '<label class="label-checkbox item-content">' +
                '<input type="checkbox" name="my-checkbox" id="block-checkbox" value="reasons">' +
                '<div class="item-media media">' +
                '<i class="icon icon-form-checkbox report-checkbox"></i>' +
                '</div>' +
                '<div class="item-inner">' +
                '<div class="item-title options-popup__title">Report user</div>' +
                '</div>' +
                '</label>' +
                '<div class="options-popup__reason">' +
                '<p>Tell us why we should block this user:</p>' +
                '<div class="align-top">' +
                '<div class="item-content">' +
                '<div class="item-inner">' +
                '<div class="item-input">' +
                '<textarea id="text-area"></textarea>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<p class="options-popup__links"><a href="#" class="close-popup">Cancel</a>' +
                '<a href="#" class="block-button">Block</a>' +
                '<a href="#" class="report-button is-disabled">Report</a></p>' +
                '</div>' +
                '</div>';
            myApp.popup(popupHTML);
            showReport();
            disabledButton();
            blockButton();
            reportButton();
        });

        /**
         * disable buttons if textarea is empty
         */
        function disabledButton() {
            $('.options-popup').on('keyup', '#text-area', function() {

                if ($(this).val().length === 0) {
                    $('.report-button').addClass('is-disabled');
                }
                else {
                    $('.report-button').removeClass('is-disabled');
                }
            });

        }


        /**
         * show textarea if report checked
         */
        function showReport() {
            $('.options-popup').on('change', '#block-checkbox', function(e) {
                e.stopPropagation();

                if (this.checked) {
                    $('.options-popup__reason').show();
                    $('.block-button').hide();
                    $('.report-button').show();


                }
                else {
                    $('.options-popup__reason').hide();
                    $('.block-button').show();
                    $('.report-button').hide();
                }
            });
        }

        /**
         * Below to be replaced once login is integrated & api.js calls auth.js instead.
         */
        function blockButton() {
            $('.options-popup').on('click', '.block-button', function() {

                var id = mainView.activePage.query.id.slice(5);
                makeApiRequest('users/' + id + '/block', null, 'POST', true).done(function(res) {

                    myApp.closeModal('.options-popup');
                    myApp.alert('User blocked', 'Success!');
                    mainView.router.loadPage('chats.html');

                });
            });
        }

        function reportButton() {
            $('.options-popup').on('click', '.report-button', function() {

                var reason = { reason: $('#text-area').val().trim() };

                makeApiRequest('feedback', reason, 'POST', true).done(function(res) {

                    myApp.closeModal('.options-popup');
                    myApp.alert('User reported', 'Success!');
                    mainView.router.loadPage('chats.html');

                });
            });
        }
    }


    // Init blockReport()
    myApp.onPageInit('user-profile', function(page) {
        blockReport();
    });
    myApp.onPageInit('chatroom', function(page) {
        blockReport();
    });
}(jQuery, document, window));
