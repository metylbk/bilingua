window.messageDots = function() {
    var pressEvent = myApp.support.touch ? 'press' : 'click';
    var chatPage = $('.chat');

    chatPage
        .on('click', '.chat__received-dots, .chat__send-dots', function(e) {
            e.stopPropagation();
            $(this)
                .parent()
                .addClass('is-active');
        })

        .on('click', '.bubble-options li a', function() {
            $(this)
                .closest('.bubble-options')
                .removeClass('is-active');
        });

    $('.chat-screen')
        .on('click', function(e) {
            if ($(e.target)
                    .closest('.bubble-options').length === 0) {
                $('.bubble-options')
                    .removeClass('is-active');
            }
        });

    chatPage.on(pressEvent, '.message', function(e) {
        // var $this = $(this);
        e.stopPropagation();
        $(this).closest('.message, .bubble-options').addClass('is-active');

    })
        .on('click', '.bubble-options li a', function(e) {
            e.stopPropagation();
            $(this).closest('.message').removeClass('is-active');

        });

    $('body, .page-content').on('click', function(e) {
        if ($(e.target)
                .closest('.message').length === 0) {
            $('.message')
                .removeClass('is-active');
        }
    });

    if (myApp.support.touch) {
        chatPage.on('contextmenu', '.message', function(e) {
            e.preventDefault();
        });
    }
};

