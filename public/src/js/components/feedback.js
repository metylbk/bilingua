window.feedback = {
    showPopup: function() {
        $('.feedback')
            .on('click', function() {
                var popupHTML = '<div class="popup options-popup">' +
                    '<div class="content-block">' +
                    '<h1 class="options-popup__user">Feedback</h1>' +
                    '<p class="options-popup__text">Thank you for using Bilingua. We hope you enjoy our app and we are always looking for feedback to help us improve.</p>' +
                    '<div class="list-block">' +
                    '<div class="align-top">' +
                    '<div class="item-content">' +
                    '<div class="item-inner">' +
                    '<div class="item-input">' +
                    '<textarea id="text-area"></textarea>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<p class="options-popup__links"><a href="#" class="close-popup">Cancel</a>' +
                    '<a href="" class="submit-button is-disabled">Submit</a></p>' +
                    '</div>' +
                    '</div>';
                myApp.popup(popupHTML);
                disabledButton();
                submitButton();

                // $('.submit-button').on('click', function () {
                //     var feedback = $('#text-area').val();
                //     $('.external').attr('href', 'mailto:hello@bilingua.io?subject=Bilingua Feedback%20&body=' + feedback).attr('target', '_blank');
                //     myApp.alert('Thanks for your feedback', '');
                //     myApp.closeModal('.options-popup');
                // });
            });
    }
};


function disabledButton() {
    $('.options-popup')
        .on('keyup', '#text-area', function() {

            if ($(this)
                    .val().length === 0) {
                $('.submit-button')
                    .addClass('is-disabled');
            }
            else {
                $('.submit-button')
                    .removeClass('is-disabled');
            }
        });

}

function submitButton() {
    $('.options-popup')
        .on('click', '.submit-button', function() {

            var feedback = $('#text-area')
                .val().trim();
            makeApiRequest('feedback', {
                message: feedback
            }, 'POST', true)
                .done(
                    function(res) {

                        myApp.alert('Thanks for your feedback', 'Success!');
                        mainView.router.loadPage('chats.html');
                        myApp.closeModal('.options-popup');
                    });

        });
}
