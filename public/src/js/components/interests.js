(function($, document, window) {
    'use strict';

    var makeApiRequest = window.makeApiRequest;

    var interestsArr;

    window.Interests = {

        getInterests: function(callback) {
            var self = this;
            makeApiRequest('interests', '', 'GET', true)
                .done(function(response) {
                    self.setInterestsArr(response);

                    typeof callback === 'function' && callback(true);
                })
                .fail(function() {
                    typeof callback === 'function' && callback(false);
                });

        },

        getInterestsArr: function() {
            return interestsArr;
        },

        setInterestsArr: function(response) {
            interestsArr = response;
        },

        linkChips: function() {
            $('.chip-label')
                .on('click', function() {
                    window.filterParams = {
                        interests: [],
                        minAge: 13,
                        maxAge: 80,
                        country: ''
                    };
                    window.filterParams.interests = [$(this)
                        .closest('.custom-chip')
                        .attr('data-languageID')];
                    mainView.loadPage('partners.html');
                });
        }
    };

}(jQuery, document, window));
