(function($, document, window) {

    'use strict';

    $(document)
        .on('pageInit', '*', function(e) {
            makeApiRequest('users/me', null, 'GET', true).done(function(res) {
                var user = res;

                // If user avatar exists in DB use it, otherwise default image will used from HTML
                if (user.avatar) {
                    $('#user-box-avatar, .js-user-box-avatar').attr('src', user.avatar);
                }
            });
        });
}(jQuery, document, window));

