/**
 * Alert at the bottom of screen
 * @param message
 * @constructor
 */

window.BilinguaAlert = {

    show: function(message) {
        myApp.addNotification({
            message: message,
            additionalClass: 'bilingua-alert',
            hold: 5000
        });
    }
};
