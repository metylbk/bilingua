window.getLocation =
    function(callback) {

        // check if geolocation is available
        if (navigator.geolocation) {

            // get latitude and longitude coordinates
            navigator.geolocation.getCurrentPosition(function(position) {

                // pass these coordinates to map api as an input
                $.get(' https://maps.googleapis.com/maps/api/geocode/json?latlng= ' + position.coords.latitude + ' , ' + position.coords.longitude)
                    .done(function(res) {
                        var country;
                        var city;

                        // this wil store country
                        country = res.results[0].address_components.filter(function(item) {
                            if (item.types.indexOf('country') > -1) {
                                return true;
                            }
                        })[0].long_name;

                        // this will store the city
                        city = res.results[0].address_components.filter(function(item) {
                            if (item.types.indexOf('political') > -1 && item.types.indexOf('locality') > -1) {
                                return true;
                            }
                        })[0].long_name;

                        if (callback && typeof (callback) === 'function') {
                            //call the callback with these two parameters
                            callback(country, city);
                        }
                    })
                    .fail(function(err) {

                    });
            }, function(error) {
                var message = '';
                var strErrorCode = error.code.toString();
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        message = 'This website does not have permission to use ' + 'the Geolocation API';
                        break;
                    case error.POSITION_UNAVAILABLE:
                        message = 'The current position could not be determined.';
                        break;
                    case error.PERMISSION_DENIED_TIMEOUT:
                        message = 'The current position could not be determined ' + 'within the specified timeout period.';
                        break;
                }

                if (message == '') {
                    message = 'The position could not be determined due to ' + 'an unknown error (Code: ' + strErrorCode + ').';
                }

            }, {
                enableHighAccuracy: true
            });
        }

    };
