var valid = true;
myApp.onPageAfterAnimation('login', function(page) {
    /*
     Display the errors
     */
    function displayErrors() {
        $('.error-login').show();
        $('.error-reg').show();
    }


    $.validate({
        form: '#login-form , #registration-form',
        onError: function(form) {
            displayErrors();
        }
    });
});

function displayErrors() {
    var called = false;
    $('*[class^="error-msg-"]').each(function(index, element) {
        if ($(element).text() !== '') {
            $(element).parent().show();
            called = true;
        }
    });
    if (called) {
        $('.errors').slideDown('fast');
    }
}

function hideErrors() {
    $('.errors').hide();
    $('*[class^="error-msg-"]').parent().hide();
}

function validate(isRegex, check, element, errorElement, errorMessage, checkEmpty, fieldname) {
    if (checkEmpty) {
        if ($(element).val() === '') {
            $(errorElement)[0].innerHTML = fieldname + ' is required';
            valid = false;
        }
        else if (isRegex ? !check.test($(element).val()) : check) {
            $(errorElement)[0].innerHTML = errorMessage;
            valid = false;
        }
        else {
            $(errorElement)[0].innerHTML = '';
        }
    }
    else if (isRegex ? !check.test($(element).val()) : check) {
        $(errorElement)[0].innerHTML = errorMessage;
        valid = false;
    }
    else {
        $(errorElement)[0].innerHTML = '';
    }
}


myApp.onPageAfterAnimation('profile-filling', function(page) {
    hideErrors();

    $('.submit-btn').on('click', function(e) {
        valid = true;
        hideErrors();
        //Custom Validations

        //Check if gender was selected
        validate(false, $('.gender-selected').length != 1,
            '.color-red',
            '.error-msg-gender',
            'Please select an option for gender',
            false);

        //Check if avatar was uploaded
        validate(false, $('#avatar-image').attr('src') == 'images/placeholder_user_pic.png',
            '#avatar-image',
            '.error-msg-avatar',
            'Please upload an avatar',
            false);

        //Check format of firstname
        validate(true, /^([A-Za-z]+)$/,
            'input[name="firstname"]',
            '.error-msg-first',
            'First Name should only contain letters',
            true, 'First Name');

        //Check format of lastname
        validate(true, /^([A-Za-z]+)$/,
            'input[name="lastname"]',
            '.error-msg-last',
            'Last Name should contain only letters',
            true, 'Last Name');

        //Check format of country
        validate(true, /^([A-Z\sa-z]+)$/,
            'input[name="country"]',
            '.error-msg-country',
            'Country should contain only letters',
            true, 'Country');

        //Check age
        validate(false, $('input[name="age"]').val() < 13 || !(/^([0-9]+)$/.test($('input[name="age"]').val())),
            'input[name="age"]',
            '.error-msg-age',
            'You should be at least 13 to register',
            true, 'Age');
        //END CUSTOM VALIDATIONS
        if (valid) {
            $('#profile-form').submit();
        }
        displayErrors();
        return false;
    });

});

myApp.onPageAfterAnimation('language-selection', function(page) {
    hideErrors();
    $('.submit-btn').on('click', function(e) {
        valid = true;
        hideErrors();
        //Check if speak languages added
        validate(false, $('.js-selected-speak').length == 0,
            '.js-selected-speak',
            '.error-msg-speak',
            'Please select at least one language you know',
            false);

        //Check if learning languages added
        validate(false, $('.js-selected-practice').length == 0,
            '.js-selected-practice',
            '.error-msg-practice',
            'Please select at least one language you want to learn',
            false);

        if (valid) {
            $('#language-form').submit();
        }
        displayErrors();
    });
});

myApp.onPageAfterAnimation('interests', function(page) {
    hideErrors();
    $('.finish').on('click', function(e) {
        valid = true;
        hideErrors();

        validate(false, $('.fa-check').length == 0,
            '.fa-check',
            '.error-msg-interests',
            'Please select at least 1 interest',
            false);

        if (valid) {
            $('#interests-form').submit();
        }

        displayErrors();
    });
});
