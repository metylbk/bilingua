window.filterConvos = {
    run: function() {
        $('.search__btn')
            .on('click', function() {
                var popupHTML = '';
                popupHTML += '<div class="popup options-popup">';
                popupHTML += '<div class="popup_header">Search/filter</div>';
                popupHTML += '<div class="content-block">';
                popupHTML += '<h1 class="options-popup__user">Filter by :</h1>';
                popupHTML += '<form id="filter">';
                popupHTML += '<div class="list-block">';
                popupHTML += '  <ul>';
                popupHTML += '    <li>';
                popupHTML += '      <div class="row">';
                popupHTML += '        <div class="col-100">';
                popupHTML += '          <div class="item-content">';
                popupHTML += '            <div class="item-inner">';
                popupHTML += '              <div class="item-title floating-label">Name:</div>';
                popupHTML += '              <div class="item-input">';
                popupHTML += '                  <input type="text" name="name">';
                popupHTML += '              </div>';
                popupHTML += '            </div>';
                popupHTML += '          </div>';
                popupHTML += '        </div>';
                popupHTML += '      </div>';
                popupHTML += '    </li>';
                popupHTML += '    <li>';
                popupHTML += '      <div class="row">';
                popupHTML += '        <div class="col-100">';
                popupHTML += '          <div class="item-content">';
                popupHTML += '            <div class="item-inner">';
                popupHTML += '              <div class="item-title floating-label">Interests:</div>';
                popupHTML += '              <div class="item-input">';
                popupHTML += '                  <input type="text" name="interests">';
                popupHTML += '              </div>';
                popupHTML += '            </div>';
                popupHTML += '          </div>';
                popupHTML += '        </div>';
                popupHTML += '      </div>';
                popupHTML += '    </li>';
                popupHTML += '    <li>';
                popupHTML += '      <div class="row">';
                popupHTML += '        <div class="col-100">';
                popupHTML += '          <div class="item-content">';
                popupHTML += '            <div class="item-inner">';
                popupHTML += '              <div class="item-title floating-label">Language:</div>';
                popupHTML += '              <div class="item-input">';
                popupHTML += '                  <input type="text" name="languages">';
                popupHTML += '              </div>';
                popupHTML += '            </div>';
                popupHTML += '          </div>';
                popupHTML += '        </div>';
                popupHTML += '      </div>';
                popupHTML += '    </li>';
                popupHTML += '  </ul>';
                popupHTML += '</div>';
                popupHTML += '<p class="options-popup__links"><a href="#" class="close-popup">Cancel</a>';
                popupHTML += '<a href="#" id="submitFilter">Submit</a></p>';
                popupHTML += '</form>';
                popupHTML += '</div>';
                popupHTML += '</div>';
                myApp.popup(popupHTML);


                $('#submitFilter')
                    .on('click', function() {
                        window.convFilters = {};
                        window.convFilters.name = $('input[name="name"]')
                            .val();
                        window.convFilters.interests = $('input[name="interests"]')
                            .val()
                            .split(',');
                        window.convFilters.languages = $('input[name="languages"]')
                            .val()
                            .split(',');
                        myApp.closeModal();
                    });
            });
    }
};
