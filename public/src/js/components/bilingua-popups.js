/**
 * Bilingua Popup
 * @type {{dailyRewards: bilinguaPopup.dailyRewards, inviteFriends: bilinguaPopup.inviteFriends}}
 */
window.BilinguaPopup = {

    /**
     * Daily Rewards Popup
     */
    dailyRewards: function() {
        var popupHTML = '<div class="popup bilingua-popup_daily-rewards">' +
            '<div class="content-block login-screen-content">' +
            '<h1 class="about-me-popup__title">Daily Reward!</h1>' +
            '<div class="daily-rewards__img list-block-label"><img src="https://placeholdit.imgix.net/~text?txtsize=33&txt=img&w=100&h=100"/><p class="daily-rewards__days">5 days in a row!</p><p class="daily-reward__points">Another consecutive login streak! To Thank you, we offer you 100 points!</p></div>' +
            '<div class="about-me-popup__save"><p class="buttons-row"><input type="submit" name="submit" value="Get Them!" class="button button-fill button-raised close-popup"></p></div>' +
            '</div>' +
            '</div>';
        myApp.popup(popupHTML);
    },

    /**
     * Invite Friends Popup
     */
    inviteFriends: function() {
        var popupHTML = '<div class="popup bilingua-popup__invite">' +
            '<div class="content-block login-screen-content">' +
            '<h1 class="about-me-popup__title">Share the love of languages</h1>' +
            '<div class="daily-rewards__img list-block-label"><img src="../../images/shiro/shiro-happy.png" />' +
            '<p class="daily-reward__points">I’m so glad you’re with us!</p>' +
            '<p>Would you share the love and invite your friends to join us?</p>' +
            '<p>Afterall, learning is more fun with friends!</p>' +
            '</div>' +
            '<div class="about-me-popup__save"><p class="buttons-row"><a href="invite-friends.html" class="button button-fill button-raised close-popup">Add Friends</a></div>' +
            '<a href="#" class="close-popup">close</a>' +
            '</div>' +
            '</div>';
        myApp.popup(popupHTML);
    }
};
