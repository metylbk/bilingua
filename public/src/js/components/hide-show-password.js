(function($, document, window) {

    'use strict';

    // Init hide/show password
    myApp.onPageInit('login my-settings', function(page) {
        $('.fa-eye').on('click', function() {
            // toggle our classes for the eye icon
            $(this).toggleClass('fa-eye').toggleClass('fa-eye-slash');
            // activate the hideShowPassword plugin
            $('#password, #password-reg').togglePassword();
        });
    });

}(jQuery, document, window));
