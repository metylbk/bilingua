(function($, document, window) {
    'use strict';

    $(document)
        .on('pageInit', '.page[data-page="my-profile"], .page[data-page="partners"], .page[data-page="chats"], .page[data-page="user-profile"]', function(e) {
            var headerHtml = '';
            $('.header__dots')
                .on('click', function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $(this)
                        .parent()
                        .addClass('isActive');
                });
            $('.header__options')
                .on('click', 'a', function() {
                    $(this)
                        .closest('.header__options')
                        .removeClass('isActive');
                });
            $('body, .page-content')
                .on('click', function(e) {
                    if ($(e.target)
                            .closest('.header__options').length === 0) {
                        $('.header__options')
                            .removeClass('isActive');
                    }
                });

            headerHtml += '<li><a href="my-settings.html" class="">Settings</a></li>';
            headerHtml += '<li><a href="invite-friends.html" class="">Invite Friends</a></li>';
            headerHtml += '<li><a href="who-can-find-me.html" class="">Who Can Find Me</a></li>';
            headerHtml += '<li><a href="about.html" class="">About</a></li>';
            headerHtml += '<li><a href="#" class="feedback">Feedback</a></li>';

            $('.dropdown')
                .html(headerHtml);

        });

}(jQuery, document, window));
