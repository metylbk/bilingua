// Initialize app
var myApp = new Framework7({
    material: true,
    showBarsOnPageScrollEnd: false,
    router: 'intro.html'
});

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true
});




window.Echo = new Echo({
    broadcaster: 'pusher',
    cluster: 'ap1',
    authEndpoint: 'https://k567d6c3nqbrdzb5.app.bilingua.io/' + 'broadcasting/auth',
    key: '8940857d7fcad22b2d0b'
});

function Notified(fromChat, notification) {
    //Do things with notification
    window.DisplayNotification(notification);
    if (typeof window.renderAgain == 'function') {
        window.renderAgain();
    }

    if (fromChat) {
        window.scrollUpdate();
    }
    else {
        window.notificationThread = notification.thread.id;
        if (!window.getUnreadMessages(window.notificationThread)) {
            window.nullUnreadMessages(window.notificationThread);
            window.incrementUnreadMessage(window.notificationThread);
        }
        else if (mainView.activePage.name !== 'chatroom') {
            window.incrementUnreadMessage(window.notificationThread);
        }
    }
}

window.listenNotifications = function(id) {
    //Notification
    Echo.private('App.User.' + id)
        .notification(function(notification) {
            var peers = notification.thread.type;
            var peersArray = peers.split('-');
            if (peersArray[0] != peersArray[1]) {
                Notified(window.fromChat, notification);
            }
        });
};

//Initialize pusher
//var pusher = new Pusher('8940857d7fcad22b2d0b',{
//  cluster: 'ap1'
//});

// If we need to use custom DOM library, let's save it to $$ variable:
// var $$ = Dom7;

// if user is already logged in then redirect to Conversations List page
if (Auth.isLoggedIn()) {
    localStorage.register = false;
    mainView.router.loadPage('chats.html');
}
else {
    /**
     * TODO: replace index.html with intro content
     */
    mainView.router.loadPage('intro.html');
}


/**
 * Detect touch support
 * Add .touchevents to html tag
 */
if (myApp.support.touch) {
    $('html')
        .addClass('touchevents');
}

/**
 * Function to get gender name from API returned value
 * @param gender
 * @returns {string}
 */
window.getGender = function(apiGender) {
    var genderArray = ['mars', 'venus', 'transgender-alt'];
    var iconHtml = '<i class="fa fa-' + genderArray[apiGender - 1] + '" aria-hidden="true"></i>';

    return iconHtml;
};


(function($, document, window) {

    'use strict';

    var view,
        len;

    // setup Raven for error reporting
    Raven.config('https://5fe55ae1a1464455b0524976589ff748@sentry.createl.la/3', { ignoreUrls: [/localhost/] }).install();

    // set URL prefix for Finmates API
    window.API_PREFIX = 'https://k567d6c3nqbrdzb5.app.bilingua.io/api';

    /**
     * Detect touch support
     * Add .touchevents to html tag
     */
    if (myApp.support.touch) {
        $('html')
            .addClass('touchevents');
    }

    /**
     * Back button redirecting to the previous page
     */

    $('.back')
        .on('click', 'a', function(e) {
            e.preventDefault();
            view = myApp.getCurrentView();
            len = view.history.length;
            if (len > 1) {
                view.router.back();
            }
        });

}(jQuery, document, window));
