(function($, document, window) {
    'use strict';

    window.WebFontConfig = {
        google: {
            families: ['Open+Sans', 'Montserrat']
        }
    };
    (function() {
        var wf = document.createElement('script');
        var s = document.getElementsByTagName('script')[0];
        wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        s.parentNode.insertBefore(wf, s);
    })();

}(jQuery, document, window));
