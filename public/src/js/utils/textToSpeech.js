/**
*
* This module  have the logic to call TTS provider in this moment the current
* provider is responseVoice JS library
*
*/

(function($, document, window) {
    'use strict';

    var TTS;

    var responVoiceLanguages = {
        'en': 'UK English Female',
        //'en': 'UK English Male',
        //'en': 'US English Female',
        'es': 'Spanish Female',
        'fr': 'French Female',
        'de': 'Deutsch Female',
        'it': 'Italian Female',
        'el': 'Greek Female',
        'hu': 'Hungarian Female',
        'tr': 'Turkish Female',
        'ru': 'Russian Female',
        'nl': 'Dutch Female',
        'sv': 'Swedish Female',
        'no': 'Norwegian Female',
        'ja': 'Japanese Female',
        'ko': 'Korean Female',
        'zh-CN': 'Chinese Female',
        'hi': 'Hindi Female',
        'sr': 'Serbian Male',
        'hr': 'Croatian Male',
        //'': 'Bosnian Male',
        'ro': 'Romanian Male',
        'ca': 'Catalan Male',
        //'': 'Australian Female',
        'fi': 'Finnish Female',
        'af': 'Afrikaans Male',
        'sq': 'Albanian Male',
        'ar': 'Arabic Male',
        //'': 'Armenian Male',
        'cs': 'Czech Female',
        'da': 'Danish Female',
        'eo': 'Esperanto Male',
        //'': 'Hatian Creole Female',
        'is': 'Icelandic Male',
        'id': 'Indonesian Female',
        'la': 'Latin Female',
        //'': 'Latvian Male',
        'mk': 'Macedonian Male',
        //'': 'Moldavian Male',
        //'': 'Montenegrin Male',
        'pl': 'Polish Female',
        //'': 'Brazilian Portuguese Female',
        'pr': 'Portuguese Female',
        //'': 'Serbo-Croatian Male',
        'sk': 'Slovak Female',
        //'es': 'Spanish Latin American Female',
        'sw': 'Swahili Male',
        'ta': 'Tamil Male',
        'th': 'Thai Female',
        'vi': 'Vietnamese Male',
        'cy': 'Welsh Male'
    };

    // Current TTS provider responsive Voice Lib it could be others
    function getTTSprovider() {
        return responsiveVoice;
    }


     // get the respnseVoice for the detected language
    function getResponsiveLanguage(lang) {
        return responVoiceLanguages[lang];
    }


    // make get call to the google api to detect the language
    // @ text to dectect the language
    // @ calback function to return and pass the detected language
    function detectLanguage(text, callback) {
        var lan;

        $.ajax({
            //The URL to process the request
            'url': 'https://www.googleapis.com/language/translate/v2/detect',
            //The type of request, also known as the "method" in HTML forms
            //Can be 'GET' or 'POST'
            'type': 'GET',
            //Any post-data/get-data parameters
            //This is optional
            'data': {
                'key': 'AIzaSyByhc-vuL99jHG3AxM5oq2d9jkLQ9tnoSY',
                'q': text
            },
            //The response from the server
            'success': function(data) {
                // detection of the language
                var responseData = data.data || undefined;
                var detections = responseData !== undefined ? responseData.detections != undefined ? responseData.detections : undefined : undefined;
                var language = detections !== undefined ? detections[0][0].language : undefined;

                // convert the google language code to responsiveVoice language code
                lan = getResponsiveLanguage(language) !== '' && getResponsiveLanguage(language) !== null ? getResponsiveLanguage(language) : undefined;

                // call the callback method with the text to  listed
                // pass the language
                if (lan !== undefined) {
                    callback(true, text, lan);
                }
                else {
                    callback(false, null, null);
                }
            },
            error: function() {
               // alert('Text to Speech not supported in this moment some error ocurried in the Internet');
                callback(false, null, null);
            }
        });
    }

    // function to call the responseVoice api
    function tts(flag, text, idiom) {
        if (flag) {
            //console.log('see ' + text + ' ' + idiom);
            getTTSprovider().speak(text, idiom);
        }
        else {
            alert('Language is not supported in this moment');
        }
    }

    TTS = {
        //Generate the text to Speech
        textToSpeech: function(text) {
            // Detect the language then send to read the text
            detectLanguage(text, tts);
        },
    };

    window.TTS = TTS;

}(jQuery, document, window));
