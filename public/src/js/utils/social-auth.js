(function($, document, window) {
    var hello = window.hello;

    var makeApiRequest = window.makeApiRequest;

    var SocialAuth;

    'use strict';

    // only proceed if hello.js has already been referenced
    if (typeof window.hello === 'undefined') {
        return;
    }


    /**
     * Lib for making Facebook and Google OAuth API calls.
     * @type {Object}
     */
    SocialAuth = {

        /**
         * Initialize the social login APIs.
         */
        init: function() {

            // initialize hello.js
            hello.init({
                facebook: '1684686755117990',
                google: '617421937193-dbfsdngr0j1i3g10ecdum7ugjf92t6r6.apps.googleusercontent.com'
            }, {
                'redirect_uri': '',
                scope: 'basic, email, friends, birthday'
            });
        },


        /**
         * Registers a new user or logs in an existing user.
         * @param  {string}   provider - Must be either 'facebook' or 'google' (required)
         * @param  {Function} callback - Called after the login request is completed
         */
        login: function(provider, callback) {
            var self = this;

            // if there is a valid token in localStorage, the user is already authenticated
            if (this.isLoggedIn()) {
                typeof callback === 'function' && callback(true);
                return;
            }

            // first authenticate with the provider
            hello(provider)
                .login()
                .then(function() {
                    var authToken, data;
                    if (provider == 'facebook') {
                        authToken = JSON.parse(localStorage.hello).facebook['access_token'];
                        data = {
                            'token': authToken, 'provider': 'facebook'
                        };
                    }
                    else if (provider == 'google') {
                        authToken = JSON.parse(localStorage.hello).google['access_token'];
                        data = {
                            'token': authToken, 'provider': 'google'
                        };
                    }

                    // after that make the login api request to own site
                    makeApiRequest('auth/oauth', data, 'POST')
                        .success(function(response) {
                            self.setLoginToken(true);
                            self.setAccountType(provider);
                            typeof callback === 'function' && callback(true);
                        })
                        .error(function() {
                            typeof callback === 'function' && callback(false);
                        });
                }, function() {
                    typeof callback === 'function' && callback(false);
                });
        },


        /**
         * Logs the current user out.
         * @param  {Function} callback - Called after the user is logged out
         */
        logout: function(callback) {
            hello.logout()
                 .then(callback);
        },


        /**
         * Checks if the user is logged in with a valid token.
         * @returns {Boolean} - is true when user is logged in
         */
        isLoggedIn: function() {
            return !!localStorage.BilinguaToken;
        },


        /**
         * Gets the login token from localStorage.
         * @returns {string} - the login token
         */
        getLoginToken: function() {
            return localStorage.BilinguaToken;
        },


        /**
         * Sets the login token in localStorage.
         * @param {string} token - the login token provided by the API (required)
         */
        setLoginToken: function(token) {
            localStorage.BilinguaToken = token;
        },


        /**
         * Sets the account type in localStorage.
         * @param {string} type - possible values 'google' and 'facebook'
         */
        setAccountType: function(type) {
            localStorage.accountType = type;
        },


        /**
         * Retrieves some user data from the provider's API & stores it in the DB.
         * @param {string} provider - Either 'google' or 'facebook' (required)
         */
        getUserData: function(provider) {
            var data;
            hello(provider)
                .api('me')
                .then(function(json) {
                    data = {};
                    data.email = json.email;
                    data['first_name'] = json['first_name'];
                    data['family_name'] = json['last_name'];
                    data['birthday'] = json['birthday'] || 'undefined';

                    if (provider == 'facebook') {
                        data.avatar = json.picture + '?width=200&height=200';
                    }
                    else if (provider == 'google') {
                        data.avatar = json.picture.split('?')[0] + '?sz=200';
                    }

                    makeApiRequest('users/me', data, 'PATCH', true);
                });
        }
    };

    SocialAuth.init();

    window.SocialAuth = SocialAuth;

}(jQuery, document, window));
