function detectCordova() {
  var ua = window.navigator.userAgent;
  var regApp = /Bilingua\/([\d.]+)/;
  return regApp.test(ua);
}

var cordovaApp = {
  loaded: false,
  initialize: function() {
    console.log('init');
    if (detectCordova() && !cordovaApp.loaded) {
      $.ajax({
        url: './js/facebookConnectPlugin.js',
        dataType: 'script',
        async: false
      });
      $.ajax({
        url: './cordova.js',
        dataType: 'script',
        async: false,
        success: function () {
          cordovaApp.bindEvents();
          cordovaApp.loaded = true;
        }
      });
    }else{

    }
  },

  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },

  onDeviceReady: function() {
    window.open = cordova.InAppBrowser.open;
    console.log('device ready!');
    //alert('deviceready');
    navigator.splashscreen.hide();
    //cordovaApp.initPush();
    cordovaApp.initGA();
    //cordovaApp.selfUpdate();
    // Attach the click handler to facebook button.
    $('#fbLoginBtn').on('click',function(e){
      e.preventDefault();
      cordovaApp.fbLogin();
    });


  },

  fbLogin: function() {
    var scope = ['email', 'public_profile', 'user_friends'];
    facebookConnectPlugin.login(scope, cordovaApp.fbOnLoginSuccess, cordovaApp.fbOnError);
  },

  fbOnLoginSuccess: function(response) {
    if (response.status === 'connected') {
      var api = 'me/?fields=id,name,email';
      var scope = ['public_profile', 'email'];
      facebookConnectPlugin.api(api, scope, cordovaApp.fbOnConnectSuccess, cordovaApp.fbOnError);
    } else if (response.status === 'not_authorized') {
      alert('You are logged in facebook, but not authorized our application! Why?');
    } else {
      alert('No Facebook? You can also register by email ;-)');
    }
  },

  fbOnConnectSuccess: function(response) {
    if (response && !response.error) {
      var api, scope;
      var currentUser = new fbUserInfo(response.id, response.name, response.email, '', response.first_name, response.last_name, response.gender, response.verified, 0, true);
      if (!isRegisteredSM(currentUser.user_id, currentUser.name, currentUser.email, 'facebook')) {
        api = 'me/?fields=id,name,email,first_name,last_name,verified,gender';
        scope = ['public_profile', 'email'];
        facebookConnectPlugin.api(api, scope, cordovaApp.fbRegisterUser, cordovaApp.fbOnError);
      } else {
        LoginSM(currentUser.user_id, currentUser.name, currentUser.email, 'facebook');
      }
    } else {
      alert('Problem with Facebook!');
    }
  },

  fbRegisterUser: function(response) {
    if (response && !response.error) {
      var fbPicture = 'https://graph.facebook.com/' + response.id + '/picture?width=200&height=200';
      var currentUser = new fbUserInfo(response.id, response.name, response.email, fbPicture,response.first_name, response.last_name, response.gender, response.verified, 0, true);
      var api = 'me/friends';
      var scope = ['user_friends'];
      facebookConnectPlugin.api(api ,scope, function(response) {
        if (response && !response.error) {
          currentUser.numFriends = response.summary.total_count;
        } else {
          currentUser.numFriends = -1;
        }
        if (currentUser.isFilled) {
          RegisterFromFacebook(currentUser);
        }
      }, cordovaApp.fbOnError);
    }

  },

  fbOnError: function(error) {
    alert(JSON.stringify(error));
  },

  googleAuthorize: function(options) {
    var deferred = $.Deferred();
    var authUrl = 'https://accounts.google.com/o/oauth2/auth?' + $.param({
          client_id: options.client_id,
          redirect_uri: options.redirect_uri,
          response_type: 'code',
          scope: options.scope
        });
    var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');

    $(authWindow).on('loadstart', function(e) {
      var url = e.originalEvent.url;
      var code = /\?code=(.+)$/.exec(url);
      var error = /\?error=(.+)$/.exec(url);

      if (code || error) {
        authWindow.close();
      }

      if (code) {
        $.post('https://accounts.google.com/o/oauth2/token', {
          code: code[1],
          client_id: options.client_id,
          client_secret: options.client_secret,
          redirect_uri: options.redirect_uri,
          grant_type: 'authorization_code'
        }).done(function(data) {
          deferred.resolve(data);
        }).fail(function(response) {
          deferred.reject(response.responseJSON);
        });
      } else if (error) {
        deferred.reject({
          error: error[1]
        });
      }
    });
    return deferred.promise();
  },

  googleLogin: function() {
    var data =  {
      client_id: '617421937193-dbfsdngr0j1i3g10ecdum7ugjf92t6r6.apps.googleusercontent.com',
      client_secret: 'PhYSVl2kMlvlJQF2vTOq4cne',
      redirect_uri: 'http://localhost',
      scope: 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
    };
    cordovaApp.googleAuthorize(data).done(function(data) {
      cordovaApp.googleGetProfile(data.access_token);
    });
  },

  googleGetProfile: function(accessToken) {
    var term = null;
    $.ajax({
      url: 'https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=' + accessToken,
      type: 'GET',
      data: term,
      dataType: 'json',
      error: function(jqXHR, text_status, strError) {},
      success: function(data) {
        var user = {
          user_id: data.id,
          name: data.name,
          email: data.email,
          photo: data.picture,
          firstName: data.given_name,
          lastName: data.family_name,
          gender: data.gender,
          verified: data.verified_email,
          numFriends: 0,
          isFilled: true
        };
        if (isRegisteredSM(user.user_id, user.name, user.email, 'google')) {
          LoginSM(user.user_id, user.name, user.email, 'google');
        } else {
          RegisterFromGoogle(user);
        }
      }
    });
  },

  initPush: function() {
    //alert('startinit');
    var push = PushNotification.init({
      android: {
        senderID: '861459321125',
        icon: 'icon',
        iconColor: '#FFE168'
      },
      ios: {
        //senderID: ""
        alert: 'true',
        badge: 'true',
        sound: 'true'
      },
      windows: {}
    });

    push.on('registration', cordovaApp.onPushRegistration);
    push.on('notification', cordovaApp.onPushNotication);
    push.on('error', cordovaApp.onPushError);
  },
  initPushWithNoRedirect: function() {
    //alert('startinit');
    var push = PushNotification.init({
      android: {
        senderID: '861459321125',
        icon: 'icon',
        iconColor: '#FFE168'
      },
      ios: {
        //senderID: ""
        alert: 'true',
        badge: 'true',
        sound: 'true'
      },
      windows: {}
    });

    push.on('registration', cordovaApp.onPushRegistrationWithNoRedirect);
    push.on('notification', cordovaApp.onPushNotication);
    push.on('error', cordovaApp.onPushError);
  },
  onPushRegistration: function(data) {
    //alert(data.registrationId);
    $.ajax({
      type: 'POST',
      async: false,
      url: 'http://cash2cash.sg/structure/data.php',
      data: {
        O: 'User',
        F: 'SetDeviceInfo',
        P: [localStorage['IdCash'],localStorage['CashPass'],device.uuid, data.registrationId]
      },
      dataType: 'json',
      success: function(data){
        console.log(JSON.stringify(data));
        window.location='index.html';
      },
      error: function(err) {
        alert(JSON.stringify(err));
      }
    });
  },
  onPushRegistrationWithNoRedirect: function(data) {
    //alert(data.registrationId);
    $.ajax({
      type: 'POST',
      async: false,
      url: 'http://cash2cash.sg/structure/data.php',
      data: {
        O: 'User',
        F: 'SetDeviceInfo',
        P: GetParameter([device.uuid, data.registrationId])
      },
      dataType: 'json',
      success: function(data){
        console.log(JSON.stringify(data));
      },
      error: function(err) {
        alert(JSON.stringify(err));
      }
    });
  },
  onPushNotication: function(data) {

  },

  onPushError: function(e) {
    console.log('Push notification initialize failed :' + e.message);
  },

  initGA: function() {
    //alert('initGA');
    window.analytics.startTrackerWithId('UA-67704881-2');
    window.analytics.trackView(document.title);
  },

  selfUpdate: function() {
    if (navigator.connection.type == Connection.WIFI) {
      console.log('checking update');
      var updateDialog = {
        mandatoryUpdateMessage: 'Cash2Cash is going to update now for an important issue, the page will be reloaded soon.',
        optionalUpdateMessage: 'Cash2Cash have an update, do you want to update now?',
        updateTitle: 'Update Notice!',
        optionalInstallButtonLabel: 'Yes',
        optionalIgnoreButtonLabel: 'Later',
        mandatoryContinueButtonLabel: 'Continue'
      };
      var options = {
        installMode: InstallMode.IMMEDIATE,
        updateDialog: updateDialog
      };
      window.codePush.sync(cordovaApp.onSyncSuccess, options, cordovaApp.onSyncing);
    }
  },

  onSyncSuccess: function(syncStatus) {
    console.log('syncStauts:' + syncStatus);
  },

  onSyncing: function(downloadProgress) {
    console.log(downloadProgress.receivedBytes + '/' + downloadProgress.totalBytes);
  }
};

