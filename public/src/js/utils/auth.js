(function($, document, window) {

    'use strict';

    var makeApiRequest = window.makeApiRequest;

    /**
     * Authentication lib to check whether an e-mail exists, login, register & get/set the auth token
     * from localStorage.
     * @type {Object}
     */
    var Auth = {

        /**
         * Checks if an e-mail already exists in the system.
         * @param  {string}   email -  The email of the user (required)
         * @param  {Function} callback - Called after the check is completed
         */
        checkEmail: function(email, callback) {

            var data = {
                'email': email
            };

            makeApiRequest('auth/email.check', data, 'POST')
                .done(function() {
                    typeof callback === 'function' && callback(true);
                })
                .fail(function() {
                    typeof callback === 'function' && callback(false);
                });
        },


        /**
         * Registers a new user in the system.
         * @param  {string}   email -  The email of the user (required)
         * @param  {string}   password - The password of the user (required)
         * @param  {Function} callback - Called after the registration request is completed
         */
        register: function(email, password, callback) {
            var self = this;

            var data = {
                'email': email, 'password': password
            };

            makeApiRequest('auth/reg', data, 'POST')
                .done(function(response) {
                    self.setLoginToken(true);
                    self.setAccountType('local');
                    typeof callback === 'function' && callback(true);
                })
                .fail(function(jqXHR) {
                    if (jqXHR.status === 422) {
                        myApp.alert('Oops, seems you deleted this account. You can contact us at <a href="mailto:hello@bilingua.io?subject=Account%20Reactivate%20Request:%20' + email + '">hello@bilingua.io</a> to reactivate it!', 'Error!');
                    }
                    typeof callback === 'function' && callback(false);
                });
        },


        /**
         * Logs the user in.
         * @param  {string}   email - The email of the user (required)
         * @param  {string}   password - The password of the user (required)
         * @param  {Function} callback - Called after the login request is completed
         */
        login: function(email, password, callback) {
            var self = this;
            var data;

            // if there is a valid token in localStorage, the user is already authenticated
            if (this.isLoggedIn()) {
                typeof callback === 'function' && callback(true);
                return;
            }

            // otherwise proceed with login process
            data = {
                'email': email, 'password': password
            };

            makeApiRequest('auth/login', data, 'POST', false)
                .success(function(response) {
                    self.setLoginToken(true);
                    self.setAccountType('local');
                    typeof callback === 'function' && callback(true);
                })
                .error(function(jqXHR) {
                    if (jqXHR.status === 422) {
                        myApp.alert('Oops, seems you deleted this account. You can contact us at <a href="mailto:hello@bilingua.io?subject=Account%20Reactivate%20Request:%20' + +'">hello@bilingua.io</a> to reactivate it!', 'OK');
                    }
                    typeof callback === 'function' && callback(false);
                });
        },


        /**
         * Logs the current user out.
         * @param  {Function} callback - Called after the user is logged out
         */
        logout: function(callback) {
            localStorage.removeItem('BilinguaToken');
            localStorage.removeItem('accountType');
            typeof callback === 'function' && callback(true);
        },


        /**
         * Checks if the user is logged in with a valid token.
         * @returns {Boolean} - is true when user is logged in
         */
        isLoggedIn: function() {
            // var flag;

            // if (localStorage.BilinguaToken) {
            //     makeApiRequest('auth/check', null, 'GET', true)
            //         .done(function() {
            //             flag = true;
            //         })
            //         .fail(function() {
            //             flag = false;
            //         });
            // } else {
            //     flag = false;
            // }

            // return flag;
            return !!localStorage.BilinguaToken;
        },


        /**
         * Sets the account type in localStorage.
         * @param {string} type - possible values 'local'
         */
        setAccountType: function(type) {
            localStorage.accountType = type;
        },


        /**
         * Get the account type of the user
         * @returns {string} - possible values are 'facebook', 'google' and 'local'
         */
        getAccountType: function() {
            return localStorage.accountType;
        },


        /**
         * Gets the login token from localStorage.
         * @returns {string} - the login token
         */
        getLoginToken: function() {
            return localStorage.BilinguaToken;
        },


        /**
         * Sets the login token in localStorage.
         * @param {string} token - the login token provided by the API (required)
         */
        setLoginToken: function(token) {
            localStorage.BilinguaToken = token;
        }
    };

    window.Auth = Auth;

}(jQuery, document, window));
