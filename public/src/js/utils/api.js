(function($, document, window) {

    'use strict';

    /**
     * Method for making ajax calls to Bilingua's API.
     * @param {string} endpoint - the api endpoint
     * @param {object} [data] - key:value pairs of the data to to sent to server
     * @param {string} [type] - the type of ajax request to make
     * @param {boolean} [authNeeded] - determines if the login token is needed
     * @returns {Object} jQuery ajax promise object
     */

    function makeApiRequest(endpoint, data, type, authNeeded) {
        return $.ajax({
            url: 'https://k567d6c3nqbrdzb5.app.bilingua.io/api/' + endpoint,
            type: type || 'GET',
            data: data ? JSON.stringify(data) : '',
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
        });
    }

    window.makeApiRequest = makeApiRequest;

    /**
     * Capture global ajax errors and report it to raven.js
     */
    $(document)
        .ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
            Raven.captureMessage(thrownError || jqXHR.statusText, {
                extra: {
                    type: ajaxSettings.type,
                    url: ajaxSettings.url,
                    data: ajaxSettings.data,
                    status: jqXHR.status,
                    error: thrownError || jqXHR.statusText,
                    response: jqXHR.responseText ? jqXHR.responseText.substring(0, 100) : ''
                }
            });
            //Fix the api fails
            if (jqXHR.status === 401) {
                Auth.logout();
                SocialAuth.logout();
                mainView.router.loadPage('login.html');
            }
        });

}(jQuery, document, window));
