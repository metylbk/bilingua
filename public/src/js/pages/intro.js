(function($, document, window) {

    'use strict';

    // Now we need to run the code that will be executed only for Feature page.
    myApp.onPageInit('intro', function(page) {

        var makeApiRequest = window.makeApiRequest;

        // Display splash screen
        splashScreen();

        // Initiate Slider
        makeApiRequest('sliders', null, 'GET', true).done(buildSlider).done(initiateSlider);

        // build slider
        function buildSlider(slidesArr) {
            var slider = slidesArr;

            var sliderHTML = '';

            $.each(slider, function(i) {
                sliderHTML += '<div class="swiper-slide">';

                // images need to have proportion of 700x907
                sliderHTML += '<img src="' + slider[i].image + '" />';
                sliderHTML += '<span>' + slider[i].description + '</span>';
                sliderHTML += '<div class="preloader"></div>';
                sliderHTML += '</div>';
            });


            $('.swiper-wrapper').html(sliderHTML);
        }

        // Feedback
        window.feedback.showPopup();

        // show splash screen for 2 seconds
        function splashScreen() {
            var splashHTML = '<div class="splash-wrapper"><img src="images/splash.png" class="splash" /></div>';
            var hideSplash = function() {
                $('.splash-wrapper').remove();
            };
            $('#wrapper').prepend(splashHTML);

            setTimeout(hideSplash, 2000);
        }

        // initiate framework7 slider
        function initiateSlider() {

            // initiate slider
            new Swiper('.swiper-container', {
                speed: 400,
                preloadImages: false,
                lazyLoading: true,
                keyboardControl: true,
                pagination: '.swiper-pagination',
                paginationHide: false,
                paginationClickable: true,
                nextButton: '.swiper-button-next',
                onSliderMove: function(e) {
                    if (e.isEnd) {
                        mainView.router.loadPage('login.html');
                    }

                }
            });

            // disable swiping on desktop
            if (!myApp.support.touch) {
                $('.swiper-container').addClass('swiper-no-swiping');
            }
        }

    });

}(jQuery, document, window));
