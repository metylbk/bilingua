(function($, document, window) {

    'use strict';

    myApp.onPageInit('invite-friends', function(page) {

        var makeApiRequest = window.makeApiRequest;
        var $googleContacts = $('.google-contacts');
        var list = [];
        var shareurl = 'http://www.facebook.com/dialog/share?app_id=1684686755117990&href=https://bilingua.io/&redirect_uri=https://app.bilingua.io/&mobile_iframe=true&quote="Designed to be your companion as you enhance and enrich your language skills, Bilingua connects you with other users who are fluent in a language you are learning."';

        makeApiRequest('users/me', null, 'GET', true)
            .done(function(res) {
                var user = res;
                if (user.avatar) {
                    $('#user-avatar, #user-box-avatar')
                        .attr('src', user.avatar);
                }
            });

        window.feedback.showPopup();

        FB.init({
            appId: '1684686755117990', cookie: true, status: true, xfbml: true
        });

        function shareToFacebookFriends() {
            FB.ui({
                method: 'send',
                name: 'Join me at Bilingua',
                link: 'https://bilingua.io/',
                description: 'Designed to be your companion as you enhance and enrich your language skills, Bilingua connects you with other users who are fluent in a language you are learning.',
                picture: '/images/shiro/shiro-happy.png'
            });
        }


        if (Auth.getAccountType() === 'local') {
            $('.connect-google')
                .show();
            $('.connect-facebook')
                .show();
            $('.google-contacts')
                .hide();
            $('.facebook-contacts')
                .hide();
            $('#fbShare')
                .hide();
        }
        else if (Auth.getAccountType() === 'google') {
            $('.connect-facebook')
                .show();
            $('.facebook-contacts')
                .hide();
            $('#fbShare')
                .hide();
            pullGoogleData();
        }
        else {
            myApp.showTab('#tab-2');
            $('.connect-google')
                .show();
            $('.google-contacts')
                .hide();
            $('.connect-facebook')
                .show();
            $('#fbBtn')
                .hide();
            //pullFacebookData();
        }


        /**
         * Social login using Facebook/Google.
         */
        function socialLogin(provider) {
            myApp.showPreloader('Connecting');

            Auth.logout();
            SocialAuth.logout();


            SocialAuth.login(provider, function(status) {
                if (status) {
                    myApp.hidePreloader();
                    SocialAuth.getUserData(provider);
                    onLogin();
                }
                else {
                    myApp.hidePreloader();
                    myApp.alert('Something went wrong. Please try again.', 'Error');
                }
            });
        }

        function onLogin() {
            mainView.router.refreshPage();
        }


        $('#fbBtn')
            .on('click', function(e) {
                e.preventDefault();
                socialLogin('facebook');
            });
        $('#googleBtn')
            .on('click', function(e) {
                e.preventDefault();
                socialLogin('google');
            });
        $('#fbShare')
            .on('click', function(e) {
                e.preventDefault();
                // Mobile friendly share dialog
                if (myApp.support.touch == true) {
                    window.open(shareurl);
                }
                // Use new FB.ui share method
                else {
                    shareToFacebookFriends();
                }
            });

        // pulling google friends & contacts
        function getGoogleFriends() {
            var deferred = $.Deferred();
            hello('google')
                .api('/me/friends')
                .then(function(data) {
                    deferred.resolve(data);
                });
            return deferred;
        }

        function getGoogleContacts() {
            var deferred = $.Deferred();
            hello('google')
                .api('/me/contacts')
                .then(function(data) {
                    deferred.resolve(data);
                });
            return deferred;
        }

        function pullGoogleData() {
            myApp.showIndicator();
            $.when(
                getGoogleFriends(),
                getGoogleContacts()
                )
                .then(function(friendsData, contactsData) {
                    var googleData = friendsData.data.concat(contactsData.data);
                    createContactsList(googleData, 'google');
                    myApp.hideIndicator();
                });
        }

        // pulling facebook friends
        /*function shareToFacebookFriends() {
         hello('facebook').api('me/share','post',{
         message: 'Designed to be your companion as you enhance and enrich your language skills, Bilingua connects you with other users who are fluent in a language you are learning.',
         link: 'https://bilingua.io/',
         picture: 'https://bilingua.io/app/uploads/2016/08/top-bar-logo.png'
         })
         }*/

        /*function pullFacebookData() {
         $.when(
         getFacebookFriends()
         )
         .then(function (friendsData) {
         createContactsList(friendsData.data, 'facebook');
         });
         }*/


        function createContactsList(data, provider) {
            var html = '';

            $.each(data, function(i, obj) {

                var picture = '',
                    name = '';

                if (obj.picture) {
                    picture = obj.picture;
                }
                else {
                    picture = 'http://www.sparkle.com/wp-content/uploads/2015/03/photodune-3336023-blue-diamond-l-2.jpg';
                }

                if (obj.name) {
                    name = obj.name;
                }
                else {
                    name = obj.email;
                }


                html += '<li data-email="' + obj.email + '">';
                html += '<div class="item-content contact">';
                html += '<div class="item-media contact__media">';
                html += '<img src="' + picture + '">';
                html += '</div>';
                html += '<div class="item-inner">';
                html += '<label class="label-checkbox item-content contact__checkbox">';
                html += '<input class="check" type="checkbox" name="my-checkbox" value="contact">';
                html += '<div class="item-media">';
                html += '<i class="icon icon-form-checkbox"></i>';
                html += '</div>';
                html += '</label>';
                html += '<div class="item-title-row">';
                html += '<div class="item-title contact__name">' + name + '</div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</li>';

            });

            $('#' + provider + '-contacts-list')
                .append(html);

        }


        // if the checkbox is checked show the INVITE button for GOOGLE contacts
        $googleContacts.on('change', '#checkAllGoogle, .check', function(e) {
            var allChecked = false;

            if (e.target.id === 'checkAllGoogle' && this.checked) {
                allChecked = true;
            }
            else {
                $googleContacts.find('.check')
                    .each(function(i) {
                        if (this.checked) {
                            allChecked = true;
                            return false;
                        }
                    });
            }

            if (allChecked) {
                $('.invite-google')
                    .show();
            }
            else {
                $('.invite-google')
                    .hide();
            }
        });


        // if the checkbox is checked show the INVITE button for FACEBOOK contacts
        /*var $facebookContacts = $('.facebook-contacts');
         $facebookContacts.on('change', '#checkAllFacebook, .check', function (e) {
         var allChecked = false;

         if (e.target.id === 'checkAllFacebook' && this.checked) {
         allChecked = true;
         }
         else {
         $facebookContacts.find('.check').each(function (i) {
         if (this.checked) {
         allChecked = true;
         return false;
         }
         });
         }

         if (allChecked) {
         $('.invite-facebook').show();
         }
         else {
         $('.invite-facebook').hide();
         }
         });*/

        $('#inviteGoogle')
            .on('click', function(e) {
                myApp.showIndicator();
                e.preventDefault();
                $googleContacts.find('.check:checked')
                    .each(function(i) {
                        var email = $(this)
                            .closest('li')
                            .attr('data-email');
                        list.push(email);
                    });
                makeApiRequest('invite', {
                    emails: list
                }, 'POST', true)
                    .done(function(data) {
                        myApp.hideIndicator();
                        myApp.alert('', 'Invitation Sent!', function() {
                            mainView.router.refreshPage();
                        });
                    });
            });


        //grab the email from the checked contact FACEBOOK
        /*$facebookContacts.find('.check:checked').each(function (i) {
         var email = $(this).closest('li').data('data-email');

         });*/


        //check all contacts
        $('#checkAllGoogle')
            .change(function() {
                $('.check')
                    .prop('checked', $(this)
                        .prop('checked'));
            });
    });

}(jQuery, document, window));
