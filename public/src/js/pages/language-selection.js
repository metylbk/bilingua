(function($, document, window) {

    'use strict';


    myApp.onPageInit('language-selection', function(page) {
        var makeApiRequest = window.makeApiRequest;
        // Init limit variables.
        var languagesSpoken = 0;
        var languagesPracticed = 0;

        myApp.showIndicator();

        // Feedback
        window.feedback.showPopup();


        // Fetch list of languages from the API & populate the dropdown menus.
        makeApiRequest('languages', null, 'GET', true)
            .done(dropdownInit);


        // Hide dropdown menus.
        $('.language-search-dropdown')
            .hide();


        // Hide respective dropdown menu if user clicks anywhere outside the search wrapper.
        $('.language-selection-wrapper')
            .on('click', function(e) {
                if (!$(e.target)
                        .parents()
                        .is('#searchWrapperSpeak')) {
                    $('#dropdownSpeak')
                        .hide();
                }

                if (!$(e.target)
                        .parents()
                        .is('#searchWrapperPractice')) {
                    $('#dropdownPractice')
                        .hide();
                }
            });


        // Initialize search bar functionalities.
        initSearch('#searchInputSpeak', 'Speak');
        initSearch('#searchInputPractice', 'Practice');

        function initSearch(id, section) {
            var $id = $(id);
            var $clrBtn = $(id)
                .siblings('.fa-close');
            var $body = $('body')
                .not($id);

            // Hide 'x' button in search bar.
            $clrBtn.hide();

            // Clear search bar when 'x' is clicked.
            $clrBtn.on('click', function(e) {
                var $clearBtn = $(this);
                var $searchBar = $clearBtn.siblings('input');

                $clearBtn.hide();
                $searchBar.val('');
                $searchBar.trigger('input');
            });

            // Show/hide dropdown menu when search bar is clicked.
            $id.on('click', function(e) {
                toggleDropdown($(this), $('#dropdown' + section), true);
                return false;
            });

            $body.on('click', function(e) {
                toggleDropdown($(this), $('#dropdown' + section), false);
                // return false;
            });

            // Filter dropdown menu items on input in search bar.
            $id.on('input', function(e) {
                filterLanguages(section, e);
            });
        }


        // Init dropdown buttons.
        $('.dropdown-btns a')
            .on('click', function() {
                $('#dropdownSpeak')
                    .hide();
                $('#dropdownPractice')
                    .hide();
            });


        // Submit the selected spoken & practiced languages.
        $('#language-form')
            .submit(submitLanguages);


        /*
         * Populates the dropdown menus.
         */
        function dropdownInit(languagesArr) {
            var dropdownHTML = '<div class="col-100 error-search"><p>No languages found</p></div>';
            var dropdownLanguages = '';
            var langName;
            var mainLangs = [], sortableLangs = [];

            /*
             This loop sorts the language array keeping some exceptions at top
             */
            languagesArr.forEach(function(language, index) {
                if (language.is_major !== 0) {
                    mainLangs.push(language);
                }
                else {
                    sortableLangs.push(language);
                }
            });

            languagesArr = mainLangs.concat(sortableLangs.sort(function(a, b) {
                if (a.english_name < b.english_name) {
                    return -1;
                }
                else if (b.english_name < a.english_name) {
                    return 1;
                }
                return 0;
            }));

            languagesArr.forEach(function(language, index) {
                langName = toTitleCase(language.english_name);
                if (language.is_major === 0) {
                    dropdownLanguages += genDropdownItem(language.id, langName, language.flag);
                }
                else {
                    dropdownHTML += genDropdownItem(language.id, langName, language.flag);
                }
            });

            dropdownHTML += dropdownLanguages;

            $('.language-search-dropdown')
                .children('.dropdown-items')
                .append(dropdownHTML);
            $('.error-search')
                .hide();

            // Attach event listeners to dropdown items.
            $('.language-search-dropdown-item')
                .on('click', selectLanguage);

            // Fetch user's current language preferences.
            fetchUserLanguages();
        }

        /*
         * Converts the lowercase language name to title case.
         */
        function toTitleCase(str) {
            return str.replace(/\w\S*/g, function(txt) {
                return txt.charAt(0)
                        .toUpperCase() + txt.substr(1)
                        .toLowerCase();
            });
        }

        /*
         * Generates the HTML for every dropdown menu item.
         */
        function genDropdownItem(langID, langName, langFlag) {
            var item = '<div class="col-50">' +
                '<div class="row no-gutter language-search-dropdown-item" data-language="' + langName.toLowerCase() + '" data-languageID="' + langID + '">' +
                '<span class="col-5"></span>' +
                '<span class="col-65 language-name">' + langName + '</span>' +
                '<span class="col-5"></span>' +
                '<span class="col-20">' +
                '<div class="language-flag" style="background-image: url(\'' + langFlag + '\'), url(\'/images/flags/earth.png\')"></div>' +
                '</span>' +
                '<span class="col-5"></span>' +
                '</div>' +
                '</div>';

            return item;
        }


        /*
         * Fetches & displays the list of user's selected languages through the REST API.
         */
        function fetchUserLanguages() {
            makeApiRequest('users/me', null, 'GET', true)
                .done(selectUserLanguages);
        }

        function selectUserLanguages(userProfile) {
            var spokenLangs = userProfile['spoken_languages'];
            var practicedLangs = userProfile['learning_languages'];


            spokenLangs.forEach(function(spokenLang, index) {
                $('#dropdownSpeak')
                    .find('.language-search-dropdown-item[data-languageID="' + spokenLang.id + '"]')
                    .trigger('click');

                $('.js-selected-speak')
                    .last()
                    .find('.proficiency-slider > .range-slider > input')
                    .val(spokenLang.pivot.proficiency);
                $('.js-selected-speak')
                    .last()
                    .find('.proficiency-slider > .range-slider > input')
                    .trigger('input');
            });

            practicedLangs.forEach(function(practicedLang, index) {
                $('#dropdownPractice')
                    .find('.language-search-dropdown-item[data-languageID="' + practicedLang.id + '"]')
                    .trigger('click');

                $('.js-selected-practice')
                    .last()
                    .find('.proficiency-slider > .range-slider > input')
                    .val(practicedLang.pivot.proficiency);
                $('.js-selected-practice')
                    .last()
                    .find('.proficiency-slider > .range-slider > input')
                    .trigger('input');
            });

            $('#dropdownSpeak')
                .hide();
            $('#dropdownPractice')
                .hide();

            myApp.hideIndicator();
        }


        /*
         * Toggles the dropdown menu when search bar is clicked.
         */
        function toggleDropdown($searchBar, $dropdown, show) {
            if (show) {
                $dropdown.show();
            }
            else {
                $dropdown.hide();
            }
        }


        function LangRemover(wrapperClass, langName) {
            $(wrapperClass)
                .each(function() {
                    if ($(this)
                            .find('.language-name')
                            .text()
                            .trim() == langName) {
                        $(this)
                            .remove();
                        return false;
                    }
                });
        }


        /*
         * Toggles the 'isSelected' class when an item is selected in the dropdown menu & adds/removes the
         * selected language to/from the appropriate list.
         */
        function selectLanguage() {
            var $selectedLang = $(this);
            var langData = $selectedLang.attr('data-language');
            var langID = $selectedLang.attr('data-languageID');
            var langName = $selectedLang.children('.language-name')
                .text()
                .trim();
            var langFlag = $selectedLang.find('.language-flag')
                .attr('style');
            var $selectedSection = $selectedLang.closest('.language-search-dropdown');
            var selectedSpeak = $selectedSection.is('#dropdownSpeak');
            var selectedPractice = $selectedSection.is('#dropdownPractice');
            var $speakSelected = $('#dropdownSpeak')
                .find('.language-search-dropdown-item[data-languageid=' + langID + ']');
            var $practiceSelected = $('#dropdownPractice')
                .find('.language-search-dropdown-item[data-languageid=' + langID + ']');
            var tempText = selectedSpeak ? 'spoken' : 'practiced';
            var $searchBar;
            var output;

            if ($selectedLang.hasClass('isSelected')) {
                $selectedLang.removeClass('isSelected');


                LangRemover('.js-selected-speak', langName);
                LangRemover('.js-selected-practice', langName);

                if (selectedSpeak) {
                    $practiceSelected.removeClass('isSelected');
                    languagesSpoken--;
                }
                else if (selectedPractice) {
                    $speakSelected.removeClass('isSelected');
                    languagesPracticed--;
                }
            }
            else {
                output = '<div class="col-50">' +
                    '<div class="language-selected-name">' +
                    '<div class="row">' +
                    '<div class="col-5 deselect-btn"><i class="fa fa-close" aria-hidden="true"></i></div>' +
                    '<div class="col-65 language-name">' + langName + '</div>' +
                    '<div class="col-30">' +
                    '<div class="language-flag -whiteborder" style="' + langFlag + '"></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-50">' +
                    '<div class="language-selected-proficiency">' +
                    '<div class="row proficiency-item -advanced">' +
                    '<div class="proficiency-text">' +
                    'advanced' +
                    '</div>' +
                    '<div class="proficiency-slider item-input">' +
                    '<div class="range-slider">' +
                    '<input type="range" min="1" max="5" value="3" step="1">' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

                if (selectedSpeak && languagesSpoken < 5) {
                    output = '<div class="row language-selected-item js-selected-speak" data-language="' + langData + '" data-languageID="' + langID + '">' + output + '</div>';
                    $('#selectedSpeak')
                        .children('.col-90')
                        .append(output);

                    $practiceSelected.addClass('isSelected');
                    $selectedLang.addClass('isSelected');

                    languagesSpoken++;
                }
                else if (selectedPractice && languagesPracticed < 5) {
                    output = '<div class="row language-selected-item js-selected-practice" data-language="' + langData + '" data-languageID="' + langID + '">' + output + '</div>';
                    $('#selectedPractice')
                        .children('.col-90')
                        .append(output);

                    $speakSelected.addClass('isSelected');
                    $selectedLang.addClass('isSelected');

                    languagesPracticed++;
                }
                else {
                    myApp.alert('Sorry, you can only select a maximum of five ' + tempText + ' languages.', 'Max. Number of Languages Reached');
                }
            }

            // Attach event listener to the deselect button.
            $('.deselect-btn')
                .on('click', deselectLanguage);

            // Attach event listener to the proficiency slider.
            $('.proficiency-slider > .range-slider > input')
                .on('input', updateProficiencyLevel);

            // Reset search bar & dropdown.
            if (selectedSpeak) {
                $searchBar = $('#searchInputSpeak');
            }
            else if (selectedPractice) {
                $searchBar = $('#searchInputPractice');
            }
            else {
                console.error('Error resetting search.');
            }
            $searchBar.val('');
            $searchBar.trigger('input');
        }


        /*
         * Deselects a selected language through the 'x' by imitating a deselection through the dropdown menu.
         */
        function deselectLanguage() {
            var $languageDeselected = $(this);
            var languageID = $languageDeselected.closest('.language-selected-item')
                .attr('data-languageID');
            var $selectedWrapper = $languageDeselected.closest('.language-selected-wrapper');
            var speak = $selectedWrapper.is('#selectedSpeak');
            var practice = $selectedWrapper.is('#selectedPractice');

            if (speak) {
                $('#dropdownSpeak')
                    .find('.language-search-dropdown-item.isSelected[data-languageID="' + languageID + '"]')
                    .trigger('click');
            }
            else if (practice) {
                $('#dropdownPractice')
                    .find('.language-search-dropdown-item.isSelected[data-languageID="' + languageID + '"]')
                    .trigger('click');
            }
        }


        /*
         * Filters the languages in the dropdown menu according to user input.
         */
        function filterLanguages(dropdownSection, e) {
            var $dropdownMenu = $('#dropdown' + dropdownSection);
            var $searchBar = $(e.target);
            var numLanguages = $dropdownMenu.children('.row')
                .children('.col-50')
                .filter(function() {
                    return $(this)
                            .css('display') !== 'none';
                }).length;

            $dropdownMenu.children('.row')
                .children('.col-50')
                .each(function() {
                    if ($(this)
                            .children('.language-search-dropdown-item')
                            .attr('data-language')
                            .indexOf($searchBar.val()
                                .toLowerCase()) === -1) {
                        $(this)
                            .hide();
                    }
                    else {
                        $(this)
                            .show();
                    }
                });

            if (numLanguages === 0) {
                $dropdownMenu.find('.error-search')
                    .show();
            }
            else {
                $dropdownMenu.find('.error-search')
                    .hide();
            }

            if ($searchBar.val() === '') {
                $searchBar.siblings('.fa-close')
                    .hide();
                $searchBar.focus();
                $dropdownMenu.show();
            }
            else {
                $searchBar.siblings('.fa-close')
                    .show();
                $searchBar.focus();
                $dropdownMenu.show();
            }
        }


        /*
         * When the user uses the proficiency slider(s), the appropriate proficiency text and colors (through
         * CSS classes) are set.
         */
        function updateProficiencyLevel() {
            var $proficiencyInput = $(this);
            var $proficiencyParent = $proficiencyInput.closest('.proficiency-item');
            // var $languageContainer = $proficiencyInput.closest('.language-selected-item');
            var proficiencyLevel = $proficiencyInput.val();
            var profLevels = ['elementary', 'intermediate', 'advanced', 'fluent', 'native'];
            var c = 0,
                len = profLevels.length;

            for (c < len; c++;) {
                $proficiencyParent.removeClass('-' + profLevels[c]);
            }

            $proficiencyParent.addClass('-' + profLevels[proficiencyLevel - 1]);
            $proficiencyParent.children('.proficiency-text')
                .text(profLevels[proficiencyLevel - 1]);

            $proficiencyInput.attr('value', proficiencyLevel);

        }


        /*
         * Stores the selected spoken & practiced languages into two separate arrays of objects before
         * sending them to the backend via a PUT call.
         */
        function submitLanguages() {
            var options;
            var speakArr = [], practiceArr = [];
            var $selectedSpeak = $('.js-selected-speak');
            var $selectedPractice = $('.js-selected-practice');
            var possibleTransition = false;
            var weakLangs = [];
            var powerLangs = [];
            var speakObj = {
                'languages': speakArr, 'type': 2
            };
            var practiceObj = {
                'languages': practiceArr, 'type': 1
            };
            var powerLangNames = [];
            var weakLangNames = [];
            var i, j;

            storeArr(speakArr, $selectedSpeak);
            storeArr(practiceArr, $selectedPractice);

            //Find Languages that are weak
            speakArr.forEach(function(speakingLang, index) {
                if (speakingLang.proficiency < 3) {
                    possibleTransition = true;
                    weakLangs.push(speakingLang);
                }
            });

            //Find strong languages
            practiceArr.forEach(function(practiceLang, index) {
                if (practiceLang.proficiency > 3) {
                    possibleTransition = true;
                    powerLangs.push(practiceLang);
                }
            });

            function submitToApi(speakObj, practiceObj) {

                makeApiRequest('users/me/languages', speakObj, 'PUT', true)
                    .done(function() {
                        makeApiRequest('users/me/languages', practiceObj, 'PUT', true)
                            .done(function() {
                                if (page.query.edit) {
                                    mainView.router.loadPage('my-profile.html');
                                }
                                else if (page.query.edit == '2') {
                                    mainView.router.loadPage('chats.html');
                                }
                                else {
                                    mainView.router.loadPage('interests.html');
                                }
                            })
                            .fail(function() {
                                myApp.alert('Sorry, you can only select a maximum of five learning languages.', 'Max. Number of Languages Reached');
                            });
                    })
                    .fail(function() {
                        myApp.alert('Sorry, you can only select a maximum of five speaking languages.', 'Max. Number of Languages Reached');
                    });
            }

            // remove language by id
            function removeById(arr, value) {
                i = arr.length;
                while (i--) {
                    if (arr[i] && arr[i].id == value) {
                        arr.splice(i, 1);
                    }
                }
                return arr;
            }

            function optionCallback(modal, index) {
                var found = 0;
                if (practiceObj.languages.length + weakLangs.length > 5 || speakObj.languages.length + powerLangs.length > 5) {
                    index = 1;
                }
                switch (index) {
                    case 1:
                        submitToApi(speakObj, practiceObj);
                        break;
                    case 2:
                        //Find already present language on other side
                        for (i = 0; i < weakLangs.length; i++) {
                            found = 0;
                            for (j = 0; j < practiceObj.languages.length; j++) {
                                if (weakLangs[i].id == practiceObj.languages[j].id) {
                                    found = 1;
                                    break;
                                }
                            }
                            if (!found) {
                                practiceObj.languages.push(weakLangs[i]);
                                speakObj.languages = removeById(speakObj.languages,
                                    weakLangs[i].id);
                            }
                        }

                        for (i = 0; i < powerLangs.length; i++) {
                            found = 0;
                            for (j = 0; j < speakObj.languages.length; j++) {
                                if (powerLangs[i].id == speakObj.languages[j].id) {
                                    found = 1;
                                    break;
                                }
                            }
                            if (!found) {
                                speakObj.languages.push(powerLangs[i]);
                                practiceObj.languages = removeById(practiceObj.languages, powerLangs[i].id);
                            }
                        }
                        submitToApi(speakObj, practiceObj);
                        break;
                }
                return false;
            }

            //Some languages didn't match their category proficiency
            if (possibleTransition) {
                weakLangs.forEach(function(weakLang, index) {
                    weakLangNames.push($('div[data-languageid=' + weakLang.id + ']')
                        .attr('data-language'));
                });

                powerLangs.forEach(function(powerLang, index) {
                    powerLangNames.push($('div[data-languageid=' + powerLang.id + ']')
                        .attr('data-language'));
                });

                options = {
                    title: 'Should we Switch?',
                    text: (weakLangNames.length > 0 ? '<p class="stillLearningMessage">It seems you are still learning these languages: '
                        + weakLangNames.join(', ')
                        .toUpperCase() +
                        '!  We will add them to the languages you are learning!</p>' + (powerLangNames.length > 0 ?
                        '<p class="alreadyGoodMessage">It seems you are already good at these languages: '
                        + powerLangNames.join(', ')
                            .toUpperCase() +
                        '!  We will add them to the languages you speak!</p>' : '') :
                        'It seems you are good at these languages: '
                        + powerLangNames.join(', ')
                            .toUpperCase() +
                        '! We will add them to the languages you speak!'),
                    buttons: [
                        {
                            text: 'Cancel'
                        },
                        {
                            text: 'No'
                        },
                        {
                            text: 'Yes'
                        }
                    ],
                    onClick: optionCallback
                };
                //Generate a confirm message for language switching
                myApp.modal(options, optionCallback);
            }

            else {
                submitToApi(speakObj, practiceObj);
            }
            return false;
        }

        /*
         * Pulls the language data from the DOM and pushes it into the passed array.
         */
        function storeArr(arr, elements) {
            var lang = {};
            var $element;
            var c, len;
            for (c = 0, len = elements.length; c < len; c++) {
                $element = $(elements[c]);
                lang.id = Number($element.attr('data-languageID'));
                lang.proficiency = Number($element.find('.proficiency-slider')
                    .children('.range-slider')
                    .children('input')
                    .val());
                arr.push({
                    'id': lang.id, 'proficiency': lang.proficiency
                });
            }
        }
    });

}(jQuery, document, window));
