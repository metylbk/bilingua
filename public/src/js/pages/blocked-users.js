(function($, document, window) {

    'use strict';

    // Now we need to run the code that will be executed only for Feature page.
    myApp.onPageInit('blocked-users', function(page) {
        var makeApiRequest = window.makeApiRequest;

        // show loader
        myApp.showIndicator();

        makeApiRequest('users/blocked', null, 'GET', true).done(blockedUsersInit);

        bindEvents();

        function blockedUsersInit(response) {
            var blockedUsersHTML = '';
            var blockedUsers = response;

            $.each(blockedUsers, function(i, obj) {
                blockedUsersHTML += '<li class="item-content"><div class="item-media blocked-users-list__media"><img class="blocked-users-list__avatar" src=' + obj.avatar + ' width="100%"></div>';
                blockedUsersHTML += '<div class="item-inner"><div class="item-title-row"><h3 class="item-title blocked-users-list__title" id="user-name">' + obj.first_name + ' ' + obj.family_name + '</h3>';
                blockedUsersHTML += '<p class="buttons-row"><a href="#" data-userID="' + obj.id + '" data-userName="' + obj.first_name + ' ' + obj.family_name + '" class="button button-fill button-raised">unblock</a></div></div></li>';
            });

            $('#blockedUsers').append(blockedUsersHTML);

            // hide loader
            myApp.hideIndicator();
        }


        function bindEvents() {
            $('.blocked-users-list').on('mouseover', '.button', function() {
                $(this).removeClass('button-fill');
            }).on('mouseout', '.button', function() {
                $(this).addClass('button-fill');
            }).on('click', '.button', function() {
                var userName = $(this).attr('data-userName');
                var userID = $(this).attr('data-userID');
                myApp.confirm('', 'Are you sure you want unblock ' + userName + '?', function() {
                    makeApiRequest('users/' + userID + '/unblock', null, 'POST', true);
                    mainView.router.refreshPage();
                });
            });
        }

        // Feedback
        window.feedback.showPopup();

        /**
         * Test block user
         */
        // blockUser('4');
        //
        // function blockUser(id) {
        //     makeApiRequest('users/' + id + '/block', null, 'POST', true);
        // }

    });

}(jQuery, document, window));
