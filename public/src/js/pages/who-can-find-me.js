/**
 * Created by Bianca on 02/06/16.
 */
(function($, document, window) {

    'use strict';

    // Now we need to run the code that will be executed only for Feature page.
    myApp.onPageInit('who-can-find-me', function(page) {
        // Do something here for 'feature' page


        var slider = document.getElementById('slider');

        noUiSlider.create(slider, {
            start: [13, 80],
            connect: true,
            step: 1,
            range: {
                'min': 13,
                'max': 80
            }
        });


        /**
         * Extra div for increasing the clickable area for the age slider
         */
        $('<div class="clickable-area"></div>').appendTo('.noUi-base');


        slider.noUiSlider.on('update', function(values) {
            $('.rangeValues').text(parseInt(values[0], 10) + ' - ' + parseInt(values[1], 10));
        });


        /**
         * Method for making ajax calls to Bilingua's API.
         * @param {string} endpoint - the api endpoint
         * @param {object} [data] - key:value pairs of the data to to sent to server
         * @param {string} [type] - the type of ajax request to make
         * @param {boolean} [authNeeded] - determines if the login token is needed
         * @returns {Object} jQuery ajax promise object
         */


        // function makeApiRequest(endpoint, data, type, authNeeded) {
        //     return $.ajax({
        //         url: 'https://staging.bilingua.io/api/' + endpoint,
        //         type: type || 'GET',
        //         data: JSON.stringify(data) || null,
        //         dataType: 'json',
        //         headers: authNeeded ? {'Content-Type': 'application/json', 'Accept': 'application/vnd.bilingua.v1+json', 'Authorization': 'Bearer ' + localStorage.loginToken} : {'Content-Type': 'application/json', 'Accept': 'application/vnd.bilingua.v1+json'}
        //     });
        // }

        window.makeApiRequest = makeApiRequest;

        // Below to be replaced once login is integrated & api.js calls auth.js instead.
        // doLogin();
        // function doLogin() {
        //     var login = {'email': 'email@example.com', 'password': '123456'};
        //     var token;
        //
        //     makeApiRequest('auth/login', login, 'POST', false).done(function(data) {
        //         localStorage.loginToken = data.token;
        //     });
        // }

        function check() {


            makeApiRequest('users/me/settings', null, 'GET', true).done(function(res) {

                if (res.visibility_same_gender) {
                    $('#genders').prop('checked', true);
                }

                if (res.visibility_speaking_native_language_match) {
                    $('#learning').prop('checked', true);
                }

                if (res.visibility_learning_native_language_match) {
                    $('#speaking').prop('checked', true);
                }

                if (res.visibility_location_match) {
                    $('#country').prop('checked', true);
                }

            });
        }

        check();

        window.feedback.showPopup();

        makeApiRequest('users/me/settings', null, 'GET', true).done(function(res) {

            slider.noUiSlider.set([res.visibility_min_age, res.visibility_max_age]);
            $('.rangeValues').text(res.visibility_min_age + ' - ' + res.visibility_max_age);

        });


        /**
         * send changes to DB
         */

        $('#applyChanges').on('click', function() {

            var values = slider.noUiSlider.get();
            var minValue = parseInt(values[0], 10);
            var maxValue = parseInt(values[1], 10);
            var changes = {
                'visibility_min_age': minValue,
                'visibility_max_age': maxValue,
                'visibility_same_gender': $('#genders').is(':checked'),
                'visibility_speaking_native_language_match': $('#learning').is(':checked'),
                'visibility_learning_native_language_match': $('#speaking').is(':checked'),
                'visibility_location_match': $('#country').is(':checked')

            };

            makeApiRequest('users/me/settings', changes, 'PATCH', true).done(function(res) {


                myApp.alert('Changes Saved', '');
                mainView.router.loadPage('my-profile.html');


            });


        });
    });


}(jQuery, document, window));
