(function($, document, window) {

    'use strict';
    // Now we need to run the code that will be executed only for Feature page.
    myApp.onPageInit('my-settings', function(page) {

        var makeApiRequest = window.makeApiRequest;

        // Feedback
        window.feedback.showPopup();

        // remove 'readonly' attribute
        $('.item-content').on('click', '.edit-form', function() {
            $(this).siblings().children('input')
                .removeAttr('readonly')
                .select()
                .css('color', 'black');
        });

        // save button notification
        $('.notification-single').on('click', function() {
            myApp.addNotification({
                hold: 5000,
                message: 'New password saved!'
            });
        });

        // get user info
        makeApiRequest('users/me', null, 'GET', true).done(function(userArr) {
            var user = userArr;
            $('#email').text(user.email);
            if (user.avatar) {
                $('#user-avatar')
                    .attr('src', user.avatar);
            }
        });


        // submit password change
        $('#my-form').submit(function(ev) {
            var password = $('#password').val().trim(),
                data = {};
            ev.preventDefault();


            //This condition will only be true if the value is not an empty string
            if (password) {
                data = { email: $('#email').text(), password: password };
            }
            else {
                data = { email: $('#email').text() };
            }

            makeApiRequest('users/me', data, 'PATCH', true);
        });


        // confirm account delete
        $('.confirm-ok').on('click', function() {
            myApp.confirm('', 'Are you sure?', function() {

                makeApiRequest('users/me', null, 'DELETE', true).done(function(res) {

                    myApp.alert('Account deleted', '');
                    mainView.router.loadPage('intro.html');

                });
            });
        });


        // logout
        $('.logout').on('click', function() {
            Auth.logout();
            SocialAuth.logout();
            mainView.router.loadPage('login.html');
        });

        /**
         * back button
         */
        // $('.back').on('click','a',function(e){
        //     var view=myApp.getCurrentView();
        //     var len=view.history.length;
        //     if(len>1){
        //         view.router.back({
        //             url:view.history[0],
        //             force:true
        //         });
        //     }
        // });


    });
}(jQuery, document, window));
