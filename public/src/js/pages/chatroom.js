(function($, document, window) {

    'use strict';

    myApp.onPageAfterAnimation('chatroom', function(page) {
        var id,
            user,
            friendId = 0,
            week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            textarea = $('textarea'),
            messagetoSay,
            myMessagebar,
            unreadMessageCount;
        myApp.showIndicator();

        unreadMessageCount = JSON.parse(localStorage.unreadMessages);

        id = mainView.activePage.query.id.slice(5);
        window.fromChat = true;

        window.renderAgain = function() {
            retrieveMessages();
        };
        startRendering();

        //header options
        $('.header__dots')
            .on('click', function(e) {
                e.stopPropagation();
                $(this)
                    .parent()
                    .addClass('isActive');
            });
        $('.header__options')
            .on('click', 'a', function() {
                $(this)
                    .closest('.header__options')
                    .removeClass('isActive');
            });
        $('.chat-screen, .options-toolbar')
            .on('click', function(e) {
                if ($(e.target)
                        .closest('.header__options').length === 0) {
                    $('.header__options')
                        .removeClass('isActive');
                }
            });
        $('#chating-user')
            .html(mainView.activePage.query.name);
        $('.chat-user__image')
            .attr('src', mainView.activePage.query.image);
        $('.js-user-box-name').html(mainView.activePage.query.name);

        //User profile on clicking image or user name
        $('.chat-user')
            .on('click', function() {
                mainView.router.load({
                    url: 'user-profile.html',
                    query: {
                        id: mainView.activePage.query.fromPartner ? 'part-' + friendId : 'part-' + mainView.activePage.query.userid
                    }
                });
            });
        // Handle message
        $('.messagebar .link')
            .on('click', function() {

                var time = new Date(), messageHTML;
                // Exit if empty message
                var message = myMessagebar.value()
                    .trim();

                if (message == '') {
                    return;
                }
                // Add message to chat as sending
                messageHTML = '<div class="message message-sent message-sending">'
                    + '<div class="bubble-options">'
                    + '<a href="#" class="chat__send-dots">'
                    + '<i class="fa fa-ellipsis-v fa-lg" aria-hidden="true"></i>'
                    + '</a>'
                    + '<ul class="dropdown-sent">'
                    + '<li><a href="#" class="">Copy</a></li>'
                    + '<li><a href="#" class="">Delete</a></li>'
                    + '<li><a href="#" class="">Edit</a></li>'
                    + '<li><a href="#" class="read-text">Listen</a></li>'
                    + '</ul>'
                    + '</div>'
                    + '<div class="message-text chat__text -sent">' + message + '<span class="chat__hour">' + time.getHours() + ':' + time.getMinutes() + '</span></div>'
                    + '</div>';

                $('.chat')
                    .append(messageHTML);

                if (message.length === 0) {
                    return;
                }

                renderEmojis();

                function sendMessage(message) {
                    makeApiRequest('messages/' + id, {
                        message: message
                    }, 'PUT', true)
                        //Message sent
                        .success(function(data) {
                            $('.message-sending')
                                .removeClass('.message-sending');
                            retrieveMessages();
                        })
                        //Connection Problem
                        .error(function(data) {
                            $('.message-sending')
                                .replaceClass('.message-sending', '.message-error');
                            sendMessage(message);
                        });
                }

                sendMessage(message);

                // Empty messagebar
                myMessagebar.clear();
                $('.message-bar__send')
                    .removeClass('active');

            });

        textarea
            .emojiarea({
                button: '.message-bar__smiley'
            });

        textarea
            .on('focus', function() {
                scrollUpdate('read');
            });

        //learn more popup

        $('.learn')
            .on('click', function() {
                var popupHTML = '<div class="popup learn-popup">' +
                    '<div class="content-block">' +
                    '<ol class="how-to">' +
                    '<li><a href="#">Send messages in the languages they are learning.</a></li>' +
                    '<li><a href="#">Encourage partners to write in languages they are learning</a></li>' +
                    '<li><a href="#">Correct partners writing mistakes by long hold the bubble and using <i>correct</i> function</a></li>' +
                    '</ol>' +
                    '<a href="#" class="close-popup cancel">cancel</a>' +
                    '</div>' +
                    '</div>';
                myApp.popup(popupHTML);
            });


        function renderEmojis() {
            emojify.run();
            //remove any previous logic
            $('.read-text').off('click');

            // Register the click to the read buttons

            $('.read-text').on('click',
                function(e) {
                    var currentElement = e.toElement || e.target;
                    e.preventDefault();

                    messagetoSay = currentElement.parentNode.parentNode.parentNode.nextSibling.innerHTML.split('<span')[0];

                    TTS.textToSpeech(messagetoSay);

                }
            );
        }

        //send icon
        function sendIcon() {
            $('.message-bar')
                .on('keyup', function() {

                    if (event.keyCode === 13) {
                        $('.message-bar .link')
                            .click();
                    }
                    if (myMessagebar.value()
                            .trim().length === 0) {
                        $('.message-bar__send')
                            .removeClass('active');
                    }
                    else {
                        $('.message-bar__send')
                            .addClass('active');
                    }
                });
        }


        sendIcon();

        // Init Messages
        myApp.messages('.messages', {
            autoLayout: true
        });

        // Init Messagebar
        myMessagebar = myApp.messagebar('.messagebar');

        //Render the chat
        function renderer(userId) {

            nullUnreadMessages(id);

            // Echo.join('chat.thread.' + id)
            //     .here(function (data) {
            //         console.log(data);
            //     })
            //     .joining(function (user) {
            //         console.log(user);
            //     })
            //     .leaving(function (user) {
            //         console.log(user);
            //     })
            //     .listen('NewThreadMessageEvent', function (e) {
            //             retrieveMessages('newThread');
            //         }
            //     );

            retrieveMessages();
        }

        function retrieveMessages() {
            var totalReceivedMessages,
                unreadMessageHTML = '';
            makeApiRequest('messages/' + id, null, 'GET', true).success(function(messages) {
                renderMessages(messages.messages, user);
                totalReceivedMessages = $('.message-received').length;
                if (unreadMessageCount[id] === 0) {
                    scrollUpdate('read');
                }
                else {
                    unreadMessageHTML += '<div id="message-unread-status" class="unread-message">' +
                        '<span class="new-msg-info">' + unreadMessageCount[id] + ' new messages</span>' +
                        '<a class="clear-unread-messages">' +
                            '<span class="clear-unread-messages-text">Mark as read</span>' +
                        '</a>' +
                    '</div>';
                    $($('div.messages-auto-layout div.message-received')[totalReceivedMessages - unreadMessageCount[id]]).before($(unreadMessageHTML));
                    scrollUpdate('unread');
                }
                setTimeout(function() {
                    $('#message-unread-status').remove();
                }, 5000);
            })
            //Wait for last request before making new one
            .error(function(data) {
                if (Auth.isLoggedIn()) {
                    retrieveMessages();
                }
            });
            myApp.hideIndicator();
        }

        function startRendering() {
            //Get User Data
            makeApiRequest('users/me', null, 'GET', true)
                .success(function(data) {
                    user = data;
                    renderer(user.id);
                    // Feedback
                    window.feedback.showPopup();
                });
        }
        //Render the messages
        function renderMessages(messages, user) {
            var curDay = new Date(),
                html = '',
                newDate;

            messages.forEach(function(message, index) {
                newDate = new Date(message.created_at.replace(/-/g, '/'));
                newDate = new Date(newDate.valueOf() - newDate.getTimezoneOffset() * 60000 - 28800000);
                //If a new day is beginning add a date separator
                if (curDay - newDate >= 86400000) {
                    html += '<div class="messages-date">' + week[curDay.getDay()] + ', ' + months[curDay.getMonth()] + ' ' + curDay.getDate() + '</div>';
                    curDay = newDate;
                }
                if (message.user_id == user.id) {
                    //It is the user
                    html += '<div class="message message-sent noselect">'
                        + '<div class="bubble-options">'
                        + '<a href="#" class="chat__send-dots">'
                        + '<i class="fa fa-ellipsis-v fa-lg" aria-hidden="true"></i>'
                        + '</a>'
                        + '<ul class="dropdown-sent">'
                        + '<li><a href="#" class="">Copy</a></li>'
                        + '<li><a href="#" class="">Delete</a></li>'
                        + '<li><a href="#" class="">Edit</a></li>'
                        + '<li><a href="#" class="read-text">Listen</a></li>'
                        + '</ul>'
                        + '</div>'
                        + '<div class="message-text chat__text -sent">' + message.body
                        + '<span class="chat__hour">' + newDate.getHours() + ':' + newDate.getMinutes() + '</span>'
                        + '</div>'
                        + '</div>';
                }
                //It is not the user
                else {
                    html += '<div class="message message-received noselect">'
                        + '<div class="bubble-options">'
                        + '<a href="#" class="chat__received-dots">'
                        + '<i class="fa fa-ellipsis-v fa-lg" aria-hidden="true"></i>'
                        + '</a>'
                        + '<ul class="dropdown-received">'
                        + '<li><a href="#" class="">Copy</a></li>'
                        + '<li><a href="#" class="">Translate</a></li>'
                        + '<li><a href="#"  class="read-text">Listen</a></li>'
                        + '</ul>'
                        + '</div>'
                        + '<div class="message-text chat__text -received">' + message.body
                        + '<span class="chat__hour">' + newDate.getHours() + ':' + newDate.getMinutes() + '</span>'
                        + '</div>'
                        + '</div>';
                }
            });
            //scroll to bottom and add the inner html code
            $('.chat')
                .html(html);

            renderEmojis();
        }

        //Calculate age
        function calculateAge(birthday) {
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs);
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        // Pull data for user-box
        makeApiRequest('users/' + mainView.activePage.query.userid, null, 'GET', true).done(function(userArr) {
            var user = userArr;
            var age = calculateAge(new Date(user.birthday));
            var interestsHTML = '';

            $('.js-location').text(user.country.name);
            $('.js-age').text(age + ' yo, ');
            $('.js-gender').html(getGender(user.gender) + ', ');
            $('.js-user-flag').addClass('flag ' + user.country.iso_3166_2.toLowerCase());

            if (user.avatar) {
                $('.user-box-avatar').attr('src', user.avatar);
            }

            if (user.cover_image) {
                $('.user-box-banner').attr('src', user.cover_image);
            }

            $.each(user.interests, function(i, obj) {
                interestsHTML += '<div class="chip custom-chip" data-languageid=' + obj.id + '>';
                interestsHTML += '<div class="chip-label">' + obj.name + '</div>';
                interestsHTML += '</div>';
            });
            $('.user-interests__chips').html(interestsHTML);

            // languages & proficiency
            function myLanguages(speakingLearning) {

                // initiate variables
                var myLanguagesHTML = '';
                var langAPI = '';

                // determine which API object to use, speaking or learning
                if (speakingLearning == 'speaking') {
                    langAPI = user.spoken_languages;
                }
                else if (speakingLearning == 'learning') {
                    langAPI = user.learning_languages;
                }


                // function to return proficiency class
                function getProfClass(proficiency) {
                    if (proficiency == '1') {
                        return 'elementary';
                    }
                    else if (proficiency == '2') {
                        return 'intermediate';
                    }
                    else if (proficiency == '3') {
                        return 'advanced';
                    }
                    else if (proficiency == '4') {
                        return 'fluent';
                    }
                    else if (proficiency == '5') {
                        return 'native';
                    }
                }

                // loop through each language array
                $.each(langAPI, function(i, obj) {

                    var proficiency = obj.pivot.proficiency;
                    var profClass = getProfClass(proficiency);

                    myLanguagesHTML += '<div class="languages__column">';
                    myLanguagesHTML += '<div class="language-item">';
                    myLanguagesHTML += '<div class="languages__flag-container">';
                    myLanguagesHTML += '<div class="set-size charts-container">';
                    myLanguagesHTML += '<div class="pie-wrapper progress-' + profClass + '">';
                    myLanguagesHTML += '<div class="pie">';
                    myLanguagesHTML += '<div class="left-side half-circle"></div>';
                    myLanguagesHTML += '<div class="right-side half-circle"></div>';
                    myLanguagesHTML += '</div>';
                    myLanguagesHTML += '</div>';
                    myLanguagesHTML += '</div>';
                    myLanguagesHTML += '<img src="' + obj.flag + '">';
                    myLanguagesHTML += '</div>';
                    myLanguagesHTML += '</div>';
                    myLanguagesHTML += '</div>';

                });

                $('.js-' + speakingLearning + '-language-container > .rows').html(myLanguagesHTML);
            }

            // initiate functions for speaking and learning languages
            myLanguages('speaking');
            myLanguages('learning');

            window.Interests.linkChips();
        });

        window.messageDots();

    });

}(jQuery, document, window));
