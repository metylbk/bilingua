(function($, document, window) {

    'use strict';

    // Now we need to run the code that will be executed only for Feature page.
    myApp.onPageInit('about', function(page) {

        // Feedback
        window.feedback.showPopup();

    });

}(jQuery, document, window));
