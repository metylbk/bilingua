(function($, document, window) {

    'use strict';

    myApp.onPageInit('filter-users', function(page) {
        var makeApiRequest = window.makeApiRequest;
        var slider = document.getElementById('slider');

        myApp.showIndicator();

        makeApiRequest('users/me', null, 'GET', true)
            .done(interestsInit);
        makeApiRequest('countries', null, 'GET', true)
            .done(countryDropdownInit);
        makeApiRequest('languages', null, 'GET', true)
            .done(languageDropdownInit);


        // $('.range-slider.-distance > input').on('input', getDistVal);
        // $('.range-slider.-distance > input').trigger('input');

        $('.proficiency-slider > .range-slider > input')
            .on('input', updateProficiencyLevel);

        $('#applyFilter')
            .on('click', storeFilterParams);


        /**
         * Initialises the interests section with the user's interests.
         */
        function interestsInit(res) {
            res = res.interests;

            res.forEach(function(interestItem) {
                var html = '';

                html += '<div class="chip custom-chip interest" data-interestID="' + interestItem.id + '">' +
                    '<div class="chip-media"><i class="fa fa-plus"></i></div>' +
                    '<div class="chip-label">' + interestItem.name + '</div>' +
                    '</div>';

                $('.interests .content')
                    .append(html);
            });

            // Attach 'click' event listener to interest items.
            $('.interest')
                .on('click', selectInterest);
        }

        // Feedback
        window.feedback.showPopup();


        /**
         * Selects the interest and adds/removes the appropriate CSS classes.
         */
        function selectInterest() {
            var $this = $(this);
            $this.toggleClass('bg-blue');

            if ($this.hasClass('bg-blue')) {
                $this.find('.fa')
                    .removeClass('fa-plus')
                    .addClass('fa-check');
            }
            else {
                $this.find('.fa')
                    .removeClass('fa-check')
                    .addClass('fa-plus');
            }
        }


        /**
         * Age slider
         */

        noUiSlider.create(slider, {
            start: [13, 80],
            connect: true,
            step: 1,
            range: {
                'min': 13,
                'max': 80
            }
        });

        slider.noUiSlider.on('update', function(values) {
            $('.rangeValues')
                .text(parseInt(values[0], 10) + ' - ' + parseInt(values[1], 10));
        });


        /**
         * Extra div for increasing the clickable area for the age slider
         */
        $('<div class="clickable-area"></div>')
            .appendTo('.noUi-base');


        /**
         * Initialises the autocomplete dropdown for countries.
         */
        function countryDropdownInit(response) {
            var countries = response;

            myApp.autocomplete({
                input: '#autocomplete_country',
                openIn: 'dropdown',
                source: function(autocomplete, query, render) {
                    var results = [];
                    if (query.length === 0) {
                        render(results);
                        return;
                    }

                    countries.forEach(function(country, index) {
                        if (country.name.toLowerCase()
                                .indexOf(query.toLowerCase()) === 0) {
                            results.push(country.name);
                        }
                    });

                    render(results);
                }
            });
        }


        /**
         * Initialises the autocomplete dropdown for language names.
         */
        function languageDropdownInit(response) {
            var languages = response;

            myApp.autocomplete({
                input: '#autocomplete_language',
                openIn: 'dropdown',
                source: function(autocomplete, query, render) {
                    var results = [];
                    if (query.length === 0) {
                        render(results);
                        return;
                    }

                    languages.forEach(function(language) {
                        if (language['english_name'].toLowerCase()
                                .indexOf(query.toLowerCase()) === 0) {
                            results.push(language['english_name']);
                        }
                    });

                    render(results);
                }
            });

            languages.forEach(function(language) {
                language['english_name'] = toTitleCase(language['english_name']);
            });

            myApp.hideIndicator();
        }

        /*
         * Converts the lowercase language name to title case.
         */
        function toTitleCase(str) {
            return str.replace(/\w\S*/g, function(txt) {
                return txt.charAt(0)
                        .toUpperCase() + txt.substr(1)
                        .toLowerCase();
            });
        }


        /*
         * When the user uses the proficiency slider, the appropriate proficiency text and colors (through
         * CSS classes) are set.
         */
        function updateProficiencyLevel() {
            var $proficiencyInput = $(this);
            var $proficiencyParent = $proficiencyInput.closest('.proficiency-item');
            var proficiencyLevel = $proficiencyInput.val();
            var profLevels = ['elementary', 'intermediate', 'advanced', 'fluent', 'native'];

            profLevels.forEach(function(profLevel, index) {
                $proficiencyParent.removeClass('-' + profLevel);
            });

            $proficiencyParent.addClass('-' + profLevels[proficiencyLevel - 1]);
            $proficiencyParent.children('.proficiency-text')
                .text(profLevels[proficiencyLevel - 1]);

            $proficiencyInput.attr('value', proficiencyLevel);
        }


        function storeFilterParams() {
            var interests = [];
            var country;
            var language = {};
            var values = slider.noUiSlider.get();
            var minValue = parseInt(values[0], 10);
            var maxValue = parseInt(values[1], 10);
            var filterParams;

            $('.fa-check')
                .each(function() {
                    interests.push($(this)
                        .closest('.interest')
                        .attr('data-interestID'));
                });

            country = $('input[name="country"]')
                .val();

            if ($('input[name="language"]').val()) {
                language.name = $('input[name="language"]')
                    .val();
                language.proficiency = $('.proficiency-slider > .range-slider > input')
                    .val();
            }

            filterParams = {
                'interests': interests,
                'minAge': minValue,
                'maxAge': maxValue,
                'country': country ? country : '',
                'language': language.name ? language : undefined
            };
            window.filterParams = filterParams;
        }
    });

}(jQuery, document, window));
