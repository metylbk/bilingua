(function($, document, window) {

    myApp.onPageAfterAnimation('chats', function(page) {
        var showThreads = 20,
            threads = [];
        // Loading flag
        var loading = false;

        // Last loaded index
        var lastIndex = $('.list-block li').length;

        // Append items per load
        var itemsPerLoad = 20;
        window.fromChat = false;
        window.renderAgain = function() {
            renderer();
        };
        if (myApp.support.touch) {
            $('html')
                .addClass('touchevents');
        }
        if (!myApp.support.touch) {
            myApp.destroyPullToRefresh('.pull-to-refresh-content');
            $('.pull-to-refresh-layer')
                .hide();
            $('.page-content')
                .removeClass('pull-to-refresh-content');
        }
        myApp.showIndicator();
        makeApiRequest('users/me', null, 'GET', true)
            .success(function(data) {
                var user = data;
                window.authUser = data;

                Raven.setUserContext({
                    email: user.email,
                    id: user.id
                });

                if (user.avatar) {
                    $('#user-avatar, #user-dropdown-avatar')
                        .attr('src', user.avatar);
                }

                if (window.initialLoad) {
                    listenNotifications(window.authUser.id);
                }

                // $('#user-name')
                //     .text(user.first_name + ' ' + user.family_name);
                if (user.avatar) {
                    $('#user-avatar, #user-dropdown-avatar')
                        .attr('src', user.avatar);
                }

                //Check everthing important
                if (user.first_name == null || user.family_name == null || user.avatar == null ||
                    user.country_id == null || user.gender == null ||
                    user.birthday == null) {
                    mainView.router.loadPage('profile-filling.html?edit=2');
                }

                else if (user.learning_languages.length == 0 || user.spoken_languages.length == 0) {
                    mainView.router.loadPage('language-selection.html?edit=2');
                }

                else if (user.interests.length == 0) {
                    mainView.router.loadPage('interests.html?edit=2');
                }
                renderer();
            });

        //Loads messages and initiates rendering
        function renderer() {
            makeApiRequest('messages', null, 'GET', true)
                .success(function(data) {
                    myApp.hideIndicator();

                    threads = data;

                    threads = threads.filter(function(thread) {
                        if (thread.users.length === 2 && thread.users[1] !== null) {
                            return true;
                        }
                        return false;
                    });

                    threads = threads.sort(function(prev, next) {
                        if (new Date(next.updated_at) - new Date(prev.updated_at) < 0) {
                            return -1;
                        }
                        else if (new Date(next.updated_at) - new Date(prev.updated_at) == 0) {
                            return 0;
                        }
                        return 1;
                    });

                    if (window.initialLoad) {
                        threads.forEach(function(thread, index) {
                            nullUnreadMessages(thread.id);
                        });
                        window.initialLoad = false;
                    }

                    renderList(threads, showThreads);

                });
        }


        //header-links
        function headerLinks() {
            $('.conversation-list')
                .on('click', '.conversation-item', function(e) {
                    var curHolder = e.currentTarget;
                    if ($(e.target)
                            .closest('.options-dots').length === 0 && $(e.target)
                            .closest('.js-item-options').length === 0 && !$('.conversation-list')
                            .hasClass('disable-redirect')) {
                        mainView.router.load({
                            url: 'chatroom.html',
                            query: {
                                id: curHolder.id,
                                userid: curHolder.getAttribute('data-userid'),
                                name: $(e.target)
                                    .closest('.item-content')
                                    .find('.item-title')
                                    .text(),
                                image: $(e.target)
                                    .closest('.item-content')
                                    .find('.item-media')
                                    .find('img')
                                    .attr('src'),
                                fromPartner: false
                            }
                        });
                    }
                });
        }

        // Feedback
        window.feedback.showPopup();

        // Filter Conversations
        window.filterConvos.run();

        // Block/Delete User
        window.blockDeleteUser();

        // Get Last Login time
        function getTimeDiff(time) {
            var curDate,
                given,
                diff,
                min,
                hours;

            time = time.replace(/-/g, '/');
            curDate = new Date();
            curDate = new Date(curDate.valueOf() + curDate.getTimezoneOffset() * 60000 + 28800000);
            given = new Date(time);
            diff = curDate - given;
            min = Math.floor((diff / 1000) / 60);
            if (min === 0) {
                return 'Just Now';
            }
            if (min >= 60) {
                hours = Math.floor(min / 60);
                if (hours >= 48) {
                    return Math.round(hours / 24) + ' days';
                }
                else if (hours === 1) {
                    return hours + ' Hour';
                }
                else if (hours >= 24 && hours < 48) {
                    return Math.round(hours / 24) + ' day';
                }
                else {
                    return hours + ' Hours';
                }
            }
            else {
                return min + ' min';
            }
        }

        //Filter the conversations
        function filterFalse(thread) {
            var i, j;
            var nameFilter;
            var found;
            if (window.convFilters === undefined ||
                (window.convFilters.name == '' && window.convFilters.interests[0] == ''
                && window.convFilters.languages[0] == '')) {
                window.convFilters = undefined;
                return true;
            }

            nameFilter = new RegExp(window.convFilters.name.toLowerCase(), 'g');
            if (!nameFilter.test((thread.users[1].first_name + ' ' + thread.users[1].family_name).toLowerCase())) {
                return false;
            }

            found = false;

            if (window.convFilters.languages[0] != '') {
                for (i = 0; i < thread.users[1].spoken_languages.length; i++) {
                    for (j = 0; j < window.convFilters.languages.length; j++) {
                        if (thread.users[1].spoken_languages[i].english_name == window.convFilters.languages[j].toLowerCase()) {
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        break;
                    }
                }

                for (i = 0; i < thread.users[1].learning_languages.length; i++) {
                    for (j = 0; j < window.convFilters.languages.length; j++) {
                        if (thread.users[1].learning_languages[i].english_name == window.convFilters.languages[j].toLowerCase()) {
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        break;
                    }
                }

                if (!found) {
                    return false;
                }
            }

            if (window.convFilters.interests[0] != '') {
                for (i = 0; i < thread.users[1].interests.length; i++) {
                    for (j = 0; j < window.convFilters.interests.length; j++) {
                        if (thread.users[1].interests[i].name.toLowerCase() == window.convFilters.interests[j].toLowerCase()) {
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        break;
                    }
                }
                if (!found) {
                    return false;
                }
            }

            return true;

        }


        function renderLanguageList(languageList) {
            var profStr = '';
            var html = '';
            var c, len;

            for (c = 0, len = languageList.length; c < len; c++) {
                switch (languageList[c].pivot.proficiency) {
                    case 1:
                        profStr = 'elementary';
                        break;
                    case 2:
                        profStr = 'intermediate';
                        break;
                    case 3:
                        profStr = 'advanced';
                        break;
                    case 4:
                        profStr = 'fluent';
                        break;
                    case 5:
                        profStr = 'native';
                        break;
                }

                html += '<div class="noselect language-flag-container -' + profStr + '" data-langID="' + languageList[c].id + '">' +
                    '<div class="set-size charts-container">' +
                    '<div class="pie-wrapper progress-' + profStr + '">' +
                    '<div class="pie">' +
                    '<div class="left-side half-circle"></div>' +
                    '<div class="right-side half-circle"></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<img src="' + languageList[c].flag + '">' +
                    '</div>';
            }

            return html;
        }

        function renderInterestsList(interests) {
            var html = '<p class="item-text user-item__interests noselect">';
            var i, len;
            for (i = 0, len = interests.length; i < len; i++) {
                html += interests[i].name.toLowerCase();

                if (i !== (len - 1)) {
                    html += ', ';
                }
            }
            html += '</p>';
            return html;
        }

        //Render Conversations List
        function renderList(threads, showThreads) {
            var conversationHTML;
            var participant;
            var spokenLangs;
            var learningLangs;
            var interests;
            var unreadMessageCount;
            var threadInfo;

            if (threads.length == 0) {
                conversationHTML = '<div class="main-column my-profile hide-bars-on-scroll"><div class="noConversations"><img src="../../images/shiro/shiro-hat.png" /><h2>Invite partners to start interesting discussions with them and practise languages!</h2></div></div>';
            }
            else {

                conversationHTML = '<div class="list-block media-list conversation-list"><ul>';
                threads.forEach(function(thread, index) {
                    var participants = thread.users;

                    participants = participants.filter(function(participant) {
                        return participant.id != window.authUser.id;
                    });


                    participant = participants[0];
                    spokenLangs = participant.spoken_languages;
                    learningLangs = participant.learning_languages;
                    interests = participant.interests;

                    if (index < showThreads && filterFalse(thread)) {
                        threadInfo = '<li>' +
                            '<div id="chat-' + thread.id + '" data-userid="' + participant.id + '" data-thread-id="' + thread.id + '" class="item-content conversation-item">' +
                            '<div class="conversation-item__options js-item-options">' +
                            '<a href="#" class="js-delete">Delete chat</a>' +
                            '<a href="#" class="js-block">Block user</a>' +
                            '</div>' +
                            '<div class="item-media conversation-item__media">' +
                            '<img class="lazy conversation-item__avatar js-user-avatar"' +
                            'src="' + (participant.avatar ? participant.avatar : 'images/default-avatar.png') + '"' +
                            'width="100%">' +
                            '<div class="conversation-item__flag">' +
                            '<span class="flag ' + participant.country.iso_3166_2.toLowerCase() + '">' +
                            '</div>' +
                            '</div>' +
                            '<div class="item-inner conversation-item__inner">' +
                            '<div class="item-title-row">' +
                            '<h3 class="item-title conversation-item__title js-user-name noselect">' + participant.first_name + ' ' + participant.family_name + '<span' +
                            ' class="online-circle"></span></h3>' +
                            '<div class="item-after">' +
                            '<span class="conversation-item__last-login noselect">' + getTimeDiff(thread.updated_at) + '</span>' +
                            '<a href="#" class="options-dots"><i class="fa fa-ellipsis-v fa-lg" aria-hidden="true"></i></a>' +
                            '</div>' +
                            '</div>' +
                            '<div class="conversation-item__badge-placeholder item-after">';

                        unreadMessageCount = window.getUnreadMessages(thread.id);

                        if (unreadMessageCount) {
                            threadInfo += '<span class="badge bg-red unread-messages">' + unreadMessageCount + '</span>';
                        }

                        //render unread message count
                        window.renderUnreadMessageCount();

                        threadInfo += '</div><div class="item-subtitle">' +
                            '<div class="conversation-item__languages subtitle">';

                        threadInfo += renderLanguageList(spokenLangs);

                        if (learningLangs.length > 0) {
                            threadInfo += '<i class="fa fa-long-arrow-right"></i>';
                            threadInfo += renderLanguageList(learningLangs);
                        }

                        threadInfo += '</div>' +
                            renderInterestsList(interests);

                        threadInfo += '<p class="item-text conversation-item__lastmessage noselect">' + (thread.subject ? thread.subject : '') + '</p>';

                        threadInfo += '</div>' +
                            '</div>' +
                            '</li>';
                        conversationHTML += threadInfo;
                    }
                });
                conversationHTML += '</ul></div>';
            }
            $('.conversation-page')
                .html(conversationHTML);

            threads.forEach(function(thread, index) {
                makeApiRequest('messages/' + thread.id, null, 'GET', true)
                    .success(function(messages) {
                        var lastMessage = messages.messages[messages.messages.length - 1];
                        $('#chat-' + thread.id)
                            .find('.conversation-item__lastmessage')
                            .html(lastMessage.body);
                    });
            });


            window.blockDeleteUser();
            headerLinks();
            blockUser();
            deleteUser();
        }

        /****************
         * Infinite Sroll
         * @type {boolean}
         */


        // Attach 'infinite' event handler
        $('.infinite-scroll')
            .on('infinite', function() {

                // Exit, if loading in progress
                if (loading) {
                    return;
                }

                // Set loading flag
                loading = true;

                // Emulate 1s loading
                setTimeout(function() {
                    // Reset loading flag
                    loading = false;

                    if (lastIndex >= threads.length) {
                        // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
                        myApp.detachInfiniteScroll($('.infinite-scroll'));
                        // Remove preloader
                        $('.infinite-scroll-preloader')
                            .remove();
                        return;
                    }

                    showThreads += itemsPerLoad;
                    //request conversations
                    makeApiRequest('messages', null, 'GET', true)
                        .success(function(res) {
                            threads = res.threads;
                            renderList(threads, showThreads);
                        });

                    // Update last loaded index
                    lastIndex = $('.list-block li').length;
                }, 1000);
            });

        if (showThreads >= threads.length) {
            myApp.detachInfiniteScroll($('.infinite-scroll'));
            $('.infinite-scroll-preloader')
                .remove();
        }


        /*****************
         * Pull to refresh
         * @type {string[]}
         */

        $('.pull-to-refresh-content')
            .on('refresh', function(e) {

                // Exit, if loading in progress
                if (loading) {
                    return;
                }

                // Set loading flag
                loading = true;

                // Emulate 1s loading
                setTimeout(function() {
                    var lastIndex = $('.list-block li').length;
                    // Reset loading flag
                    loading = false;

                    if (lastIndex >= threads.length) {
                        // Nothing more to load, detach pull to refresh to prevent unnecessary loadings
                        myApp.pullToRefreshDone();
                        // Remove preloader
                        myApp.hideIndicator();
                        return;
                    }

                    showThreads += itemsPerLoad;
                    //request conversations
                    makeApiRequest('users/me', null, 'GET', true)
                        .success(function(res) {
                            threads = res.threads;
                            renderList(threads, showThreads);
                        });
                }, 1000);
            });

        if (showThreads >= threads.length) {
            myApp.pullToRefreshDone();
            myApp.hideIndicator();
        }

        /**
         * Disable infinite scroll if the outerHeight of list of conversations is less than the height of the window
         */
        if ($('.conversation-list')
                .outerHeight() < $(window)
                .height()) {
            $('.infinite-scroll-preloader')
                .remove();
        }


        /**
         * Block User
         */
        function blockUser() {
            $('.js-block')
                .on('click', function() {
                    var id = $(this)
                        .closest('.conversation-item')
                        .attr('data-userid');
                    makeApiRequest('users/' + id + '/block', null, 'POST', true);
                    $(this)
                        .closest('li')
                        .slideUp(300, function() {
                            $(this)
                                .remove();
                        });
                });
        }

        /**
         * Delete User
         */
        function deleteUser() {
            $('.js-delete')
                .on('click', function() {
                    var id = $(this)
                        .closest('.conversation-item')
                        .attr('data-thread-id');
                    makeApiRequest('messages/' + id, null, 'DELETE', true);
                    $(this)
                        .closest('li')
                        .slideUp(300, function() {
                            $(this)
                                .remove();
                        });
                });
        }

    });
}(jQuery, document, window));
