(function($, document, window, navigator) {
    'use strict';
    myApp.onPageInit('profile-filling', function(page) {
        var makeApiRequest = window.makeApiRequest;
        var countries;
        var userId;


        myApp.showIndicator();

        myApp.support.touch && ageInit();

        makeApiRequest('countries', null, 'GET', true)
            .done(countryDropdownInit);

        makeApiRequest('users/me', null, 'GET', true)
            .done(function(res) {
                userId = res.id;
                if (res.avatar) {
                    $('#avatar-image')
                        .attr('src', res.avatar)
                        .addClass('avatar');
                }

                userProfileInit(res);
            });


        if (page.query.edit) {
            $('.back')
                .show();
        }
        else {
            $('.back')
                .hide();
        }
        //upload profile image popup
        $('#avatar-image')
            .on('click', function() {
                var popupHTML = '<div class="popup profile-image-popup">' +
                    '<div class="content-block">' +
                    '<h1 class="profile-image-popup__title">Select Profile Image</h1>' +
                    '<div id="upload-preview-profile" class="dropzone-previews">' +
                    '<div class="preview-template file-row">' +
                    '<div>' +
                    '<span class="preview"><img data-dz-thumbnail /></span>' +
                    '</div>' +
                    '<div>' +
                    '<strong class="error text-danger" data-dz-errormessage></strong>' +
                    '</div>' +
                    '<div>' +
                    '<p class="size" data-dz-size></p>' +
                    '<div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">' +
                    '<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="profile-image-popup__upload">' +
                    '<p class="buttons-row">' +
                    '<button class="button button-fill button-raised no-fastclick" id="upload-btn-profile">Upload</button>' +
                    '</p>' +
                    '</div>' +
                    '<div class="profile-image-popup__upload">' +
                    '</div>' +
                    '<p><a href="#" class="close-popup profile-image-popup__close">Close</a></p>' +
                    '</div>' +
                    '</div>';
                myApp.popup(popupHTML);
                setupUploadProfile();
            });

        function setupUploadProfile() {
            var $preview,
                previewTemplate,
                uploader;

            // prevent multiple intsantiation of dropzone
            if ($('#upload-btn-profile')
                    .hasClass('dz-clickable')) {
                return;
            }
            $preview = $('#upload-preview-profile');
            previewTemplate = $preview.html();
            $preview.empty();

            uploader = new Dropzone('#upload-btn-profile', {
                method: 'POST',
                url: 'https://k567d6c3nqbrdzb5.app.bilingua.io/api/users/me/avatar',
                // file size in MB
                maxFilesize: 7,
                paramName: 'image',
                uploadMultiple: false,
                withCredentials: true,
                headers: {
                    'Cache-Control': null,
                    'X-Requested-With': null
                },
                acceptedFiles: 'image/*',
                previewsContainer: '#upload-preview-profile',
                previewTemplate: previewTemplate,
                thumbnailWidth: null,
                thumbnailHeight: 120
            });
            uploader.on('addedfile', function() {
                // $('#upload-btn-profile').prop('disabled', true);
                $('#upload-btn-profile')
                    .remove();
                $('.profile-image-popup__close')
                    .remove();
            });
            uploader.on('success', function(file, response) {
                myApp.closeModal('.profile-image-popup');
                myApp.addNotification({
                    hold: 5000,
                    message: 'Profile Image Added!'
                });
                setImageSrc($('#avatar-image')[0], 'https://k567d6c3nqbrdzb5.app.bilingua.io/avatars/' + userId);
                $('#avatar-image')
                    .addClass('avatar');
            });
            uploader.on('error', function(file, errorMessage, xhr) {
            });
        }

        function setImageSrc(imgNode, src) {
            imgNode.src = src + '?nocache=' + (Math.random() * 1000);
        }

        $('.button')
            .on('click', function() {
                var $self = $(this);
                if ($self.hasClass('gender-unselected')) {
                    $self.closest('.row')
                        .find('.button')
                        .removeClass('color-red')
                        .addClass('color-blue');
                    $self.closest('.row')
                        .find('.button')
                        .removeClass('gender-selected')
                        .addClass('gender-unselected');
                    $self.removeClass('gender-unselected');
                    $self.removeClass('color-blue');
                    $self.addClass('gender-selected');
                    $self.addClass('color-red');
                }
                else {
                    $self.removeClass('color-blue');
                    $self.addClass('color-red');
                }
            });
        function ageInit() {
            var ages = [];
            var c;

            for (c = 13; c <= 80; c++) {
                ages.push(c);
            }

            myApp.picker({
                input: '#picker_age',
                cols: [
                    {
                        textAlign: 'center',
                        values: ages
                    }
                ]
            });
        }

        function countryDropdownInit(response) {
            countries = response;

            myApp.autocomplete({
                input: '#autocomplete_country',
                openIn: 'dropdown',
                source: function(autocomplete, query, render) {
                    var results = [];
                    var i,
                        len;

                    if (query.length === 0) {
                        render(results);
                        return;
                    }
                    for (i = 0, len = countries.length; i < len; i++) {
                        if (countries[i].name.toLowerCase()
                                .indexOf(query.toLowerCase()) >= 0) {
                            results.push(countries[i].name);
                        }
                    }
                    render(results);
                }
            });
        }

        function getCountryID(country) {
            var c,
                len;

            for (c = 0, len = countries.length; c < len; c++) {
                if (countries[c].name.toLowerCase() === country.toLowerCase()) {
                    return countries[c].id;
                }
            }
            return null;
        }

        function userProfileInit(response) {

            myApp.onPageAfterAnimation('profile-filling', function(page) {
                // if country name is singapore which is default name returned by api if its not available from users account
                if (response.country.name === 'Singapore' || response.country.name === undefined || response.country.name === null) {

                    window.getLocation(
                        // this callback will be fired after geolocation is successfull
                        // getlocation function has two local variables country and city which get filled onces it completes successfully
                        // these fields city and country are then passed to the callback function
                        function callback(country, city) {

                            $('input[name="country"]')
                                .focus()
                                .val(country);
                            $('input[name="city"]')
                                .focus()
                                .val(city);

                        });
                }
            })
            .trigger();

            myApp.autocomplete({
                input: '#autocomplete_city',
                openIn: 'dropdown',
                preloader: true,
                valueProperty: 'id',
                textProperty: 'name',
                limit: 20,
                dropdownPlaceholderText: 'Try "JavaScript"',
                expandInput: true,
                source: function(autocomplete, query, render) {
                    var results = [];

                    if (query.length === 0) {
                        render(results);
                        return;
                    }
                    // Show Preloader
                    autocomplete.showPreloader();
                    // Do Ajax request to Autocomplete data
                    $.ajax({
                        url: 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + query + '&types=(cities)&key=AIzaSyByhc-vuL99jHG3AxM5oq2d9jkLQ9tnoSY',
                        method: 'GET',
                        dataType: 'json',

                        success: function(data) {

                            var i = 0;

                            // Find matched items
                            for (i = 0; i < data.length; i++) {
                                if (data[i].name.toLowerCase()
                                        .indexOf(query.toLowerCase()) >= 0) {
                                    results.push(data[i]);
                                }
                            }
                            // Hide Preoloader
                            autocomplete.hidePreloader();
                            // Render items by passing array with result items
                            render(results);
                        }
                    });
                }
            });

            $('input[name="firstname"]')
                .focus()
                .val(response['first_name']);
            $('input[name="lastname"]')
                .focus()
                .val(response['family_name']);
            switch (response['gender']) {
                case 1:
                    $('#gender_m')
                        .click();
                    break;
                case 2:
                    $('#gender_f')
                        .click();
                    break;
                case 3:
                    $('#gender_o')
                        .click();
                    break;
            }

            //Birthday
            if (response['birthday'] != 'undefined') {
                $('input[name="birthday"]')
                    .focus()
                    .val(response['birthday']);
                $('input[name="age"]')
                    .focus()
                    .val(calculateAge($('input[name="birthday"]').val()));
            }
            else {
                $('input[name="birthday"]')
                    .focus()
                    .val('1900-01-01');
                $('input[name="age"]')
                    .focus()
                    .val(calculateAge($('input[name="birthday"]').val()));
            }

            // Calculate age
            if (localStorage.register == 'false' || localStorage.update == 'true') {
                $('input[name="country"]')
                    .focus()
                    .val(response.country.name);
            }

            myApp.hideIndicator();
        }

        $('#your-bithday').on('change', function() {
            var ageCalc = calculateAge($(this).val());
            $('input[name="age"]').val(ageCalc);
        });

        $('#picker_age').on('change', function() {
            var d = new Date();
            var birthYear = d.setFullYear(d.getFullYear() - $(this).val());
            $('input[name="birthday"]').val(new Date(birthYear).toISOString().slice(0, 10));
        });

        function calculateAge(birthday) {
            return new Date().getFullYear() - new Date(birthday).getFullYear();
        }

        function submitUserDetails() {
            var user = {
                'first_name': $('input[name="firstname"]')
                    .val()
                    .trim(),
                'family_name': $('input[name="lastname"]')
                    .val()
                    .trim(),
                'gender': $('.gender-selected')
                    .text(),
                'birthday': $('input[name="birthday"]')
                    .val()
                    .trim(),
                'country': $('input[name="country"]')

                    .val(),
                'city': $('input[name="city"]')
                    .val()
                    .trim()

            };
            var flag = true;
            /*for (var prop in user) {
             if (user[prop] == '') {
             myApp.alert('Please fill in your ' + prop.replace('_', ' ') + '.', 'Missing Field');
             flag = false;
             break;
             }
             }*/
            if (flag === true) {
                //user.birthday = user.age;
                user['country_id'] = getCountryID(user.country);
                delete user.country;
                //delete user.age;
                switch (user.gender) {
                    case 'Male':
                        user.gender = 1;
                        break;
                    case 'Female':
                        user.gender = 2;
                        break;
                    case 'Other':
                        user.gender = 3;
                        break;
                }
                localStorage.removeItem('register');
                localStorage.update = true;
                makeApiRequest('users/me', user, 'PATCH', true)
                    .success(function() {
                        if (page.query.edit == '1') {
                            mainView.router.loadPage('my-profile.html');
                            //Adding Notification for Language on  update
                            window.BilinguaAlert.show('Profile successfully updated, ' + user['first_name']);
                        }
                        else if (page.query.edit == '2') {
                            mainView.router.loadPage('chats.html');
                        }
                        else {
                            mainView.router.loadPage('language-selection.html');
                        }
                    })
                    .error(function() {
                        alert('There was an error!');
                        mainView.router.loadPage('chats.html');
                    });
            }
            return false;
        }

        $('#profile-form')
            .submit(submitUserDetails);
    });
}(jQuery, document, window, navigator));
