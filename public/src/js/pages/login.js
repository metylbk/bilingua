(function($, document, window) {

    'use strict';

    myApp.onPageAfterAnimation('login', function(page) {
        var Auth = window.Auth;
        var SocialAuth = window.SocialAuth;
        myApp.hideIndicator();
        bindEvents();

        // Feedback
        window.feedback.showPopup();

        /**
         * Registers the user on the system.
         */
        function register(email, password) {
            myApp.showPreloader('Registering');

            Auth.register(email, password, function(status) {
                if (status) {
                    myApp.hidePreloader();
                    onRegistration();
                }
                else {
                    myApp.hidePreloader();
                    $('.error-reg .error-msg').text('Something went wrong. Please try again with Facebook or Google.');
                    $('.error-reg').slideDown('fast');
                }
            });
        }

        /**
         * Logs the user onto the system.
         */
        function login(email, password, origReg) {
            myApp.showPreloader('Logging In');

            Auth.login(email, password, function(status) {
                if (status) {
                    myApp.hidePreloader();
                    onLogin();
                }
                else {
                    myApp.hidePreloader();
                    if (origReg) {
                        $('.error-login .error-msg').text(' There is an account for this email, but the password does not match.');
                    }
                    else {
                        $('.error-login .error-msg').text(' Incorrect email or password.');
                    }
                    $('.error-login').slideDown('fast');
                }
            });
        }

        /**
         * Processes the user's registration input and calls register() or login().
         */
        function registrationSubmit(e) {
            var $regForm = $('#registration-form');
            var email = $regForm.find('input[name="reg-email"]').val().toLowerCase().trim(),
                password = $regForm.find('input[name="reg-pass"]').val().trim();
            e.preventDefault();
            Auth.checkEmail(email, function(status) {
                if (!status) {
                    register(email, password);
                }
                else {
                    $('input[name="reg-email"]').val('').trim();
                    $('input[name="reg-pass"]').val('').trim();
                    $('input[name="login-email"]').val(email).trim();
                    $('input[name="login-pass"]').val(password).trim();
                    myApp.showTab('#loginTab');
                    login(email, password, true);
                }
            });
        }

        /**
         * Callback function on successful registration.
         */
        function onRegistration() {
            localStorage.register = true;
            mainView.router.loadPage('chats.html');
        }

        /**
         * Processes the user's login input and calls login() or switches to registration form.
         */
        function loginSubmit(e) {
            var $loginForm = $('#login-form');
            var email = $loginForm.find('input[name="login-email"]').val().toLowerCase(),
                password = $loginForm.find('input[name="login-pass"]').val();
            e.preventDefault();
            Auth.checkEmail(email, function(status) {
                if (status) {
                    login(email, password);
                }
                else {
                    myApp.confirm('Would you like to register instead?', 'Email Not Found', function() {
                        // if "ok" is clicked
                        $('input[name="login-email"]').val('');
                        $('input[name="login-pass"]').val('');
                        $('input[name="reg-email"]').val(email).closest('.item-inner').addClass('focus-state');
                        myApp.showTab('#regTab');
                    }, function() {
                        // do nothing if "cancel"
                    });
                }
            });
        }

        /**
         * Social login using Facebook/Google.
         */
        function socialLogin(provider) {
            myApp.showPreloader('Logging In');

            SocialAuth.login(provider, function(status) {
                if (status) {
                    myApp.hidePreloader();
                    SocialAuth.getUserData(provider);
                    onRegistration();
                }
                else {
                    myApp.hidePreloader();
                    myApp.alert('Something went wrong. Please try again with Facebook or Google.', 'Error');
                }
            });
        }

        /**
         * Callback function on successful login.
         */
        function onLogin() {
            localStorage.register = false;
            mainView.router.loadPage('chats.html');
        }


        /**
         * Field for user to input email address if password forgotten
         */
        // function forgotPassword () {
        //     $('.forgot-pass').on('click', function () {
        //         var popupHTML = '<div class="popup options-popup">' +
        //             '<div class="list-block">' +
        //             '<p class="address-title">Please fill in your email address</p>' +
        //             '<ul>' +
        //             '<li>' +
        //             '<div class="item-content">' +
        //             '<div class="item-media"><i class="icon icon-form-email"></i></div>' +
        //             '<div class="item-inner">' +
        //             '<div class="item-input">' +
        //             '<input type="email" placeholder="E-mail">' +
        //             '</div>' +
        //             '</div>' +
        //             '</div>' +
        //             '</li>' +
        //             '</ul>' +
        //             '<p class="options-popup__link"><a href="#" class="close-popup">Cancel</a>' +
        //             '<p class="options-popup__link"><a href="#" class="send-email">Send</a>' +
        //             '</div>' +
        //             '</div>';
        //         myApp.popup(popupHTML);
        //     });
        //
        //     function sendEmail () {
        //         $('.options-popup').on('click', '.send-email', function () {
        //
        //
        //         	var feedback = {feedback: $('#text-area').val()};
        // 			makeApiRequest('sendEmail', null, 'POST', true).done(
        //     		function (res) {
        //
        //         	myApp.alert('Check your email for the instructions', 'Success!');
        //         	mainView.router.loadPage('login.html');
        //
        //     		});
        //         });
        //     };
        // };
        //
        // forgotPassword();

        /**
         * Binds the appropriate event callbacks for the UI elements.
         */
        function bindEvents() {
            $('.error-login').hide();
            $('.error-reg').hide();

            // $('#submitLogin').on('click', loginSubmit);
            //$('#submitReg').on('click', registrationSubmit);

            $('#login-form').submit(loginSubmit);

            $('#registration-form').submit(registrationSubmit);

            $('#fbBtn').on('click', function() {
                if (detectCordova()) {
                    cordovaApp.fbLogin();
                }
                else {
                    socialLogin('facebook');
                }

            });

            $('#googleBtn').on('click', function() {
                if (detectCordova()) {
                    cordovaApp.googleLogin();
                }
                else {
                    socialLogin('google');
                }
            });

            $('.forgot-pass').on('click', function() {
                var email = $('#login-email').val();
                $('.external').attr('href', 'mailto:hello@bilingua.io?subject=Please%20Help!%20I%20forgot%20my%20Bilingua%20password.&body=' + email);
            });

            $('input').focus(function() {
                $('.error-login').slideUp('fast');
                $('.error-reg').slideUp('fast');
            });

            $('.tab-link').click(function() {
                $('.error-login').hide();
                $('.error-reg').hide();
            });

            $('.error-dismiss').click(function() {
                $(this).parent().parent().hide();
            });

            // $('.forgot-pass').click(function () {
            //     myApp.confirm('We\'ll send you instructions on how to reset your password to your email.', 'Forgot password?');
            // });
        }
    });

}(jQuery, document, window));
