(function($, document, window) {

    'use strict';

    myApp.onPageAfterAnimation('my-profile', function(page) {


        var user,
            userId,
            interestsHTML,
            age;

        //render unread message count
        window.renderUnreadMessageCount();

        //Calculate age
        function calculateAge(birthday) {
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs);
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

          // get Country and City from geolocation

        myApp.showIndicator();

        makeApiRequest('users/me', null, 'GET', true)
            .done(function(res) {
                user = res;
                userId = user.id;
                interestsHTML = '';

                myApp.hideIndicator();

                age = calculateAge(new Date(user.birthday));


                $('#user-name')

                    .text(user.first_name + ' ' + user.family_name);
                $('#location')
                    .text(user.country.name);
                $('#age')
                    .text(age + ' yo, ');
                $('#about-me')
                    .text(user.description);
                $('#tagline')
                    .text(user.bio);

                if (user.avatar) {
                    $('#user-avatar')
                        .attr('src', user.avatar);
                }

                $('#user-flag')
                    .addClass('flag ' + user.country.iso_3166_2.toLowerCase());
                $('#gender')
                    .html(window.getGender(user.gender) + ', ');

                if (user.cover_image) {
                    $('#banner-image')
                        .attr('src', user.cover_image);
                }


                $.each(user.interests, function(i, obj) {
                    interestsHTML += '<div class="chip custom-chip">';
                    interestsHTML += '<div class="chip-label">' + obj.name + '</div>';
                    interestsHTML += '</div>';
                });

                $('.interests__chips')
                    .html(interestsHTML);

                // languages & proficiency
                function myLanguages(speakingLearning) {

                    // initiate variables
                    var myLanguagesHTML = '';
                    var langAPI = '';

                    // determine which API object to use, speaking or learning
                    if (speakingLearning == 'speaking') {
                        langAPI = user.spoken_languages;
                    }
                    else if (speakingLearning == 'learning') {
                        langAPI = user.learning_languages;
                    }
                    // function to return proficiency class
                    function getProfClass(proficiency) {
                        if (proficiency == '1') {
                            return 'elementary';
                        }
                        else if (proficiency == '2') {
                            return 'intermediate';
                        }
                        else if (proficiency == '3') {
                            return 'advanced';
                        }
                        else if (proficiency == '4') {
                            return 'fluent';
                        }
                        else if (proficiency == '5') {
                            return 'native';
                        }
                    }

                    // loop through each language array
                    $.each(langAPI, function(i, obj) {

                        var proficiency = obj.pivot.proficiency;
                        var profClass = getProfClass(proficiency);

                        myLanguagesHTML += '<div class="languages__column">';
                        myLanguagesHTML += '<div class="language-item">';
                        myLanguagesHTML += '<div class="languages__flag-container">';
                        myLanguagesHTML += '<div class="set-size charts-container">';
                        myLanguagesHTML += '<div class="pie-wrapper progress-' + profClass + '">';
                        myLanguagesHTML += '<div class="pie">';
                        myLanguagesHTML += '<div class="left-side half-circle"></div>';
                        myLanguagesHTML += '<div class="right-side half-circle"></div>';
                        myLanguagesHTML += '</div>';
                        myLanguagesHTML += '</div>';
                        myLanguagesHTML += '</div>';
                        myLanguagesHTML += '<img src="' + obj.flag + '">';
                        myLanguagesHTML += '</div>';
                        myLanguagesHTML += '<p class="name-lang">' + obj.english_name + '</p>';
                        myLanguagesHTML += '<p class="proficiency-name proficiency--' + profClass + '">' + getProfClass(proficiency) + '</p>';
                        myLanguagesHTML += '</div>';
                        myLanguagesHTML += '</div>';


                    });

                    $('.js-' + speakingLearning + '-language-container > .rows')
                        .html(myLanguagesHTML);
                }

                // initiate functions for speaking and learning languages
                myLanguages('speaking');
                myLanguages('learning');

                if (user.spoken_languages.length >= 3 || user.learning_languages.length >= 3) {
                    $('.languages__column')
                        .css('margin', '10px 4px');
                }

                // // Bind to the resize event of the window object
                // $(window).on('resize', function () {
                //     // set proper widths for parent containers when there are more than 4 languages (doesn't fit on one line)
                //     var totalLangs = user.spoken_languages.length + user.learning_languages.length;
                //     waitForFinalEvent(function () {
                //         if (totalLangs > 3) {
                //             if (window.innerWidth < 481) {
                //                 $('.js-speaking-language-container, .js-learning-language-container, .js-right-arrow').removeClass('col-45').addClass('col-100');
                //                 $('.js-right-arrow').removeClass('col-10').addClass('col-100');
                //                 $('.js-right-arrow .fa-chevron-right').css('margin-top', '0px');
                //             } else {
                //                 $('.js-speaking-language-container, .js-learning-language-container, .js-right-arrow').addClass('col-45').removeClass('col-100');
                //                 $('.js-right-arrow').addClass('col-10').removeClass('col-100');
                //                 $('.js-right-arrow .fa-chevron-right').removeAttr('style');
                //
                //             }
                //         }
                //     }, 500, 'some unique string');
                // }).resize();

                window.Interests.linkChips();

            });

        window.feedback.showPopup();

        //upload banner image popup
        $('.banner-image__edit')
            .on('click', function() {
                var popupHTML = '<div class="popup cover-image-popup">' +
                    '<div class="content-block">' +
                    '<h1 class="cover-image-popup__title">Select New Cover Image</h1>' +
                    '<div id="upload-preview" class="dropzone-previews">' +
                    '<div class="preview-template file-row">' +
                    '<div>' +
                    '<span class="preview"><img data-dz-thumbnail /></span>' +
                    '</div>' +
                    '<div>' +
                    '<strong class="error text-danger" data-dz-errormessage></strong>' +
                    '</div>' +
                    '<div>' +
                    '<p class="size" data-dz-size></p>' +
                    '<div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">' +
                    '<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="cover-image-popup__upload">' +
                    '<p class="buttons-row">' +
                    '<button class="button button-fill button-raised no-fastclick" id="upload-btn">Upload</button>' +
                    '</p>' +
                    '</div>' +
                    '<div class="cover-image-popup__upload">' +
                    '</div>' +
                    '<p><a href="#" class="close-popup cover-image-popup__close">Close</a></p>' +
                    '</div>' +
                    '</div>';
                myApp.popup(popupHTML);

                //$('.cover-image-popup').on('open', function () {
                //});
                setupUpload();
            });

        function setupUpload() {
            var $preview,
                previewTemplate,
                uploader;
            // prevent multiple intsantiation of dropzone
            if ($('#upload-btn')
                    .hasClass('dz-clickable')) {
                return;
            }
            $preview = $('#upload-preview');
            previewTemplate = $preview.html();
            $preview.empty();
            uploader = new Dropzone('#upload-btn', {
                method: 'POST',
                url: 'https://k567d6c3nqbrdzb5.app.bilingua.io/api/users/me/images/cover',
                // file size in MB
                maxFilesize: 7,
                paramName: 'image',
                uploadMultiple: false,
                withCredentials: true,
                headers: {
                    'Cache-Control': null,
                    'X-Requested-With': null
                },
                acceptedFiles: 'image/*',
                previewsContainer: '#upload-preview',
                previewTemplate: previewTemplate,
                thumbnailWidth: null,
                thumbnailHeight: 120
            });
            uploader.on('addedfile', function() {
                // $('#upload-btn').prop('disabled', true);
                myApp.showIndicator();
                $('#upload-btn')
                    .remove();
                $('.cover-image-popup__close')
                    .remove();
            });
            uploader.on('success', function(file, response) {
                myApp.hideIndicator();
                myApp.closeModal('.cover-image-popup');
                myApp.addNotification({
                    hold: 5000,
                    message: 'Cover Image Updated'
                });
                //mainView.router.refreshPage();
                setImageSrc($('#banner-image')[0], 'https://k567d6c3nqbrdzb5.app.bilingua.io/images/covers/' + userId);
                // $('#banner-image').attr('src', '');
                //
                // setTimeout(function() {
                //     $('#banner-image').attr('src', 'https://staging.bilingua.io/images/covers/' + userId);
                // }, 0);
            });
            uploader.on('error', function(file, errorMessage, xhr) {
            });
        }

        function setImageSrc(imgNode, src) {
            imgNode.src = src + '?nocache=' + (Math.random() * 1000);
        }


        //upload profile image popup
        $('.profile__edit')
            .on('click', function() {
                var popupHTML = '<div class="popup profile-image-popup">' +
                    '<div class="content-block">' +
                    '<h1 class="profile-image-popup__title">Select New Profile Image</h1>' +
                    '<div id="upload-preview-profile" class="dropzone-previews">' +
                    '<div class="preview-template file-row">' +
                    '<div>' +
                    '<span class="preview"><img data-dz-thumbnail /></span>' +
                    '</div>' +
                    '<div>' +
                    '<strong class="error text-danger" data-dz-errormessage></strong>' +
                    '</div>' +
                    '<div>' +
                    '<p class="size" data-dz-size></p>' +
                    '<div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">' +
                    '<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="profile-image-popup__upload">' +
                    '<p class="buttons-row">' +
                    '<button class="button button-fill button-raised no-fastclick" id="upload-btn-profile">Upload</button>' +
                    '</p>' +
                    '</div>' +
                    '<div class="profile-image-popup__upload">' +
                    '</div>' +
                    '<p><a href="#" class="close-popup profile-image-popup__close">Close</a></p>' +
                    '</div>' +
                    '</div>';
                myApp.popup(popupHTML);
                setupUploadProfile();
            });


        function setupUploadProfile() {
            var $preview,
                previewTemplate,
                uploader;
            // prevent multiple intsantiation of dropzone
            if ($('#upload-btn-profile')
                    .hasClass('dz-clickable')) {
                return;
            }
            $preview = $('#upload-preview-profile');
            previewTemplate = $preview.html();
            $preview.empty();
            uploader = new Dropzone('#upload-btn-profile', {
                method: 'POST',
                url: 'https://k567d6c3nqbrdzb5.app.bilingua.io/api/users/me/avatar',
                maxFilesize: 7,
                paramName: 'image',
                uploadMultiple: false,
                withCredentials: true,
                headers: {
                    'Cache-Control': null,
                    'X-Requested-With': null
                },
                acceptedFiles: 'image/*',
                previewsContainer: '#upload-preview-profile',
                previewTemplate: previewTemplate,
                thumbnailWidth: null,
                thumbnailHeight: 120
            });

            uploader.on('addedfile', function() {
                // $('#upload-btn-profile').prop('disabled', true);
                myApp.showIndicator();
                $('#upload-btn-profile')
                    .remove();
                $('.profile-image-popup__close')
                    .remove();

            });

            uploader.on('success', function(file, response) {
                myApp.hideIndicator();
                myApp.closeModal('.profile-image-popup');

                myApp.addNotification({
                    hold: 5000,
                    message: 'Profile Image Updated!'
                });

                // mainView.router.refreshPage();

                setImageSrc($('#user-avatar')[0], 'https://k567d6c3nqbrdzb5.app.bilingua.io/avatars/' + userId);
            });

            uploader.on('error', function(file, errorMessage, xhr) {
            });
        }

        //TAG LINE popup
        // $('.create-popup-tagline').on('click', function () {
        //     var popupHTML = '<div class="popup tagline-popup">' +
        //         '<form id="tagline-form">' +
        //         '<div class="content-block">' +
        //         '<h1 class="tagline-popup__title">Edit BIO</h1>' +
        //         '<div class="list-block">' +
        //         '<div class="align-top">' +
        //         '<div class="item-content">' +
        //         '<div class="item-inner">' +
        //         '<div class="item-input">' +
        //         '<textarea id="tagline-textarea">' + $('#tagline').text() + '</textarea>' +
        //         '</div>' +
        //         '</div>' +
        //         '</div>' +
        //         '</div>' +
        //         '</div>' +
        //         '</div>' +
        //         '<div class="tagline-popup__save open-indicator">' +
        //         '<p class="buttons-row">' +
        //         '<input type="submit" name="submit" value="Save" class="button button-fill button-raised open-indicator notification-single" id="save-tagline">' +
        //         '</p>' +
        //         '</div>' +
        //         '<p><a href="#" class="close-popup tagline-popup__close">Cancel</a></p>' +
        //         '</form>' +
        //         '</div>';
        //     myApp.popup(popupHTML);
        //     updateTagline()
        // });

        $('.filling-profile')
            .on('click', function() {
                mainView.router.loadPage('profile-filling.html?edit=1');
            });


        //ABOUT ME popup
        $('.create-popup-about')
            .on('click', function() {
                var popupHTML = '<div class="popup about-me-popup">' +
                    '<form id="about-form">' +
                    '<div class="content-block">' +
                    '<h1 class="about-me-popup__title">Edit ABOUT ME</h1>' +
                    '<div class="list-block">' +
                    '<div class="align-top">' +
                    '<div class="item-content">' +
                    '<div class="item-inner">' +
                    '<div class="item-input">' +
                    '<textarea id="about-me-textarea">' + $('#about-me')
                        .text() + '</textarea>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="about-me-popup__save open-indicator">' +
                    '<p class="buttons-row">' +
                    '<input type="submit" name="submit" value="Save" class="button button-fill button-raised open-indicator notification-single" id="save-about">' +
                    '</p>' +
                    '</div>' +
                    '<p><a href="#" class="close-popup about-me-popup__close">Cancel</a></p>' +
                    '</form>' +
                    '</div>';
                myApp.popup(popupHTML);
                updateAboutMe();
            });


        //updating the ABOUT-ME section
        function updateAboutMe() {
            $('.about-me-popup')
                .on('submit', '#about-form', function(e) {
                    var data = {
                        description: $('#about-me-textarea')
                            .val().trim()
                    };
                    e.preventDefault();
                    myApp.showIndicator();
                    $('#save-about')
                        .prop('disabled', true);
                    makeApiRequest('users/me', data, 'PATCH', true)
                        .done(function(res) {

                            $('#about-me')
                                .text(res.description);

                            myApp.hideIndicator();

                            myApp.addNotification({
                                hold: 5000,
                                message: 'Saved!'
                            });

                            myApp.closeModal('.about-me-popup');
                        });
                });
        }

        //redirect user for editing languages
        $('.languages__edit')
            .on('click', function() {
                mainView.router.loadPage('language-selection.html?edit=1');
            });

        $('.finish')
            .on('click', function() {
                submitInterests();
                // mainView.router.reloadPage('my-profile.html?abcd=1');
                // if (page.query.refresh) {
                //     mainView.router.reloadPage('my-profile.html');
                //
                //
                // }
            });

        //redirect user for editing interests
        $('.interests__edit')
            .on('click', function() {
                mainView.router.loadPage('interests.html?edit=1');
            });

    });

}(jQuery, document, window));
