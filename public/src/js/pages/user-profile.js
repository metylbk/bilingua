(function($, document, window) {

    'use strict';

    myApp.onPageAfterAnimation('user-profile', function(page) {
        var id = mainView.activePage.query.id.slice(5);
        window.fromChat = false;
        window.renderAgain = undefined;

        //Calculate age
        function calculateAge(birthday) {
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs);
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        myApp.showIndicator();

        makeApiRequest('users/' + id, null, 'GET', true).done(function(userArr) {
            var user = userArr;
            var interestsHTML = '';
            var age = calculateAge(new Date(user.birthday));

            myApp.hideIndicator();


            $('#user-name').text(user.first_name + ' ' + user.family_name);
            $('#location').text(user.country.name);
            $('#age').text(age + ' yo, ');
            $('#about-me').text(user.description);
            $('#tagline').text(user.bio);

            if (user.avatar) {
                $('#user-avatar').attr('src', user.avatar);
            }

            $('#user-flag').addClass('flag ' + user.country.iso_3166_2.toLowerCase());

            $('#gender').html(getGender(user.gender) + ', ');

            if (user.cover_image) {
                $('#banner-image').attr('src', user.cover_image);
            }

            $.each(user.interests, function(i, obj) {
                interestsHTML += '<div class="chip custom-chip" data-languageid=' + obj.id + '>';
                interestsHTML += '<div class="chip-label">' + obj.name + '</div>';
                interestsHTML += '</div>';
            });
            $('.user-interests__chips').html(interestsHTML);

            // languages & proficiency
            function myLanguages(speakingLearning) {

                // initiate variables
                var myLanguagesHTML = '';
                var langAPI = '';

                // determine which API object to use, speaking or learning
                if (speakingLearning == 'speaking') {
                    langAPI = user.spoken_languages;
                }
                else if (speakingLearning == 'learning') {
                    langAPI = user.learning_languages;
                }


                // function to return proficiency class
                function getProfClass(proficiency) {
                    if (proficiency == '1') {
                        return 'elementary';
                    }
                    else if (proficiency == '2') {
                        return 'intermediate';
                    }
                    else if (proficiency == '3') {
                        return 'advanced';
                    }
                    else if (proficiency == '4') {
                        return 'fluent';
                    }
                    else if (proficiency == '5') {
                        return 'native';
                    }
                }

                // loop through each language array
                $.each(langAPI, function(i, obj) {

                    var proficiency = obj.pivot.proficiency;
                    var profClass = getProfClass(proficiency);

                    myLanguagesHTML += '<div class="languages__column">';
                    myLanguagesHTML += '<div class="language-item">';
                    myLanguagesHTML += '<div class="languages__flag-container">';
                    myLanguagesHTML += '<div class="set-size charts-container">';
                    myLanguagesHTML += '<div class="pie-wrapper progress-' + profClass + '">';
                    myLanguagesHTML += '<div class="pie">';
                    myLanguagesHTML += '<div class="left-side half-circle"></div>';
                    myLanguagesHTML += '<div class="right-side half-circle"></div>';
                    myLanguagesHTML += '</div>';
                    myLanguagesHTML += '</div>';
                    myLanguagesHTML += '</div>';
                    myLanguagesHTML += '<img src="' + obj.flag + '">';
                    myLanguagesHTML += '</div>';
                    myLanguagesHTML += '<p class="name-lang">' + obj.english_name + '</p>';
                    myLanguagesHTML += '<p class="proficiency-name proficiency--' + profClass + '">' + getProfClass(proficiency) + '</p>';
                    myLanguagesHTML += '</div>';
                    myLanguagesHTML += '</div>';

                });

                $('.js-' + speakingLearning + '-language-container > .rows').html(myLanguagesHTML);

                if (user.learning_languages.length >= 5) {
                    $('.js-learning-language-container > .rows > .languages__column').css('margin', '10px 4px');
                }

                if (user.spoken_languages.length >= 5) {
                    $('.js-speaking-language-container > .rows > .languages__column').css('margin', '10px 4px');
                }
            }

            // initiate functions for speaking and learning languages
            myLanguages('speaking');
            myLanguages('learning');

            window.Interests.linkChips();
        });

        /**
         * Open conversation with user
         */

        $('.chat-init').on('click', function() {
            var data = {
                recipients: [id]
            };
            makeApiRequest('messages', data, 'POST', true)
                .success(function(data) {
                    mainView.router.load({
                        url: 'chatroom.html',
                        query: {
                            id: 'chat-' + data.id,
                            name: $('#user-name').text(),
                            image: $('#user-avatar').attr('src'),
                            fromPartner: true
                        }
                    });
                });

        });

        window.feedback.showPopup();

        // // Bind to the resize event of the window object
        // $(window).on('resize', function () {
        //     // set proper widths for parent containers when there are more than 4 languages (doesn't fit on one line)
        //     var totalLangs = $('.languages__column').length;
        //     waitForFinalEvent(function () {
        //         if (totalLangs > 3) {
        //             if (window.innerWidth < 481) {
        //                 $('.js-speaking-language-container, .js-learning-language-container, .js-right-arrow').removeClass('col-45').addClass('col-100');
        //                 $('.js-right-arrow').removeClass('col-10').addClass('col-100');
        //                 $('.js-right-arrow .fa-chevron-right').css('margin-top', '0px');
        //             } else {
        //                 $('.js-speaking-language-container, .js-learning-language-container, .js-right-arrow').addClass('col-45').removeClass('col-100');
        //                 $('.js-right-arrow').addClass('col-10').removeClass('col-100');
        //                 $('.js-right-arrow .fa-chevron-right').removeAttr('style');
        //
        //             }
        //         }
        //     }, 200, 'some unique string');
        // }).resize();

    });
}(jQuery, document, window));
