(function($, document, window) {

    'use strict';

    // Now we need to run the code that will be executed only for Feature page.
    myApp.onPageAfterAnimation('partners', function(page) {
        var mainUser;
        var makeApiRequest = window.makeApiRequest;
        // Get filter parameters, if any
        var filterParams = window.filterParams;
        // Global array of users
        var usersArr;
        // Last index from usersArr being displayed
        var endIndex = 0;
        var loading = true;

        /**
         * Pull to Refresh functionality (from Framework7).
         */
        var ptrContent = $('.pull-to-refresh-content');

        $('.filter-error-msg')
            .hide();


        // On devices without touch support, remove pull to refresh.
        if (!myApp.support.touch) {
            myApp.destroyPullToRefresh('.pull-to-refresh-content');
            $('.pull-to-refresh-layer')
                .hide();
            $('.page-content')
                .removeClass('pull-to-refresh-content');
        }


        // Delete parameters globally once stored in local var
        delete window.filterParams;


        $('.infinite-scroll-preloader')
            .hide();
        myApp.showIndicator();

        makeApiRequest('users/me', null, 'GET', true)
            .done(function(data) {
                mainUser = data;
                if (data.avatar) {
                    $('#user-partners-avatar')
                        .attr('src', data.avatar);
                }

                $('#user-box-name')
                     .text(data.first_name + ' ' + data.family_name);

                makeApiRequest('users', null, 'GET', true)
                    .done(function(res) {
                        usersArr = res;
                        renderUsers(20);
                        myApp.hideIndicator();

                        if (data.avatar) {
                            $('#user-avatar, #user-dropdown-avatar')
                                .attr('src', data.avatar);
                        }

                        $('#user-box-name')
                            .text(data.first_name + ' ' + data.family_name);
                    });
            });

        /**
         * Generates the HTML needed to add a new user to the user list.
         * @param avatarURL    {string}   URL to user's avatar.
         * @param fname        {string}   User's first name.
         * @param lname        {string}   User's last name.
         * @param countryFlag  {string}   Path to user's country flag.
         * @param spokenLangs  {obj[]}    Array of languages {name {string}, prof {int}, flag {string}, id {int}}.
         * @param interests    {obj[]}    Array of interest objects {name {string}, id {int}}.
         * @param lastActive   {string}   Date string of user's last active time.
         * @returns            {string}   HTML for <li> of user.
         */
        function generateUserItem(avatarURL, countryFlag, fname, lname, spokenLangs, learningLangs, interests, lastActive, userId) {
            var html = '';
            var now = new Date();
            var lastActiveDate = new Date(lastActive.replace(/-/g, '/'));
            var diffMs = (lastActiveDate - now) * -1;
            var diffDays = Math.round(diffMs / 86400000);
            var diffHrs = Math.round((diffMs % 86400000) / 3600000);
            var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
            var online = false;
            var lastActiveStr = '';
            var interestStr = '';
            var len,
                c,
                onlineClass;

            now = new Date(now.valueOf() + now.getTimezoneOffset() * 60000 + 28800000);

            if (mainUser.id == userId) {
                return;
            }

            if (diffDays !== 0) {
                lastActiveStr = diffDays + ' days ago';
                if (diffDays === 1) {
                    lastActiveStr = diffDays + ' day ago';
                }
            }
            else if (diffHrs !== 0) {
                lastActiveStr = Math.abs(diffHrs) + ' hours ago';
                if (diffHrs === 1) {
                    lastActiveStr = diffHrs + ' hour ago';
                }
            }
            else if (diffMins !== 0) {
                lastActiveStr = diffMins + ' min ago';
                if (diffMins <= 2) {
                    online = true;
                }
            }
            else {
                lastActiveStr = 'active now';
                online = true;
            }

            onlineClass = online ? 'online-circle' : 'offline-circle';

            html += '<li>';
            html += '<div id="part-' + userId + '" class="item-content user-item">';
            html += '<div class="item-media media">';

            html += '<img class="user-item__avatar lazy lazy-fadein" src="' + (avatarURL ? avatarURL : 'images/default-avatar.png') + '" width="100%">';

            html += '<div class="user-item__flag"><span class="flag ' + countryFlag.toLowerCase() + '"></span></div>' +
                '</div>' +
                '<div class="item-inner">' +
                '<div class="item-title-row">' +
                '<h3 class="item-title user-item__name">' + fname + ' ' + lname + '<span class="' + onlineClass + '"></span></h3>' +
                '<span class="user-item__last-login item-after">' + lastActiveStr + '</span>' +
                '</div>' +
                '<div class="user-item__languages item-subtitle subtitle">';

            html += renderLanguageList(spokenLangs);

            if (learningLangs.length > 0) {
                html += '<i class="fa fa-long-arrow-right"></i>';
                html += renderLanguageList(learningLangs);
            }

            html += '</div>';

            for (c = 0, len = interests.length; c < len; c++) {
                interestStr += interests[c].name.toLowerCase();

                if (c !== (len - 1)) {
                    interestStr += ', ';
                }

            }


            html += '<p class="item-text user-item__interests">' + interestStr + '</p>' +
                '</div>' +
                '</div>' +
                '</li>';


            return html;
        }


        function renderLanguageList(languageList) {
            var profStr = '';
            var html = '';
            var c,
                len;
            for (c = 0, len = languageList.length; c < len; c++) {
                switch (languageList[c].prof) {
                    case 1:
                        profStr = 'elementary';
                        break;
                    case 2:
                        profStr = 'intermediate';
                        break;
                    case 3:
                        profStr = 'advanced';
                        break;
                    case 4:
                        profStr = 'fluent';
                        break;
                    case 5:
                        profStr = 'native';
                        break;
                }
                html += '<div class="language-flag-container -' + profStr + '" data-langID="' + languageList[c].id + '">' +
                    '<div class="set-size charts-container">' +
                    '<div class="pie-wrapper progress-' + profStr + '">' +
                    '<div class="pie">' +
                    '<div class="left-side half-circle"></div>' +
                    '<div class="right-side half-circle"></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<img src="' + languageList[c].flag + '">' +

                '</div>';
            }
            return html;
        }


        function getUser(x, moreUsers) {

            var user,
                spokenLangs = [],
                learningLangs = [],
                interests = [],
                flagFilter,
                age,
                html,
                $clone,
                cloneHeight,
                totalHeight;

            loading = true;

            user = usersArr[x];

            makeApiRequest('users/' + user.id, null, 'GET', true)
                .success(function(user) {
                    var c, a, len, b, outerLen, innerLen;

                    spokenLangs = [];
                    learningLangs = [];
                    interests = [];

                    for (c = 0, len = user['spoken_languages'].length; c < len; c++) {
                        spokenLangs.push({
                            'name': user['spoken_languages'][c]['english_name'],
                            'prof': user['spoken_languages'][c].pivot.proficiency,
                            'flag': user['spoken_languages'][c].flag,
                            'id': user['spoken_languages'][c].id
                        });
                    }

                    for (c = 0, len = user['learning_languages'].length; c < len; c++) {
                        learningLangs.push({
                            'name': user['learning_languages'][c]['english_name'],
                            'prof': user['learning_languages'][c].pivot.proficiency,
                            'flag': user['learning_languages'][c].flag,
                            'id': user['learning_languages'][c].id
                        });
                    }

                    for (c = 0, len = user.interests.length; c < len; c++) {
                        interests.push({
                            'name': user.interests[c].name.toLowerCase(),
                            'id': user.interests[c].id
                        });
                    }

                    if (filterParams !== undefined) {
                        flagFilter = true;

                        // Age Filter
                        age = calculateAge(new Date(user.birthday));

                        if (filterParams.minAge != 0 && flagFilter === true) {
                            if (age < filterParams.minAge) {
                                flagFilter = false;
                            }
                        }

                        if (filterParams.maxAge != 80 && flagFilter === true) {
                            if (age > filterParams.maxAge) {
                                flagFilter = false;
                            }
                        }

                        // Country Filter
                        if (filterParams.country !== '' && flagFilter === true) {
                            if (user.country.name !== filterParams.country) {
                                flagFilter = false;
                            }
                        }

                        // Language Filter
                        if (filterParams.language !== undefined && filterParams.language.name !== '' && flagFilter === true) {
                            flagFilter = false;

                            for (a = 0, len = spokenLangs.length; a < len; a++) {
                                if (spokenLangs[a].name.toLowerCase() === filterParams.language.name.toLowerCase()) {
                                    if (spokenLangs[a].prof >= filterParams.language.proficiency) {
                                        flagFilter = true;
                                    }
                                }
                            }

                            for (a = 0, len = learningLangs.length; a < len; a++) {
                                if (learningLangs[a].name.toLowerCase() === filterParams.language.name.toLowerCase()) {
                                    if (learningLangs[a].prof >= filterParams.language.proficiency) {
                                        flagFilter = true;
                                    }
                                }
                            }
                        }

                        // Interests Filter
                        if (filterParams.interests.length !== 0 && flagFilter === true) {
                            flagFilter = false;

                            for (a = 0, outerLen = interests.length; a < outerLen; a++) {
                                for (b = 0, innerLen = filterParams.interests.length; b < innerLen; b++) {
                                    if (interests[a].id == filterParams.interests[b]) {
                                        flagFilter = true;
                                        break;
                                    }
                                }

                                if (flagFilter) {
                                    break;
                                }
                            }
                        }

                        // Final Check
                        if (flagFilter) {
                            html = generateUserItem(user.avatar, user.country.iso_3166_2, user['first_name'], user['family_name'], spokenLangs, learningLangs, interests, user['last_active_at'], user.id);
                            $('.users-list > ul')
                                .append(html);
                        }
                    }
                    else {
                        html = generateUserItem(user.avatar, user.country.iso_3166_2, user['first_name'], user['family_name'], spokenLangs, learningLangs, interests, user['last_active_at'], user.id);
                        $('.users-list > ul')
                            .append(html);
                    }

                    $('.user-item')
                        .on('click', function(e) {
                            var curHolder = e.target;
                            while (!curHolder.id) {
                                curHolder = curHolder.parentNode;
                            }
                            mainView.router.load({
                                url: 'user-profile.html',
                                query: {
                                    id: curHolder.id
                                }
                            });
                        });
                    if (moreUsers == 0) {
                        loading = false;
                        $clone = $('.users-list'),
                        cloneHeight = $clone.outerHeight(),
                        totalHeight = cloneHeight;


                        /**
                         * Disable infinite scroll if the list of users is less than the height of the window
                         */

                        if (totalHeight > $(window).height()) {
                            infiniteScrollInit();
                            $('.infinite-scroll-preloader')
                                .show();
                        }
                    }
                });



            moreUsers--;


            if (moreUsers + 1) {
                if (x + 1 == usersArr.length) {
                    if (filterParams !== undefined) {
                        if ($('.users-list > ul > li')
                            .length === 0) {
                            $('.filter-error-msg > .row:first > .col-100')
                                .text('Sorry, no users match your search.');
                        }
                        else {
                            $('.filter-error-msg > .row:first > .col-100')
                                .text('Sorry, no more users match your search.');
                        }

                        $('.filter-error-msg > .row:nth-child(2) > .col-33 > a')
                            .text('Reset Filters');
                    }
                    else {
                        $('.filter-error-msg > .row:first > .col-100')
                            .text('Sorry, no more users...yet.');
                        $('.filter-error-msg > .row:nth-child(2) > .col-33 > a')
                            .text('Refresh');
                    }

                    $('.filter-error-msg')
                        .show();
                    myApp.detachInfiniteScroll($('.infinite-scroll'));
                    $('.infinite-scroll-preloader')
                        .remove();
                    return;
                }
                getUser(x + 1, moreUsers);
            }
        }

        /**
         * Generates the users for the users list.
         * @param numUsers {int}    Number of users to be loaded from the endIndex (global var).
         */
        function renderUsers(numUsers) {
            var start = endIndex;

            getUser(start, numUsers);

            endIndex = start + numUsers + 1;

            window.feedback.showPopup();
        }

        /**
         * Calculate user's age based on birthday.
         */
        function calculateAge(birthday) {
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs);
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }


        /**
         * Initialise infinite scroll functionality (from Framework7).
         */
        function infiniteScrollInit() {
            // Max items to load
            // Items to append per load
            var itemsPerLoad = 20;

            // Attach 'infinite' event handler:
            $('.infinite-scroll')
                .on('infinite', function() {
                    if (loading) {
                        return;
                    }
                    loading = true;
                    setTimeout(function() {
                        renderUsers(itemsPerLoad);
                        myApp.hideIndicator();
                    }, 1000);

                });
        }

        // Add 'refresh' event handler:
        ptrContent.on('refresh', function(e) {
            setTimeout(function() {
                makeApiRequest('users', null, 'GET', true)
                    .done(function(res) {
                        usersArr = res;

                        $('.users-list > ul > li')
                            .remove();
                        $('.infinite-scroll')
                            .unbind('infinite');

                        endIndex = 0;
                        renderUsers(20);
                        infiniteScrollInit();

                        myApp.pullToRefreshDone();
                        myApp.hideIndicator();
                    });
            }, 1000);
        });

        //feedback popup


        // $('.feedback').on('click', function () {
        //     var popupHTML = '<div class="popup options-popup">' +
        //         '<div class="content-block">' +
        //         '<h1 class="options-popup__user">Feedback</h1>' +
        //         '<p class="options-popup__text">Thank you for using Bilingua. We hope you enjoy our app and we are always looking for feedback to help us improve.</p>' +
        //         '<div class="list-block">' +
        //
        //
        //         '<div class="align-top">' +
        //         '<div class="item-content">' +
        //         '<div class="item-inner">' +
        //         '<div class="item-input">' +
        //         '<textarea id="text-area"></textarea>' +
        //         '</div>' +
        //         '</div>' +
        //         '</div>' +
        //         '</div>' +
        //         '</div>' +
        //
        //
        //         '<p class="options-popup__links"><a href="#" class="close-popup">Cancel</a>' +
        //         '<a href="#" class="submit-button is-disabled">Submit</a></p>' +
        //         '</div>' +
        //         '</div>'
        //     myApp.popup(popupHTML);
        //     // disabledButton();
        //     // submitButton();
        // });


        // function disabledButton() {
        //     $('.options-popup').on('keyup', '#text-area', function () {
        //
        //         if ($(this).val().length === 0) {
        //             $('.submit-button').addClass('is-disabled');
        //         } else {
        //             $('.submit-button').removeClass('is-disabled');
        //         }
        //     });
        //
        // };
        //
        // function submitButton() {
        //     $('.options-popup').on('click', '.submit-button', function () {
        //
        //         var feedback = {feedback: $('#text-area').val()};
        //         makeApiRequest('feedbacks', null, 'GET', true).done(
        //             function (res) {
        //
        //                 myApp.alert('Thanks for your feedback', 'Success!');
        //                 mainView.router.loadPage('chats.html');
        //
        //             });
        //
        //     });
        // };

        /**
         * UNUSED: Manual refresh for non-touch devices.
         */
        $('.refresh-row a')
            .on('click', function() {
                makeApiRequest('users', null, 'GET', true)
                    .done(function(res) {
                        usersArr = res;

                        $('.users-list > ul > li')
                            .remove();
                        $('.infinite-scroll')
                            .unbind('infinite');

                        endIndex = 0;
                        renderUsers(20);
                        infiniteScrollInit();

                        myApp.pullToRefreshDone();
                    });
            });

        //render unread message count
        window.renderUnreadMessageCount();
    });
})(jQuery, document, window);
