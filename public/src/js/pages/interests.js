(function($, document, window) {
    'use strict';
    // Now we need to run the code that will be executed only for Feature page.
    myApp.onPageInit('interests', function(page) {
        var makeApiRequest = window.makeApiRequest;
        var Interests = window.Interests;
        myApp.showIndicator();
        $(document)
            .off('click')
            .on('click', '.chip', function() {
                if ($('.custom-chip .bg-blue').length > 6) {
                    if ($(this)
                            .hasClass('bg-blue')) {
                        toggleChipClass($(this));
                    }
                    else {
                        myApp.alert('Please select only 7 interests', 'Warning');
                    }
                }
                else {
                    $('.error-message__register')
                        .hide();
                    toggleChipClass($(this));
                }
            });
        bindEvents();
        getInterest();
        /*
         Click on finish will submit selected interests
         */
        $('#interests-form')
            .submit(submitInterests);
        /*
         Dismiss error message
         */
        $('.error-message__dismiss')
            .click(function() {
                $('.error-message__register')
                    .slideUp('slow');
            });
        /*
         Get all the interests from backend
         */
        function getInterest() {
            /**
             * check if user is logged in and prepopulate interests
             */
            if (Auth.isLoggedIn()) {
                Interests.getInterests(function(status) {
                    var interestsArr;
                    if (status) {
                        interestsArr = Interests.getInterestsArr();
                        makeApiRequest('users/me', null, 'GET', true)
                            .success(function(res) {
                                var userInterests = res.interests;
                                interestsArr = interestsArr.filter(function(interest) {
                                    userInterests.forEach(function(userInterest, index) {
                                        if (userInterest.id === interest.id) {
                                            return false;
                                        }
                                    });
                                    return true;
                                });
                                showInterests(interestsArr, userInterests);
                            }
                            );
                        $('.show-more-button')
                            .click(function() {
                                myApp.showIndicator();
                                showMoreInterests(interestsArr);
                                // $(this).hide();
                                autocompleteInterests(interestsArr);
                            });
                    }
                    else {
                        $('.error-message__register .error-message__text')
                            .text(' Something went wrong. Please refresh it.');
                        $('.error-message__register')
                            .slideDown('fast');
                    }
                });
            }
            else {
                $('.error-message__register .error-message__text')
                    .text('Login credential are invalid.');
                $('.error-message__register')
                    .slideDown('fast');
                myApp.hideIndicator();
            }
        }

        /*
         Show top 10 interests initially
         */
        function showInterests(interestsArr, userInterests) {
            var interestsHTML = '', i,
                interestLen = userInterests.length;
            for (i = 0; i < interestLen; i++) {
                interestsHTML += '<div id= "' + userInterests[i].id + '" class="chip custom-chip bg-blue">';
                interestsHTML += '<div class="chip-media bg-blue"><i class="fa fa-check"></i></div>';
                interestsHTML += '<div class="chip-label">' + userInterests[i].name + '</div>';
                interestsHTML += '</div>';
            }
            for (i = 0; i < 10 - interestLen; i++) {
                interestsHTML += '<div id= "' + interestsArr[i].id + '" class="chip custom-chip">';
                interestsHTML += '<div class="chip-media"><i class="fa fa-plus"></i></div>';
                interestsHTML += '<div class="chip-label">' + interestsArr[i].name + '</div>';
                interestsHTML += '</div>';
            }
            $('.js-interests-container')
                .hide()
                .append(interestsHTML)
                .fadeIn(2000);
            myApp.hideIndicator();
        }

        /*
         Show 10 more interests after click on "Show More" Button
         */
        function showMoreInterests(interestsArr) {
            var interestsHTML,
                i;
            var interestContainer = $('.page-content');

            interestContainer
                .animate({
                    scrollTop: interestContainer
                        .height()
                }, 1000);
            interestsHTML = '';
            for (i = 11; i < 21; i++) {
                interestsHTML += '<div id= "' + interestsArr[i].id + '" class="chip custom-chip">';
                interestsHTML += '<div class="chip-media"><i class="fa fa-plus"></i></div>';
                interestsHTML += '<div class="chip-label">' + interestsArr[i].name + '</div>';
                interestsHTML += '</div>';
            }
            $('.js-interests-container')
                .append(interestsHTML);
            $('.show-more-button')
                .fadeOut();
            $('.interest-selection-wrapper')
                .show();
            myApp.hideIndicator();
        }

        /*
         Selection of more interests after first 20.
         */
        function autocompleteInterests(interestsArr) {
            var col,
                interestsDropDownHTML,
                selectedInterests,
                selectedInterestsArr,
                unSelectedInterests,
                unSelectedInterestsArr;
            fillSearchMoreInterests(interestsArr);
            $('.page-content')
                .on('click', function(e) {
                    if (!$(e.target)
                            .parents()
                            .is('#searchWrapperInterest')) {
                        $('#dropdownAutoComplete')
                            .hide();
                    }
                });
            initSearch();
            /*
             Clicking on CLOSE button move selected interests from dropdown to top
             */
            $('.search-dropdown-close__buttons a')
                .on('click', function() {
                    $('#dropdownAutoComplete')
                        .hide();
                    col = getColumnDimenstion();
                    selectedInterests = $('.search-dropdown__items')
                        .children('.' + col)
                        .children('.bg-blue');
                    if (selectedInterests.length > 0) {
                        selectedInterestsArr = selectedInterests;
                        selectedInterests.parent()
                            .remove('.' + col);

                        selectedInterestsArr.forEach(function(selectedInterest, index) {
                            interestsDropDownHTML = '';
                            interestsDropDownHTML += selectedInterest.outerHTML;
                            $('.js-interests-container')
                                .prepend(interestsDropDownHTML);
                        });
                    }

                    unSelectedInterests = $('.js-interests-container')
                        .children('.custom-chip:not(.bg-blue)')
                        .children('.chip-media');
                    if (unSelectedInterests.length > 0 && unSelectedInterests.length != 20) {
                        unSelectedInterestsArr = unSelectedInterests;
                        unSelectedInterests.remove('.custom-chip');

                        unSelectedInterestsArr.forEach(function(unSelectedInterest, index) {
                            interestsDropDownHTML = '';
                            interestsDropDownHTML += '<div class = "' + col + '">';
                            interestsDropDownHTML += unSelectedInterest.outerHTML;
                            $('.search-dropdown')
                                .children('.search-dropdown__items')
                                .append(interestsDropDownHTML);
                        });
                    }
                });
        }

        /*
         Fill dropdown items
         */
        function fillSearchMoreInterests(interestsArr) {
            var interestsDropDownHTML = '<div class = "col-100 search-dropdown-error--search"><p>No interest found </p> </div>';
            var i,
                col;

            for (i = 21; i < interestsArr.length; i++) {
                col = getColumnDimenstion();
                interestsDropDownHTML += '<div class = "' + col + '">';
                interestsDropDownHTML += '<div id= "' + interestsArr[i].id + '" class="chip custom-chip">';
                interestsDropDownHTML += '<div class="chip-media"><i class="fa fa-plus"></i></div>';
                interestsDropDownHTML += '<div class="chip-label">' + interestsArr[i].name + '</div>';
                interestsDropDownHTML += '</div>';
                interestsDropDownHTML += '</div>';
            }
            $('.search-dropdown')
                .children('.search-dropdown__items')
                .append(interestsDropDownHTML);
            $('.search-dropdown-error--search')
                .hide();
            $('.search-dropdown')
                .hide();
        }

        /*
         Initiaze search on input bar
         */
        function initSearch() {
            var $xButton = $('#searchInputInterest')
                .siblings('.fa-close');
            var $searchBar;
            //hide 'x' from search bar
            $xButton.hide();
            $xButton.on('click', function(e) {
                $searchBar = $xButton.siblings('input');
                $xButton.hide();
                $searchBar.val('');
                $searchBar.trigger('input');
            });
            $('#searchInputInterest')
                .on('click', function(e) {
                    toggleDropDown(e);
                });
            $('#searchInputInterest')
                .on('input', function(e) {
                    filterInterests(e);
                });
        }

        function toggleDropDown(e) {
            if ($(this)
                    .val() === '' && $('#dropdownAutoComplete')
                    .is(':visible')) {
                $('#dropdownAutoComplete')
                    .hide();
                $(this)
                    .blur();
            }
            else {
                $('#dropdownAutoComplete')
                    .show();
                $('.page-content')
                    .animate({
                        scrollTop: $('.page-content')
                            .height()
                    }, 1000);
            }
        }

        /*
         Filter interests using user search input character.
         */
        function filterInterests(e) {
            var $dropDownMenu = $('#dropdownAutoComplete');
            var $searchBar = $(e.target);
            var col = getColumnDimenstion();
            var numInterests;
            $dropDownMenu.children('.row')
                .children('.' + col)
                .each(function() {
                    if ($(this)
                            .children('.chip')
                            .children('.chip-label')
                            .text()
                            .toLowerCase()
                            .indexOf($searchBar.val()
                                .toLowerCase()) !== 0) {
                        $(this)
                            .hide();
                    }
                    else {
                        $(this)
                            .show();
                    }
                });
            numInterests = $dropDownMenu.children('.row')
                .children('.' + col)
                .filter(function() {
                    return $(this)
                            .css('display') !== 'none';
                }).length;
            if (numInterests === 0) {
                $dropDownMenu.find('.search-dropdown-error--search')
                    .show();
            }
            else {
                $dropDownMenu.find('.search-dropdown-error--search')
                    .hide();
            }
            if ($searchBar.val() === '') {
                $searchBar.siblings('.fa-close')
                    .hide();
                $searchBar.focus();
                $dropDownMenu.show();
            }
            else {
                $searchBar.siblings('.fa-close')
                    .show();
                $searchBar.focus();
                $dropDownMenu.show();
            }
        }

        /*
         Submit interests after user clicks on finish link.
         */
        function submitInterests() {
            var interests = [];
            var payload,
                jsonInterest;

            $('.chip.bg-blue')
                .each(function(name) {
                    jsonInterest = {};
                    jsonInterest['id'] = parseInt($(this)
                        .attr('id'));
                    interests.push(jsonInterest);
                });
            if (interests.length == 0) {
                $('.error-message__register .error-message__text')
                    .text(' Please select one interest atleast.');
                $('.error-message__register')
                    .slideDown('fast');
            }
            else if (interests.length > 7) {
                $('.error-message__register .error-message__text')
                    .text(' Please select only 7 seven interests.');
                $('.error-message__register')
                    .slideDown('fast');
            }
            else {
                payload = {};
                payload['interests'] = interests;
                makeApiRequest('users/me/interests', payload, 'PUT', true)
                    .done(function(data) {
                        if (page.query.edit == '2') {
                            mainView.router.loadPage('chats.html');
                        }
                        else {
                            mainView.router.loadPage('my-profile.html');
                        }
                        //Adding Notification for Language on  update
                        BilinguaAlert('You have great passions!');
                    });
            }
            $('.page-content')
                .animate({
                    scrollTop: 0
                }, 1000);
            return false;
        }

        /*
         Toggle chips while selecting and deselecting.
         */
        function toggleChipClass(doc) {
            doc.toggleClass('bg-blue')
                .children('.chip-media')
                .toggleClass('bg-blue')
                .children('.fa')
                .toggleClass('fa-plus fa-check');
        }

        /*
         Wrapper window width selects dropdown interests ration
         */
        function getColumnDimenstion() {
            var col = '';
            if ($('#wrapper')
                    .width() >= 700) {
                col = 'col-30';
            }
            else {
                col = 'col-50';
            }
            return col;
        }

        function bindEvents() {
            $('.error-message__register')
                .hide();
            $('.interest-selection-wrapper')
                .hide();
        }
    });
}(jQuery, document, window));
