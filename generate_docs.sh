#!/bin/sh
php artisan api:docs --output-file docs.apib --include-path "docs"
sudo aglio --theme-variables slate -i docs.apib -o public/docs.html
apiary publish --api-name="bilinguaapi" --path="docs.apib"